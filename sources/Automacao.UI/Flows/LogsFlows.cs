﻿using Automacao.UI.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automacao.UI.Flows
{
    public class LogsFlows
    {
        LogsPage logsPage;

        public LogsFlows()
        {
            logsPage = new LogsPage();
        }

        public void PreencherCalendario()
        {
            logsPage.VerificaSeBotaoCarregamentoFoiFechado();
            
            logsPage.ClicarCalendario();

            //  logsPage.ClicarOpcaoAno();

            //  logsPage.ClicarNoAnoSelecionado("2020");

            //  logsPage.ClicarNoMesSelecionado("jan");
         
            logsPage.ClicarDiaCalendario("1");
            Thread.Sleep(1000);
            logsPage.ClicarSelecionarDataCalendario();
        }
    }
}
