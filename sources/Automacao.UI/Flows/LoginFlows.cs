﻿using Automacao.UI.Pages;
using System.Threading;

namespace Automacao.UI.Flows
{
    public class LoginFlows
    {
        #region Page Object and Constructor
        LoginPage loginPage;

        public LoginFlows()
        {
            loginPage = new LoginPage();
        }
        #endregion

        public void PreencherUsuarioESenha(string username, string password)
        {
            loginPage.PreencherUsuario(username);
            loginPage.PreencherSenha(password);
        }
    }
}
