﻿using Automacao.UI.Flows;
using Automacao.UI.Helpers;
using Automacao.UI.Pages;
using System.Configuration;
using TechTalk.SpecFlow;

namespace Automacao.UI.StepDefinitions
{
    [Binding]
    public class LoginSteps
    {
        LoginPage loginPage;
        LoginFlows loginFlows;

        public LoginSteps()
        {
            loginPage = new LoginPage();
            loginFlows = new LoginFlows();
        }

        [StepDefinition(@"que eu acesso a página de login")]
        public void DadoQueEuAcessoAPaginaDeLogin()
        {
            DriverFactory.INSTANCE.Navigate().GoToUrl(ConfigurationManager.AppSettings["base_url_qa"]);
        }

        [StepDefinition(@"eu preencho os campos com dados válidos")]
        public void QuandoEuPreenchoOsCamposComDadosValidos()
        {
            loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["password"]);
        }


        [StepDefinition(@"eu clico em Entrar")]
        public void QuandoEuClicoEmEntrar()
        {
            loginPage.ClicarEmEntrar();
        }
        
        [StepDefinition(@"eu preencho os campos com (.*) e (.*)")]
        public void QuandoEuPreenchoOsCamposComE(string usuario, string senha)
        {
            loginFlows.PreencherUsuarioESenha(usuario, senha);
        }

        [StepDefinition(@"eu preencho os campos com credenciais de usuário (.*)")]
        public void QuandoEuPreenchoOsCamposComCredenciaisDeUsuario(string perfil)
        {
            if(perfil.Equals("Administrador"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameAdministrador"], ConfigurationManager.AppSettings["passwordAdministrador"]);
            }
            else if(perfil.Equals("Analista"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameAnalista"], ConfigurationManager.AppSettings["passwordAnalista"]);
            }
            else if (perfil.Equals("BPO"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameBPO"], ConfigurationManager.AppSettings["passwordBPO"]);
            }
            else if (perfil.Equals("Consultor"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameConsultor"], ConfigurationManager.AppSettings["passwordConsultor"]);
            }
            else if (perfil.Equals("Coordenador BPO"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameCoordenadorBPO"], ConfigurationManager.AppSettings["passwordCoordenadorBPO"]);
            }
            else if (perfil.Equals("Especial 1"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameEspecial1"], ConfigurationManager.AppSettings["passwordEspecial1"]);
            }
            else if (perfil.Equals("Terceiro"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameTerceiro"], ConfigurationManager.AppSettings["passwordTerceiro"]);
            }
            else if (perfil.Equals("Compliance"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameCompliance"], ConfigurationManager.AppSettings["passwordCompliance"]);
            }
            else if (perfil.Equals("Analista Funcionario"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["usernameAnalistaFuncionario"], ConfigurationManager.AppSettings["passwordAnalistaFuncionario"]);
            }
            else if(perfil.Equals("com múltiplos perfis"))
            {
                loginFlows.PreencherUsuarioESenha(ConfigurationManager.AppSettings["username"], ConfigurationManager.AppSettings["password"]);
            }               
        }

        [StepDefinition(@"o sistema deve realizar o login com sucesso")]
        public void EntaoOSistemaDeveRealizarOLoginComSucesso()
        {
            loginPage.ExisteBoasVindasModal();
        }
        
        [StepDefinition(@"o sistema deve exibir a mensagem ""(.*)""")]
        public void EntaoOSistemaDeveExibirAMensagem(string mensagem)
        {
            loginPage.ExibeMensagensDeErro(mensagem);
        }
    }
}
