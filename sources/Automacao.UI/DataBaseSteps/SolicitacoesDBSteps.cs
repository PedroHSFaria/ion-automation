﻿using Automacao.UI.Queries;
using CSharpSeleniumTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automacao.UI.DataBaseSteps
{
    public class SolicitacoesDBSteps
    {
        public static List<string> RetornaSituacaoSolicitacao(string idSolicitacao, string catalog)
        {
            string query = Solicitacao.RetornarSituacaoSolicitacao.Replace("$idSolicitacao", idSolicitacao);

            return DataBaseHelpers.RetornaDadosQuery(query, catalog);
        }
    }
}
