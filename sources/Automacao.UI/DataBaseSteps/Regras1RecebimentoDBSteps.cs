﻿using CSharpSeleniumTemplate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automacao.UI.Queries;
using CSharpSeleniumTemplate.Helpers;

namespace Automacao.UI.DataBaseSteps
{
     public class Regras1RecebimentoDBSteps
        {
            public static List<string> VerificaSeHouveAlteracaoDaRegra(string catalog)
            {
                string query;

                query = RecebimentoRegra.VerificaSeHouveAlteracaoDaRegra;

                return DataBaseHelpers.RetornaDadosQuery(query, catalog);
            }
      }
 }
