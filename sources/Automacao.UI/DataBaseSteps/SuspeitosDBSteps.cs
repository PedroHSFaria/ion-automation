﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automacao.UI.Queries;
using CSharpSeleniumTemplate.Helpers;

namespace Automacao.UI.DataBaseSteps
{
    public class SuspeitosDBSteps
    {
        public static List<string> RetornaQuantidadeSuspeitos(string nome, string cpfcnpj, string agencia, string conta, string catalog)
        {
            string query;

            query = Suspeitos.RetornarQuantidadeSuspeitos.Replace("$nome", nome).Replace("$cpfcnpj", cpfcnpj).Replace("$agencia", agencia).Replace("$conta", conta);

            return DataBaseHelpers.RetornaDadosQuery(query, catalog);
        }
    }

    public class SuspeitoIncorretoDBSteps
    {
        public static List<string> SelecionarSuspeitoIncorreto(string nome, string cpfcnpj, string agencia, string conta, string catalog)
        {
            string query;

            query = Suspeitos.SelecionarSuspeitoIncorreto.Replace("$nome", nome).Replace("$cpfcnpj", cpfcnpj).Replace("$agencia", agencia).Replace("$conta", conta);

            return DataBaseHelpers.RetornaDadosQuery(query, catalog);
        }
    }

    public class SelecionaUltimoSuspeitoExcluidoDBSteps
    {
        public static List<string> SelecionaUltimoSuspeitoExcluido(string catalog)
        {
            string query;

            query = Suspeitos.SelecionaUltimoSuspeitoExcluido;

            return DataBaseHelpers.RetornaDadosQuery(query, catalog);
        }
    }
        public class SelecionaUltimoSuspeitoIncluidoDBSteps
        {
            public static List<string> SelecionaUltimoSuspeitoIncluido(string catalog)
            {
                string query;

                query = Suspeitos.SelecionaUltimoSuspeitoIncluido;

                return DataBaseHelpers.RetornaDadosQuery(query, catalog);
            }
        }
        
        public class VerificaSeusuarioFoiCadastradoDBSteps
        {
            public static List<string> SelecionaDestinatarioCadastrado(string catalog)
            {
            string query;

            query = Suspeitos.VerificaSeUsuarioFoiCadastrado;

            return DataBaseHelpers.RetornaDadosQuery(query, catalog);
            }
        }

    public class VerificaQuandoNaoHaFuncionarioDBSteps
    {
        public static List<string> VerificaQuandoNaoHaFuncionario(string catalog)
        {
            string query;

            query = Suspeitos.VerificaQuandoNaoHaFuncionario;

            return DataBaseHelpers.RetornaDadosQuery(query, catalog);
        }
    }
}
