﻿#language: pt-br

Funcionalidade: TedsDiariasAcimaDoLimite
	Como Usuário administrador
	Eu quero editar os parametros dessa regra
	Para que eu possa fazer algumas validações na regra
	
Contexto: 
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"
	E clico em editar regra Teds diárias acima do limite

@automatizar0603
Cenário: Não editar nenhum parâmetro da regra Teds diárias acima do limite
	Dado que estou na pagina de regra Teds diárias acima do limite
	E Nao edito nenhum campo
	Quando clico em salvar
	Entao exibe a mensagem confirmando que o registro nao foi alterado

@automatizar0603
Cenário: Editar regra Teds diárias acima do limite
	Dado que estou na pagina de regra Teds diárias acima do limite
	E Edito todos os campos
	Quando clico em salvar
	Entao exibe a mensagem confirmando que o registro foi alterado

@automatizar0603
Cenário: Nao preenchimento de campos obrigatórios Teds diárias acima do limite
	Dado que estou na pagina de regra Teds diárias acima do limite
	E Nao preencho algum campo obrigatorio
	Quando clico em salvar
	Entao exibe a mensagem "O Preenchimento do campo é obrigatório"

@automatizar0603
Cenário: Cancelar Edicao de Regra Teds diárias acima do limite
	Dado que estou na pagina de regra Teds diárias acima do limite
	E preencho os campos
	Quando clico em cancelar
	Entao sou redirecionado para a tela de regras de alertas		