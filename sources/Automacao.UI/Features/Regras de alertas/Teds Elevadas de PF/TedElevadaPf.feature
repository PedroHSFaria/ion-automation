﻿#language: pt-br

Funcionalidade: TedsElevadasDePF
	Como Usuário do sistema Cactus
	Eu quero Realizar ativacao e inativacao da regra
	Para que gere logs

Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"

@automatizadoProntoPraSubir
Cenario: Ativar Regra de Teds com valores elevados de PF
	Dado que Estou na pagina de logs
	Quando Clico em ativar
	Entao a barra deve constar como ativada
	E O sistema deve exibir a mensagem: "Como a regra foi ativada a seguinte data/hora de corte foi adicionada"

@automatizadoProntoPraSubir
Cenario: Inativar Regra de Teds com valores elevados de PF
	Dado que Estou na pagina de logs
	Quando Clico em inativar
	Entao a barra deve constar como inativada
	
