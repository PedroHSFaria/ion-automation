﻿#language:pt-br

Funcionalidade: ValidacaoParametrosTedsElevadas
	Como Usuário do sistema Cactus
	Eu quero validar os parametros da regra Teds elevadas de pf
	Para verificar se os alarmes estão sendo gerados normalmente
	
Contexto: 
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"
	E clico em editar regra
	E exibe a pagina de regra ted elevada de pf

@automatizadoProntoPraSubir
Cenário: Validacao de valor Ted menor ou igual ao parametro
	Dado Que edito o campo valor de cada ted
	E Salvo a regra
	E Ativo a regra
	Quando clico no menu alertas
	E realizo os filtros selecionando o alerta ted elevada para pf
#	Entao o alerta nao deve ser apresentado

@automatizar
Cenário: Validacao de valor Ted com valor que corresponde ao parametro
	Dado Que edito o campo valor de cada ted com um valor correto
	E Salvo a regra
	E Ativo a regra
	Quando clico no menu alertas
	E realizo os filtros selecionando o alerta ted elevada para pf
	Entao o alerta deve ser apresentado

@automatizadoProntoPraSubir
Cenário: Cancelar edicoes em regra
	Dado Que edito todos os parametros da regra
	Quando clico em cancelar
	Entao o sistema deve me redirecionar para a pagina regras de alertas