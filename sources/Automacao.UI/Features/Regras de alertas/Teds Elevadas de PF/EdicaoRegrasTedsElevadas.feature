﻿#language: pt-br

Funcionalidade: EdicaoRegrasTedsElevadas
	Como Usuário administrador
	Eu quero editar os parametros dessa regra
	Para que eu possa fazer algumas validações na regra
	
Contexto: 
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"
	E clico em editar regra

@automatizado
Cenário: Não editar nenhum parâmetro da regra
	Dado que estou na pagina de regra TEDs elevadas de PF
	E Nao edito nenhum campo
	Quando clico em salvar
	Entao exibe a mensagem confirmando que o registro nao foi alterado

@automatizado
Cenário: Editar regra Teds elevadas de PF
	Dado que estou na pagina de regra TEDs elevadas de PF
	E Edito todos os campos
	Quando clico em salvar
	Entao exibe a mensagem confirmando que o registro foi alterado

@automatizado
Cenário: Nao preenchimento de campos obrigatórios
	Dado que estou na pagina de regra TEDs elevadas de PF
	E Nao preencho algum campo obrigatorio
	Quando clico em salvar
	Entao exibe a mensagem "O Preenchimento do campo é obrigatório"

@automatizado
Cenário: Cancelar Edicao de Regra
	Dado que estou na pagina de regra TEDs elevadas de PF
	E preencho os campos
	Quando clico em cancelar
	Entao sou redirecionado para a tela de regras de alertas			