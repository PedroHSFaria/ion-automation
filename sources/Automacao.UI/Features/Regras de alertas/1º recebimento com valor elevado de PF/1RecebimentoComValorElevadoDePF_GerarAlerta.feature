﻿#language: pt-br

Funcionalidade: 1º recebimento com valor elevado de PF - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física efetuar transações que atendam aos parâmetros da regra “1º recebimento com valor elevado de PF”, utilizando o Aplicativo e/ou Cartão do BS2
	Para que possa tratar ou analisar a possível fraude

@automatizar
Cenário: Gerar alerta de fraude de "1º recebimento com valor elevado de PF"
	Dado que a primeira transação efetuada se encaixa nas seguintes características
      | CARACTERISTICA               | PARAMETRO                      |
      | Valor da movimentação (R$)   | Valor parametrizado na regra   |
      | Período de abertura da conta | Período parametrizado na regra |
      | Tipos de contas              | Tipo parametrizado na regra    |
   Quando o sistema validar a transação
   Então deve ser gerado um alerta de fraude da regra “1º recebimento com valor elevado de PF” com situação igual a "Aguardando análise”

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "1º recebimento com valor elevado de PF"
	Dado que o sistema gerou um alerta de “1º recebimento com valor elevado de PF”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “1º recebimento com valor elevado de PF"

@automatizar
Cenario: Ver transações de um alerta de fraude de "1º recebimento com valor elevado de PF"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "1º recebimento com valor elevado de PF"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta

#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/62171533
#
############################################ OBSERVAÇÕES ########################################################	
#	
#	TRANSAÇÕES
#
#		Deve-se utilizar todas as transações de crédito em conta (hstnat = 'C') da da tabela "tb_bs2_cc". Exemplo de consulta:
#
#			use FINFINDBS_PRD
#			select * from tb_bs2_cc
#			where hstnat = 'C'
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = '333182'
#
########################################## FIM DAS OBSERVAÇÕES #################################################