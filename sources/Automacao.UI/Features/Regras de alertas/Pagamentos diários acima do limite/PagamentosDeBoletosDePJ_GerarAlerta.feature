﻿#language: pt-br

Funcionalidade: Pagamentos de boletos de PJ - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa jurídica efetuar transações que atendam aos parâmetros da regra “Pagamentos de boletos de PJ”
	Para que possa tratar ou analisar a possível fraude

@automatizar
Cenário: Gerar alerta de fraude de "Pagamentos de boletos de PJ"
  Dado que são efetuados "pagamentos de boletos" que se encaixam nas seguintes características
      | CARACTERISTICA                                                                 | PARAMETRO                         |
      | Período de efetuação dos pagamentos                                            | Período parametrizado na regra    |
      | Qtde. de boletos pagos                                                         | Quantidade parametrizada na regra |
      | Valor do pagamento (R$)                                                        | Valor parametrizado na regra      |
      | Período de abertura da conta                                                   | Período parametrizado na regra    |
      | Período do último crédito em conta                                             | Período parametrizado na regra    |
      | Tipos de contas                                                                | Tipo parametrizado na regra       |
   Quando o sistema validar os "pagamentos de boletos"
   Então deve ser gerado um alerta de fraude da regra “Pagamentos de boletos de PJ” com situação igual a "Aguardando análise”

@automatizar
Cenário: Gerar novo alerta de fraude de "Pagamentos de boletos de PJ" quando o anterior já foi analisado como "Fraude"
    Dado que já tenham sido efetuados "pagamentos de boletos" que tenham gerado um alerta de fraude da regra  "Pagamentos de boletos de PJ"
    E a situação do alerta é igual a "Fraude"
    Quando novos "pagamentos de boletos" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então deve ser gerado um alerta de fraude da regra “Pagamentos de boletos de PJ” com situação igual a "Aguardado análise”    

@automatizar
Cenário: Gerar novo alerta de fraude de "Pagamentos de boletos de PJ" quando o anterior já foi analisado como "Não fraude"
    Dado que já tenham sido efetuados "pagamentos de boletos" que tenham gerado um alerta de fraude da regra "Pagamentos de boletos de PJ"
    E a situação do alerta é igual a "Não fraude"
    Quando novos "pagamentos de boletos" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então deve ser gerado um alerta de fraude da regra “Pagamentos de boletos de PJ” com situação igual a "Aguardado análise”    

@automatizar
Cenario: Não gerar alerta de fraude de "Pagamentos de boletos de PJ"
    Dado que já tenham sido efetuados "pagamentos de boletos" que tenham gerado um alerta de fraude da regra "Pagamentos de boletos de PJ"
	E a análise do alerta não foi realizada
	Quando novos "pagamentos de boletos" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então não deve ser gerado alerta de fraude da regra “Pagamentos de boletos de PJ”
	E os novos "pagamentos de boletos" devem ser adicionados no alerta já existente 

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "Pagamentos de boletos de PJ"
	Dado que o sistema gerou um alerta de “Pagamentos de boletos de PJ”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Pagamentos de boletos de PJ"

@automatizar
Cenario: Ver transações de um alerta de fraude de "Pagamentos de boletos de PJ"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Pagamentos de boletos de PJ"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/62050386
#
############################################ OBSERVAÇÕES ########################################################	
#
#		As transações do CCO devem ser recuperadas da tabela "tb_bs2_cc": 
#
#			| hstcod | hstdes 								|
#			| 782 	 | PGTO COBRANCA INTERN.BANK.			|
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = '333182'
#
#		Para verificar o útimo crédito em conta, pode-se efetuar a consulta abaixo na tabela "tb_bs2_cc" do CCO:
#
#			use FINFINDBS_PRD
#			select * from tb_bs2_cc
#			where ctanum = 'Número da conta do titular'
#			and hstnat = 'C'
#			ordre by hdrdata desc
#
########################################## FIM DAS OBSERVAÇÕES #################################################
