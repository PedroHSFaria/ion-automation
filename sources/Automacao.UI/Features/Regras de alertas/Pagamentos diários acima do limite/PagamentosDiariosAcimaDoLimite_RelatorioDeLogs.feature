﻿#language: pt-br

Funcionalidade: Pagamentos diários acima do limite - Relatório de log
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder extrair o relatório de log
	Para saber quando a regra "Pagamentos diários acima do limite" foi ativada, inativa e/ou alterada
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"

@automatizar
Cenario: Relatório de log de ativação a regra "Pagamentos diários acima do limite"
	Quando a regra "Pagamentos diários acima do limite" está inativa
	E aciono o comando "Ativar" da regra "Pagamentos diários acima do limite"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de inativação a regra "Pagamentos diários acima do limite"
	Quando a regra "Pagamentos diários acima do limite" está ativa
	E aciono o comando "Inativar" da regra "Pagamentos diários acima do limite"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra 
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de alteração d regra "Pagamentos diários acima do limite"
	Quando clico em "Editar" na regra "Pagamentos diários acima do limite"
	E altero um ou mais parâmetros da regra "Pagamentos diários acima do limite"
	E clico em "Salvar"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"
	
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# EXEMPLO DO LOG DE "ATIVAÇÃO DE REGRA": 
#	
#	| NOME_DO_ATRIBUTO                                   | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO                      |
#	| Produto                                            | Aplicativo                         | Aplicativo                                       |
#	| Funcionalidade                                     | Regras                             | Regras                                           |
#	| Transação                                          | Ativação de regra                  | Ativação de regra                                |
#	| Analista                                           | BSI90896 - José Silva              | BSI90896 - José Silva                            |
#	| Data/Hora da transação                             | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21                           |
#	| Regra                                              | Pagamentos diários acima do limite | Pagamentos diários acima do limite               |
#	| Situação                                           | Inativa                            | Ativa                                            |
#	| Gravidade                                          | Média                              | Média                                            |
#	| Operação do período de efetuação dos pagamentos    | Menor ou igual a                   | Menor ou igual a                                 |
#	| Período de efetuação dos pagamentos                | 12                                 | 12                                               |
#	| Tempo do período de efetuação dos pagamentos       | Horas                              | Horas                                            |
#	| Operação da qtde. de pagamentos efetuados          | Maior ou igual a                   | Maior ou igual a                                 |
#	| Qtde. de pagamentos efetuados                      | 2                                  | 2                                                |
#	| Operação do valor do limite de cada pagamento (R$) | Maior ou igual a                   | Maior ou igual a                                 |
#	| Valor do limite de cada pagamento (R$)             | R$ 500,00                          | R$ 500,00                                        |
#	| Operação do período de abertura da conta           | Maior que                          | Maior que                                        |
#	| Período de abertura da conta                       | 90                                 | 90                                               |
#	| Tempo do período de abertura da conta              | Dias                               | Dias                                             |
#	| Operação do período do último crédito em conta     | Maior que                          | Maior que                                        |
#	| Período do último crédito em conta                 | 24                                 | 24                                               |
#	| Tempo do período do último crédito em conta        | Horas                              | Horas                                            |
#	| Transações                                         | Pagamento de boleto                | Pagamento de boletO								 |
#	| Tipos de contas selecionados                       | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital                      |
#	| Bloqueio automático                                | Ativo                              | Ativo                                            |
#	| Notificar titular através de push                  | Inativo                            | Inativo                                          |
#	| Notificar titular através de e-mail                | Inativo                            | Inativo                                          |
#
#
# EXEMPLO DO LOG DE "INATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                   | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO                      |
#	| Produto                                            | Aplicativo                         | Aplicativo                                       |
#	| Funcionalidade                                     | Regras                             | Regras                                           |
#	| Transação                                          | Inativação de regra                | Inativação de regra                              |
#	| Analista                                           | BSI90896 - José Silva              | BSI90896 - José Silva                            |
#	| Data/Hora da transação                             | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21                           |
#	| Regra                                              | Pagamentos diários acima do limite | Pagamentos diários acima do limite               |
#	| Situação                                           | Ativa                              | Inativa                                          |
#	| Gravidade                                          | Média                              | Média                                            |
#	| Operação do período de efetuação dos pagamentos    | Menor ou igual a                   | Menor ou igual a                                 |
#	| Período de efetuação dos pagamentos                | 12                                 | 12                                               |
#	| Tempo do período de efetuação dos pagamentos       | Horas                              | Horas                                            |
#	| Operação da qtde. de pagamentos efetuados          | Maior ou igual a                   | Maior ou igual a                                 |
#	| Qtde. de pagamentos efetuados                      | 2                                  | 2                                                |
#	| Operação do valor do limite de cada pagamento (R$) | Maior ou igual a                   | Maior ou igual a                                 |
#	| Valor do limite de cada pagamento (R$)             | R$ 500,00                          | R$ 500,00                                        |
#	| Operação do período de abertura da conta           | Maior que                          | Maior que                                        |
#	| Período de abertura da conta                       | 90                                 | 90                                               |
#	| Tempo do período de abertura da conta              | Dias                               | Dias                                             |
#	| Operação do período do último crédito em conta     | Maior que                          | Maior que                                        |
#	| Período do último crédito em conta                 | 24                                 | 24                                               |
#	| Tempo do período do último crédito em conta        | Horas                              | Horas                                            |
#	| Transações                                         | Pagamento de boleto                | Pagamento de boleto								 |
#	| Tipos de contas selecionados                       | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital                      |
#	| Bloqueio automático                                | Ativo                              | Ativo                                            |
#	| Notificar titular através de push                  | Inativo                            | Inativo                                          |
#	| Notificar titular através de e-mail                | Inativo                            | Inativo                                          |
#
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                   | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO                      |
#	| Produto                                            | Aplicativo                         | Aplicativo                                       |
#	| Funcionalidade                                     | Regras                             | Regras                                           |
#	| Transação                                          | Alteração de regra                 | Alteração de regra                               |
#	| Analista                                           | BSI90896 - José Silva              | BSI90896 - José Silva                            |
#	| Data/Hora da transação                             | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21                           |
#	| Regra                                              | Pagamentos diários acima do limite | Pagamentos diários acima do limite               |
#	| Situação                                           | Ativa                              | Ativa                                            |
#	| Gravidade                                          | Média                              | Média                                            |
#	| Operação do período de efetuação dos pagamentos    | Menor ou igual a                   | Menor ou igual a                                 |
#	| Período de efetuação dos pagamentos                | 12                                 | 12                                               |
#	| Tempo do período de efetuação dos pagamentos       | Horas                              | Horas                                            |
#	| Operação da qtde. de pagamentos efetuados          | Maior ou igual a                   | Maior ou igual a                                 |
#	| Qtde. de pagamentos efetuados                      | 2                                  | 2                                                |
#	| Operação do valor do limite de cada pagamento (R$) | Maior ou igual a                   | Maior ou igual a                                 |
#	| Valor do limite de cada pagamento (R$)             | R$ 500,00                          | R$ 1.000,00                                      |
#	| Operação do período de abertura da conta           | Maior que                          | Maior que                                        |
#	| Período de abertura da conta                       | 90                                 | 90                                               |
#	| Tempo do período de abertura da conta              | Dias                               | Dias                                             |
#	| Operação do período do último crédito em conta     | Maior que                          | Maior que                                        |
#	| Período do último crédito em conta                 | 24                                 | 24                                               |
#	| Tempo do período do último crédito em conta        | Horas                              | Horas                                            |
#	| Transações                                         | Pagamento de boleto                | Pagamento de boleto, Pagamento de concessionária |
#	| Tipos de contas selecionados                       | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital                      |
#	| Bloqueio automático                                | Ativo                              | Ativo                                            |
#	| Notificar titular através de push                  | Inativo                            | Inativo                                          |
#	| Notificar titular através de e-mail                | Inativo                            | Inativo                                          |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################