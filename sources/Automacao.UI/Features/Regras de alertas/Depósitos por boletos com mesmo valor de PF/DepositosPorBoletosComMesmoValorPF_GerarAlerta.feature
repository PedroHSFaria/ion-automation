﻿#language: pt-br

Funcionalidade: Depósitos por boletos com mesmo valor de PF - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física receber transações que atendam aos parâmetros da regra “Depósitos por boletos com mesmo valor de PF”
	Para que possa tratar ou analisar a possível fraude

@automatizar
Cenário: Gerar alerta de fraude de "Depósitos por boletos com mesmo valor de PF"
	Dado que são efetuados "depósitos por boletos" com mesmo valor que se encaixam nas seguintes características
      | CARACTERISTICA                      | PARAMETRO                         |
      | Período de efetuação dos depósitos  | Período parametrizado na regra    |
      | Qtde. de depósitos efetuados        | Quantidade parametrizada na regra |
      | Valor do depósito (R$)              | Valor parametrizado na regra	    |
      | Período de abertura da conta        | Período parametrizado na regra    |
      | Tipos de contas                     | Tipo parametrizado na regra       |
   Quando o sistema validar os "depósitos por boletos"
   Então deve ser gerado um alerta de fraude da regra “Depósitos por boletos com mesmo valor de PF” com situação igual a "Aguardando análise”

@automatizar
Cenário: Gerar novo alerta de fraude de "Depósitos por boletos com mesmo valor de PF" quando o anterior já foi analisado como "Fraude"
    Dado que já tenham sido efetuados "depósitos por boletos" com mesmo valor que tenham gerado um alerta de fraude da regra "Depósitos por boletos com mesmo valor de PF"
    E a situação do alerta é igual a "Fraude"
    Quando novos "depósitos por boletos" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então deve ser gerado um alerta de fraude da regra “Depósitos por boletos com mesmo valor de PF” com situação igual a "Aguardado análise”    

@automatizar
Cenário: Gerar novo alerta de fraude de "Depósitos por boletos com mesmo valor de PF" quando o anterior já foi analisado como "Não fraude"
    Dado que já tenham sido efetuados "depósitos por boletos" com mesmo valor que tenham gerado um alerta de fraude da regra "Depósitos por boletos com mesmo valor de PF"
    E a situação do alerta é igual a "Não fraude"
    Quando novos "depósitos por boletos" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então deve ser gerado um alerta de fraude da regra “Depósitos por boletos com mesmo valor de PF” com situação igual a "Aguardado análise”

@automatizar
Cenario: Não gerar alerta de fraude de "Depósitos por boletos com mesmo valor de PF"
    Dado que já tenham sido efetuados "depósitos por boletos" com mesmo valor que tenham gerado um alerta de fraude da regra "Depósitos por boletos com mesmo valor de PF"
	E a análise do alerta não foi realizada
	Quando novos "depósitos por boletos" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então não deve ser gerado alerta de fraude da regra “Depósitos por boletos com mesmo valor de PF”
	E os novos "depósitos por boletos" devem ser adicionados no alerta já existente 

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "Depósitos por boletos com mesmo valor de PF"
	Dado que o sistema gerou um alerta de “Depósitos por boletos com mesmo valor de PF”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Depósitos por boletos com mesmo valor de PF"

@automatizar
Cenario: Ver transações de um alerta de fraude de "Depósitos por boletos com mesmo valor de PF"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Depósitos por boletos com mesmo valor de PF"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta

#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/61965227
#
############################################## OBSERVAÇÕES #####################################################	
#	
#		As transações do tipo "Depósito por boleto" devem ser recuperadas da tabela "tb_bs2_cc" do CCO:
#
#			| hstcod | hstdes			|
#			| 17     | LIQUIDO COBRANCA |
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = 'Número da conta do titular'
#
#		Para verificar o útimo crédito em conta, pode-se efetuar a consulta abaixo na tabela "tb_bs2_cc" do CCO:
#
#			use FINFINDBS_PRD
#			select * from tb_bs2_cc
#			where ctanum = 'Número da conta do titular'
#			and hstnat = 'C'
#			ordre by hdrdata desc
#
########################################## FIM DAS OBSERVAÇÕES #################################################