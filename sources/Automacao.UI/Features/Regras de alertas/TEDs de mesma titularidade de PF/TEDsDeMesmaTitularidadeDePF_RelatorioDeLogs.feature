﻿#language: pt-br

Funcionalidade: TEDs de mesma titularidade de PF - Relatório de regra
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder extrair o relatório de log
	Para saber quando a regra "TEDs de mesma titularidade de PF" foi ativada, inativa e/ou alterada
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"

@automatizar
Cenario: Relatório de log de ativação a regra "TEDs de mesma titularidade de PF"
	Quando a regra "TEDs de mesma titularidade de PF" está inativa
	E aciono o comando "Ativar" da regra "TEDs de mesma titularidade de PF"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de inativação a regra "TEDs de mesma titularidade de PF"
	Quando a regra "TEDs de mesma titularidade de PF" está ativa
	E aciono o comando "Inativar" da regra "TEDs de mesma titularidade de PF"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra 
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de alteração d regra "TEDs de mesma titularidade de PF"
	Quando clico em "Editar" na regra "TEDs de mesma titularidade de PF"
	E altero um ou mais parâmetros da regra "TEDs de mesma titularidade de PF"
	E clico em "Salvar"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"
	
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# EXEMPLO DO LOG DE "ATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                                              | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                                       | Regras                      | Regras                      |
#	| Transação                                                            | Ativação de regra           | Ativação de regra           |
#	| Analista                                                             | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                                                | TEDs de mesma titularidade de PF  | TEDs de mesma titularidade de PF  |
#	| Situação                                                             | Inativa                     | Ativa                       |
#	| Gravidade                                                            | Média                       | Média                       |
#	| Operação do período de efetuação das TEDs                            | Menor ou igual a            | Menor ou igual a            |
#	| Período de efetuação das TEDs                                        | 12                          | 12                          |
#	| Tempo do período de efetuação das TEDs                               | Horas                       | Horas                       |
#	| Operação da qtde. de TEDs efetuadas                                  | Maior ou igual a            | Maior ou igual a            |
#	| Qtde. de TEs efetuadas                                               | 2                           | 2                           |
#	| Operação do valor de cada TED (R$)                                   | Maior ou igual a            | Maior ou igual a            |
#	| Valor de cada TED (R$)                                               | R$ 500,00                   | R$ 500,00                   |
#	| Operação do período de abertura da conta                             | Maior que                   | Maior que                   |
#	| Período de abertura da conta                                         | 90                          | 90                          |
#	| Tempo do período de abertura da conta                                | Dias                        | Dias                        |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                                                  | Ativo                       | Ativo                       |
#	| Notificar titular através de push                                    | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail                                  | Inativo                     | Inativo                     |
#
#
# EXEMPLO DO LOG DE "INATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                                              | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                                       | Regras                      | Regras                      |
#	| Transação                                                            | Inativação de regra         | Inativação de regra         |
#	| Analista                                                             | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                                                | TEDs de mesma titularidade de PF  | TEDs de mesma titularidade de PF  |
#	| Situação                                                             | Ativa                       | Inativa                     |
#	| Gravidade                                                            | Média                       | Média                       |
#	| Operação do período de efetuação das TEDs                            | Menor ou igual a            | Menor ou igual a            |
#	| Período de efetuação das TEDs                                        | 12                          | 12                          |
#	| Tempo do período de efetuação das TEDs                               | Horas                       | Horas                       |
#	| Operação da qtde. de TEDs efetuadas                                  | Maior ou igual a            | Maior ou igual a            |
#	| Qtde. de TEs efetuadas                                               | 2                           | 2                           |
#	| Operação do valor de cada TED (R$)                                   | Maior ou igual a            | Maior ou igual a            |
#	| Valor de cada TED (R$)                                               | R$ 500,00                   | R$ 500,00                   |
#	| Operação do período de abertura da conta                             | Maior que                   | Maior que                   |
#	| Período de abertura da conta                                         | 90                          | 90                          |
#	| Tempo do período de abertura da conta                                | Dias                        | Dias                        |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                                                  | Ativo                       | Ativo                       |
#	| Notificar titular através de push                                    | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail                                  | Inativo                     | Inativo                     |
#
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                                              | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                                       | Regras                      | Regras                      |
#	| Transação                                                            | Alteração de regra          | Alteração de regra          |
#	| Analista                                                             | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                                                | TEDs de mesma titularidade de PF  | TEDs de mesma titularidade de PF  |
#	| Situação                                                             | Ativa                       | Ativa                       |
#	| Gravidade                                                            | Média                       | Média                       |
#	| Operação do período de efetuação das TEDs                            | Menor ou igual a            | Menor ou igual a            |
#	| Período de efetuação das TEDs                                        | 12                          | 12                          |
#	| Tempo do período de efetuação das TEDs                               | Horas                       | Horas                       |
#	| Operação da qtde. de TEDs efetuadas                                  | Maior ou igual a            | Maior ou igual a            |
#	| Qtde. de TEs efetuadas                                               | 2                           | 2                           |
#	| Operação do valor de cada TED (R$)                                   | Maior ou igual a            | Maior ou igual a            |
#	| Valor de cada TED (R$)                                               | R$ 500,00                   | R$ 1.000,00                 |
#	| Operação do período de abertura da conta                             | Maior que                   | Maior que                   |
#	| Período de abertura da conta                                         | 90                          | 90                          |
#	| Tempo do período de abertura da conta                                | Dias                        | Dias                        |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                                                  | Ativo                       | Ativo                       |
#	| Notificar titular através de push                                    | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail                                  | Inativo                     | Inativo                     |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################