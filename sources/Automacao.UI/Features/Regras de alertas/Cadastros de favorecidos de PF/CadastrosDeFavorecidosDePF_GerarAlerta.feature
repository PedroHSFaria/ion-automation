﻿#language: pt-br

Funcionalidade: Cadastros de favorecidos de PF - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física efetuar transações que atendam aos parâmetros da regra “Cadastros de favorecidos de PF”, utilizando o Aplicativo do BS2
	Para que possa tratar ou analisar a possível fraude

@automatizar
Esquema do Cenário: Gerar alerta de fraude de "Cadastros de favorecidos de PF"
	Dado que são efetuadas <transacoes> que se encaixam nas seguintes características
      | CARACTERISTICA                                                                 | PARAMETRO                         |
      | Período de efetuação dos cadastros                                             | Período parametrizado na regra    |
      | Qtde. de favorecidos cadastrados                                               | Quantidade parametrizada na regra |
      | Valor da transação (R$)                                                        | Valor parametrizado na regra      |
      | Período de abertura da conta                                                   | Período parametrizado na regra    |
      | Período do último crédito em conta                                             | Período parametrizado na regra    |
      | Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias) | Período parametrizado na regra    |
      | Tipos de contas                                                                | Tipo parametrizado na regra       |
   Quando o sistema validar as <transacoes>
   Então deve ser gerado um alerta de fraude da regra “Cadastros de favorecidos de PF” com situação igual a "Aguardando análise”
   Exemplos: 
	| transacoes                  |
	| TED                         |
	| Transferência interna       |

@automatizar
Esquema do Cenário: Gerar novo alerta de fraude de "Cadastros de favorecidos de PF" quando o anterior já foi analisado como "Fraude"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Cadastros de favorecidos de PF"
    E a situação do alerta é igual a "Fraude"
    Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então deve ser gerado um alerta de fraude da regra “Cadastros de favorecidos de PF” com situação igual a "Aguardado análise”    
   Exemplos: 
	| transacoes                  |
	| TED                         |
	| Transferência interna       |

@automatizar
Esquema do Cenário: Gerar novo alerta de fraude de "Cadastros de favorecidos de PF" quando o anterior já foi analisado como "Não fraude"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Cadastros de favorecidos de PF"
    E a situação do alerta é igual a "Não fraude"
    Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então deve ser gerado um alerta de fraude da regra “Cadastros de favorecidos de PF” com situação igual a "Aguardado análise”    
   Exemplos: 
	| transacoes                  |
	| TED                         |
	| Transferência interna       |

@automatizar
Esquema do Cenario: Não gerar alerta de fraude de "Cadastros de favorecidos de PF"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Cadastros de favorecidos de PF"
	E a análise do alerta não foi realizada
	Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então não deve ser gerado alerta de fraude da regra “Cadastros de favorecidos de PF”
	E as novas <transacoes> devem ser adicionadas no alerta já existente 
   Exemplos: 
	| transacoes                  |
	| TED                         |
	| Transferência interna       |

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "Cadastros de favorecidos de PF"
	Dado que o sistema gerou um alerta de “Cadastros de favorecidos de PF”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Cadastros de favorecidos de PF"

@automatizar
Cenario: Ver transações de um alerta de fraude de "Cadastros de favorecidos de PF"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Cadastros de favorecidos de PF"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta

#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/62051240
#
############################################ OBSERVAÇÕES ########################################################	
#	
#		As transações do Digital devem ser recuperadas das seguintes tabelas:
#
#			| Transação           			| Tabela 							|  
#			| TED 							| tra.TRANS_APP_TED 				|
#			| Transferência interna 		| tra.TRANS_APP_TRANSFER			|
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = 'Número da conta do titular'
#
#		Para verificar o útimo crédito em conta, pode-se efetuar a consulta abaixo na tabela "tb_bs2_cc" do CCO:
#
#			use FINFINDBS_PRD
#			select * from tb_bs2_cc
#			where ctanum = 'Número da conta do titular'
#			and hstnat = 'C'
#			ordre by hdrdata desc
#
########################################## FIM DAS OBSERVAÇÕES #################################################