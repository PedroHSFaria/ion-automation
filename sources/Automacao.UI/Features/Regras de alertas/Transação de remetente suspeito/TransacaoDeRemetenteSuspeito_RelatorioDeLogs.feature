﻿#language: pt-br

Funcionalidade: Transação de remetente suspeito - Relatório de regra
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder extrair o relatório de log
	Para saber quando a regra "Transação de remetente suspeito" foi ativada, inativa e/ou alterada
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"

@automatizar
Cenario: Relatório de log de ativação a regra "Transação de remetente suspeito"
	Quando a regra "Transação de remetente suspeito" está inativa
	E aciono o comando "Ativar" da regra "Transação de remetente suspeito"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de inativação a regra "Transação de remetente suspeito"
	Quando a regra "Transação de remetente suspeito" está ativa
	E aciono o comando "Inativar" da regra "Transação de remetente suspeito"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra 
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de alteração d regra "Transação de remetente suspeito"
	Quando clico em "Editar" na regra "Transação de remetente suspeito"
	E altero um ou mais parâmetros da regra "Transação de remetente suspeito"
	E clico em "Salvar"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"
	
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# EXEMPLO DO LOG DE "ATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO           | VALOR_POSTERIOR_A_TRANSACAO          |
#	| Produto                                                              | Aplicativo                           | Aplicativo                           |
#	| Funcionalidade                                                       | Regras                               | Regras                               |
#	| Transação                                                            | Ativação de regra                    | Ativação de regra                    |
#	| Analista                                                             | BSI90896 - José Silva                | BSI90896 - José Silva                |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21               | 30/09/2019 às 15:30:21               |
#	| Regra                                                                | Transação para destinatário suspeito | Transação para destinatário suspeito |
#	| Situação                                                             | Inativa                              | Ativa                                |
#	| Gravidade                                                            | Média                                | Média                                |
#	| Operação da qtde. de transações efetuadas                            | Maior ou igual a                     | Maior ou igual a                     |
#	| Qtde. de transações efetuadas                                        | 2                                    | 2                                    |
#	| Operação do valor de cada transação (R$)                             | Maior ou igual a                     | Maior ou igual a                     |
#	| Valor de cada transação (R$)                                         | R$ 500,00                            | R$ 500,00                            |
#	| Operação do período de abertura da conta                             | Maior que                            | Maior que                            |
#	| Período de abertura da conta                                         | 90                                   | 90                                   |
#	| Tempo do período de abertura da conta                                | Dias                                 | Dias                                 |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital          | 240 – Pessoa Física Digital          |
#	| Bloqueio automático                                                  | Ativo                                | Ativo                                |
#	| Notificar titular através de push                                    | Inativo                              | Inativo                              |
#	| Notificar titular através de e-mail                                  | Inativo                              | Inativo                              |
#
#
# EXEMPLO DO LOG DE "INATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                          | VALOR_ANTERIOR_A_TRANSACAO           | VALOR_POSTERIOR_A_TRANSACAO          |
#	| Produto                                   | Aplicativo                           | Aplicativo                           |
#	| Funcionalidade                            | Regras                               | Regras                               |
#	| Transação                                 | Inativação de regra                  | Inativação de regra                  |
#	| Analista                                  | BSI90896 - José Silva                | BSI90896 - José Silva                |
#	| Data/Hora da transação                    | 30/09/2019 às 15:30:21               | 30/09/2019 às 15:30:21               |
#	| Regra                                     | Transação para destinatário suspeito | Transação para destinatário suspeito |
#	| Situação                                  | Ativa                                | Inativa                              |
#	| Gravidade                                 | Média                                | Média                                |
#	| Operação da qtde. de transações efetuadas | Maior ou igual a                     | Maior ou igual a                     |
#	| Qtde. de transações efetuadas             | 2                                    | 2                                    |
#	| Operação do valor de cada transação (R$)  | Maior ou igual a                     | Maior ou igual a                     |
#	| Valor de cada transação (R$)              | R$ 500,00                            | R$ 500,00                            |
#	| Operação do período de abertura da conta  | Maior que                            | Maior que                            |
#	| Período de abertura da conta              | 90                                   | 90                                   |
#	| Tempo do período de abertura da conta     | Dias                                 | Dias                                 |
#	| Tipos de contas selecionados              | 240 – Pessoa Física Digital          | 240 – Pessoa Física Digital          |
#	| Bloqueio automático                       | Ativo                                | Ativo                                |
#	| Notificar titular através de push         | Inativo                              | Inativo                              |
#	| Notificar titular através de e-mail       | Inativo                              | Inativo                              |
#
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                          | VALOR_ANTERIOR_A_TRANSACAO           | VALOR_POSTERIOR_A_TRANSACAO          |
#	| Produto                                   | Aplicativo                           | Aplicativo                           |
#	| Funcionalidade                            | Regras                               | Regras                               |
#	| Transação                                 | Alteração de regra                   | Alteração de regra                   |
#	| Analista                                  | BSI90896 - José Silva                | BSI90896 - José Silva                |
#	| Data/Hora da transação                    | 30/09/2019 às 15:30:21               | 30/09/2019 às 15:30:21               |
#	| Regra                                     | Transação para destinatário suspeito | Transação para destinatário suspeito |
#	| Situação                                  | Ativa                                | Ativa                                |
#	| Gravidade                                 | Média                                | Média                                |
#	| Operação da qtde. de transações efetuadas | Maior ou igual a                     | Maior ou igual a                     |
#	| Qtde. de transações efetuadas             | 2                                    | 2                                    |
#	| Operação do valor de cada transação (R$)  | Maior ou igual a                     | Maior ou igual a                     |
#	| Valor de cada transação (R$)              | R$ 500,00                            | R$ 1.000,00                          |
#	| Operação do período de abertura da conta  | Maior que                            | Maior que                            |
#	| Período de abertura da conta              | 90                                   | 90                                   |
#	| Tempo do período de abertura da conta     | Dias                                 | Dias                                 |
#	| Tipos de contas selecionados              | 240 – Pessoa Física Digital          | 240 – Pessoa Física Digital          |
#	| Bloqueio automático                       | Ativo                                | Ativo                                |
#	| Notificar titular através de push         | Inativo                              | Inativo                              |
#	| Notificar titular através de e-mail       | Inativo                              | Inativo                              |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################