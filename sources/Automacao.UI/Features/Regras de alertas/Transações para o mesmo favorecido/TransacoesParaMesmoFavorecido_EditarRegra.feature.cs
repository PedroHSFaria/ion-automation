﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Automacao.UI.Features.RegrasDeAlertas.TransacoesParaOMesmoFavorecido
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Transações para o mesmo favorecido - Editar regra")]
    public partial class TransacoesParaOMesmoFavorecido_EditarRegraFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "TransacoesParaMesmoFavorecido_EditarRegra.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("pt-br"), "Transações para o mesmo favorecido - Editar regra", "\tComo responsável pelas parametrizações dos alertas de fraudes do BS2\r\n\tQuero pod" +
                    "er editar os parâmetros da regra  “Transações para o mesmo favorecido”\r\n\tPara qu" +
                    "e os alertas gerados sejam mais efetivos", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 8
#line 9
 testRunner.Given("que estou logado como \"Administrador\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 10
 testRunner.And("clico no menu \"Configurações\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 11
 testRunner.And("clico no submenu \"Regras de alertas\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 12
 testRunner.And("o sistema apresenta a tela \"Regras de alertas\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Ativar a regra \"Transações para o mesmo favorecido\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void AtivarARegraTransacoesParaOMesmoFavorecido()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Ativar a regra \"Transações para o mesmo favorecido\"", null, new string[] {
                        "automatizar"});
#line 15
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 16
 testRunner.When("a regra \"Transações para o mesmo favorecido\" está inativa", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 17
 testRunner.And("aciono o comando \"Ativar\" da regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 18
 testRunner.Then("o sistema deve \"ativar\" a regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 19
 testRunner.And("deve aplicar a data/hora de corte na regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 20
 testRunner.And("o sistema deve apresentar a mensagem \"Como a regra foi ativada, a seguinte data/h" +
                    "ora de corte foi adicionada: DD/MM/AAAA às HH:MM:SS.\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 21
 testRunner.And("o sistema deve gravar o log de \"Ativação\" da regra \"Transações para o mesmo favor" +
                    "ecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Inativar a regra \"Transações para o mesmo favorecido\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void InativarARegraTransacoesParaOMesmoFavorecido()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Inativar a regra \"Transações para o mesmo favorecido\"", null, new string[] {
                        "automatizar"});
#line 24
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 25
 testRunner.When("a regra \"Transações para o mesmo favorecido\" está ativa", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 26
 testRunner.And("aciono o comando \"Inativar\" da regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 27
 testRunner.Then("o sistema deve \"inativar\" a regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 28
 testRunner.And("deve gravar o log de \"Inativação\" da regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Apresentar os parâmetros da regra \"Transações para o mesmo favorecido\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void ApresentarOsParametrosDaRegraTransacoesParaOMesmoFavorecido()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Apresentar os parâmetros da regra \"Transações para o mesmo favorecido\"", null, new string[] {
                        "automatizar"});
#line 31
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 32
 testRunner.When("clico em \"Editar\" na regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 33
 testRunner.Then("o sistema apresenta a tela de edição da regra \"Transações para o mesmo favorecido" +
                    "\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            TechTalk.SpecFlow.Table table90 = new TechTalk.SpecFlow.Table(new string[] {
                        "NOME DO PARÂMETRO"});
            table90.AddRow(new string[] {
                        "Gravidade"});
            table90.AddRow(new string[] {
                        "Período de efetuação das transações"});
            table90.AddRow(new string[] {
                        "Qtde. de transações efetuadas"});
            table90.AddRow(new string[] {
                        "Valor de cada transação (R$)"});
            table90.AddRow(new string[] {
                        "Período de abertura da conta"});
            table90.AddRow(new string[] {
                        "Período do último crédito em conta"});
            table90.AddRow(new string[] {
                        "Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias)"});
            table90.AddRow(new string[] {
                        "Transações"});
            table90.AddRow(new string[] {
                        "Tipos de contas"});
#line 34
 testRunner.And("deve apresentar os seguintes parâmetros", ((string)(null)), table90, "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Editar parâmetros da regra \"Transações para o mesmo favorecido\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void EditarParametrosDaRegraTransacoesParaOMesmoFavorecido()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Editar parâmetros da regra \"Transações para o mesmo favorecido\"", null, new string[] {
                        "automatizar"});
#line 47
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 48
 testRunner.When("clico em \"Editar\" na regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 49
 testRunner.And("o sistema apresenta a tela de edição da regra \"Transações para o mesmo favorecido" +
                    "\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 50
 testRunner.And("que estou na tela de edição dos parâmetros da regra \"Transações para o mesmo favo" +
                    "recido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 51
 testRunner.And("altero um ou mais parâmetros da regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 52
 testRunner.And("aciono o comando \"Salvar\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 53
 testRunner.Then("o sistema deve atualizar os parâmetros da regra", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 54
 testRunner.And("deve aplicar a data/hora de corte na regra", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 55
 testRunner.And("o sistema deve redirecionar para a tela \"Regras de alertas\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 56
 testRunner.And("o sistema deve apresentar a mensagem \"Os parâmetros da regra foram editados com s" +
                    "ucesso e como houve alterações, a seguinte data/hora de corte foi adicionada: DD" +
                    "/MM/AAAA às HH:MM:SS. A partir de agora os alertas serão gerados baseados nestes" +
                    " parâmetros.\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 57
 testRunner.And("deve gravar o log de \"Alteração\" da regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Não editar parâmetros da regra \"Transações para o mesmo favorecido\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void NaoEditarParametrosDaRegraTransacoesParaOMesmoFavorecido()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Não editar parâmetros da regra \"Transações para o mesmo favorecido\"", null, new string[] {
                        "automatizar"});
#line 60
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 61
 testRunner.When("clico em \"Editar\" na regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 62
 testRunner.And("o sistema apresenta a tela de edição da regra \"Transações para o mesmo favorecido" +
                    "\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 63
 testRunner.And("clico em \"Salvar\" sem que tenha realizado alterações", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 64
 testRunner.Then("o sistema apresenta a tela \"Regras de alertas\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 65
 testRunner.And("o sistema deve apresentar a mensagem \"Como não houve alterações nos parâmetros da" +
                    " regra, a data/hora de corte não foi atualizada, permanecendo: DD/MM/AAAA às HH:" +
                    "MM:SS.\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 66
 testRunner.And("não devem ter sido realiazadas alterações na regra \"Transações para o mesmo favor" +
                    "ecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Parâmetros obrigatórios não preenchidos na edição dos parâmetros da regra \"Transa" +
            "ções para o mesmo favorecido\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void ParametrosObrigatoriosNaoPreenchidosNaEdicaoDosParametrosDaRegraTransacoesParaOMesmoFavorecido()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Parâmetros obrigatórios não preenchidos na edição dos parâmetros da regra \"Transa" +
                    "ções para o mesmo favorecido\"", null, new string[] {
                        "automatizar"});
#line 69
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 70
 testRunner.When("clico em \"Editar\" na regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 71
 testRunner.And("o sistema apresenta a tela de edição da regra \"Transações para o mesmo favorecido" +
                    "\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 72
 testRunner.And("não preencher todos os parâmetros obrigatórios", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 73
 testRunner.And("clico em \"Salvar\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 74
 testRunner.Then("o sistema deve apresentar a mensagem \"É obrigatório selecionar pelo menos um tipo" +
                    " de conta.\" no campo <Tipos de contas>", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 75
 testRunner.And("o sistema deve apresentar a mensagem “O preenchimento do campo <Nome do campo> é " +
                    "obrigatório.” nos demais campos obrigatórios não preenchidos", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Cancelar edição dos parâmetros da regra \"Transações para o mesmo favorecido\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void CancelarEdicaoDosParametrosDaRegraTransacoesParaOMesmoFavorecido()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Cancelar edição dos parâmetros da regra \"Transações para o mesmo favorecido\"", null, new string[] {
                        "automatizar"});
#line 78
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 79
 testRunner.When("clico em \"Editar\" na regra \"Transações para o mesmo favorecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 80
 testRunner.And("o sistema apresenta a tela de edição da regra \"Transações para o mesmo favorecido" +
                    "\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 81
 testRunner.And("clico em \"Cancelar\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 82
 testRunner.Then("o sistema apresenta a tela \"Regras de alertas\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 83
 testRunner.And("não devem ter sido realiazadas alterações na regra \"Transações para o mesmo favor" +
                    "ecido\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
