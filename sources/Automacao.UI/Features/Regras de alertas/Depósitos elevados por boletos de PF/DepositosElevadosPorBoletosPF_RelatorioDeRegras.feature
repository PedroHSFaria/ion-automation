﻿#language: pt-br

Funcionalidade: Depósitos elevados por boletos de PF - Relatório de regras
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder extrair o relatório de regras
	Para saber quais são parâmetros atuais da "Depósitos elevados por boletos de PF"

Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"
	E o sistema apresenta a tela "Regras de alertas"

@automatizar
Cenario: Exportar relatório de regras
	Quando clico em "exportar para Excel"
	Então o sistema efetua o download do relatório de regras no formato ".xls", contendo os parâmetros atualizados da regra "Depósitos elevados por boletos de PF"
	E me mantêm na tela "Regras de alertas"