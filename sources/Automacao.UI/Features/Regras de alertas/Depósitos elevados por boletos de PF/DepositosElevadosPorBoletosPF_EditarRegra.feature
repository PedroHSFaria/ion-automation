﻿#language: pt-br

Funcionalidade: Depósitos elevados por boletos de PF - Editar regra
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder editar os parâmetros da regra  “Depósitos elevados por boletos de PF”
	Para que os alertas gerados sejam mais efetivos
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"
	E o sistema apresenta a tela "Regras de alertas"

@automatizar
Cenario: Ativar a regra "Depósitos elevados por boletos de PF"
	Quando a regra "Depósitos elevados por boletos de PF" está inativa
	E aciono o comando "Ativar" da regra "Depósitos elevados por boletos de PF"
	Então o sistema deve "ativar" a regra "Depósitos elevados por boletos de PF"
	E deve aplicar a data/hora de corte na regra "Depósitos elevados por boletos de PF"
	E o sistema deve apresentar a mensagem "Como a regra foi ativada, a seguinte data/hora de corte foi adicionada: DD/MM/AAAA às HH:MM:SS."
	E o sistema deve gravar o log de "Ativação" da regra "Depósitos elevados por boletos de PF" 

@automatizar
Cenario: Inativar a regra "Depósitos elevados por boletos de PF"
	Quando a regra "Depósitos elevados por boletos de PF" está ativa
	E aciono o comando "Inativar" da regra "Depósitos elevados por boletos de PF"
	Então o sistema deve "inativar" a regra "Depósitos elevados por boletos de PF"
	E deve gravar o log de "Inativação" da regra "Depósitos elevados por boletos de PF" 

@automatizar
Cenario: Apresentar os parâmetros da regra "Depósitos elevados por boletos de PF"
	Quando clico em "Editar" na regra "Depósitos elevados por boletos de PF"
	Então o sistema apresenta a tela de edição da regra "Depósitos elevados por boletos de PF"
	E deve apresentar os seguintes parâmetros
	| NOME DO PARÂMETRO                  |
	| Gravidade                          |
	| Período de efetuação dos depósitos |
	| Qtde. de depósitos efetuados       |
	| Valor de cada depósito (R$)        |
	| Período de abertura da conta       |
	| Tipos de contas                    |  

@automatizar
Cenario: Editar parâmetros da regra "Depósitos elevados por boletos de PF" 
	Quando clico em "Editar" na regra "Depósitos elevados por boletos de PF"
	E o sistema apresenta a tela de edição da regra "Depósitos elevados por boletos de PF"
	E que estou na tela de edição dos parâmetros da regra "Depósitos elevados por boletos de PF"
	E altero um ou mais parâmetros da regra "Depósitos elevados por boletos de PF"
	E aciono o comando "Salvar"
	Então o sistema deve atualizar os parâmetros da regra
	E deve aplicar a data/hora de corte na regra
	E o sistema deve redirecionar para a tela "Regras de alertas"
	E o sistema deve apresentar a mensagem "Os parâmetros da regra foram editados com sucesso e como houve alterações, a seguinte data/hora de corte foi adicionada: DD/MM/AAAA às HH:MM:SS. A partir de agora os alertas serão gerados baseados nestes parâmetros."
	E deve gravar o log de "Alteração" da regra "Depósitos elevados por boletos de PF"

@automatizar
Cenario: Não editar parâmetros da regra "Depósitos elevados por boletos de PF"
	Quando clico em "Editar" na regra "Depósitos elevados por boletos de PF"
	E o sistema apresenta a tela de edição da regra "Depósitos elevados por boletos de PF"
	E clico em "Salvar" sem que tenha realizado alterações
	Então o sistema apresenta a tela "Regras de alertas"
	E o sistema deve apresentar a mensagem "Como não houve alterações nos parâmetros da regra, a data/hora de corte não foi atualizada, permanecendo: DD/MM/AAAA às HH:MM:SS."
	E não devem ter sido realiazadas alterações na regra "Depósitos elevados por boletos de PF"

@automatizar
Cenario: Parâmetros obrigatórios não preenchidos na edição dos parâmetros da regra "Depósitos elevados por boletos de PF"
	Quando clico em "Editar" na regra "Depósitos elevados por boletos de PF"
	E o sistema apresenta a tela de edição da regra "Depósitos elevados por boletos de PF"
	E não preencher todos os parâmetros obrigatórios
	E clico em "Salvar"
	Então o sistema deve apresentar a mensagem "É obrigatório selecionar pelo menos um tipo de conta." no campo <Tipos de contas> 
	E o sistema deve apresentar a mensagem “O preenchimento do campo <Nome do campo> é obrigatório.” nos demais campos obrigatórios não preenchidos

@automatizar
Cenario: Cancelar edição dos parâmetros da regra "Depósitos elevados por boletos de PF"
	Quando clico em "Editar" na regra "Depósitos elevados por boletos de PF"
	E o sistema apresenta a tela de edição da regra "Depósitos elevados por boletos de PF"
	E clico em "Cancelar"
	Então o sistema apresenta a tela "Regras de alertas"
	E não devem ter sido realiazadas alterações na regra "Depósitos elevados por boletos de PF"

#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/61965226
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# CAMPOS DA TELA "EDITAR PARÂMETROS":
#
#	Gravidade
#		| PARAMETRO | TIPO     | VALORES                           | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL                 |
#		| Gravidade | Combobox | "Baixa", "Média", "Alta" e "Info" | Sim         | Não          | N/A     | N/A         | Valor que já está em produção | 
#
#	Período de efetuação dos depósitos
#		| PARAMETRO          | TIPO     | VALORES                          | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER        | VALOR_INICIAL                 |
#		| Sinal              | String   | "Menor ou igual a"               | Sim         | Sim          | N/A     | N/A                | Valor que já está em produção |
#		| Quantidade (tempo) | Integer  | Qualquer valor inteiro           | Sim         | Não          | N/A     | Quantidade (tempo) | Valor que já está em produção |
#		| Tempo              | Combobox | "Segundos", "Minutos" ou "Horas" | Sim         | Não          | N/A     | N/A                | Valor que já está em produção |
#		
#	Qtde. de depósitos efetuados   
#		| PARAMETRO               | TIPO     | VALORES                         | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER             | VALOR_INICIAL                 |
#		| Sinal                   | Combobox | "Maior que" e Maior ou igual a" | Sim         | Não          | N/A     | N/A                     | Valor que já está em produção |
#		| Quantidade (depósitos)  | Integer  | Qualquer valor inteiro          | Sim         | Não          | N/A     | Quantidade (depósitos)  | Valor que já está em produção |
#
#	Valor de cada depósito (R$)    
#		| PARAMETRO  | TIPO     | VALORES                          | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL                 |
#		| Sinal      | Combobox | "Maior que" e "Maior ou igual a" | Sim         | Não          | N/A     | N/A         | Valor que já está em produção |
#		| Valor (R$) | String   | Qualquer valor real              | Sim         | Não          | R$ 0,00 | Valor (R$)  | Valor que já está em produção |
#	
#	Período de abertura da conta 
#		| PARAMETRO          | TIPO    | VALORES                | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER        | VALOR_INICIAL |
#		| Sinal              | String  | "Menor que"            | Sim         | Sim          | N/A     | N/A                | Menor que     |
#		| Quantidade (tempo) | Integer | Qualquer valor inteiro | Sim         | Não          | N/A     | Quantidade (tempo) | 90            |
#		| Tempo              | String  | "Dias"                 | Sim         | Sim          | N/A     | N/A                | Dias          |
#		
#	Tipos de contas 
#		| PARAMETRO    | TIPO    | VALORES                                                                | OBRIGATORIO | DESABILITADO | MASCARA                                                                    | PLACEHOLDER | VALOR_INICIAL                 |
#		| Disponíveis  | LisBox  | Tipos de contas que não foram transferidos para o campo <Selecionados> | Não         | Não          | Código do tipo da conta (com 3 dígitos "000") – Descrição do tipo da conta | N/A         | Valor que já está em produção |
#		| Selecionados | ListBox | Tipos de contas que foram transferidos do campo <Disponíveis>          | Sim         | Não          | Código do tipo da conta (com 3 dígitos "000") – Descrição do tipo da conta | N/A         | Valor que já está em produção |
#
#	bloquear contas automaticamente
#		| TIPO     | VALOR_INICIAL                 |
#		| Checkbox | Valor que já está em produção |
#
#	Informe se deseja ou não notificar o titular da conta
#		| PARAMETRO                             | TIPO     | VALOR_INICIAL                 |
#		| Notificar o titular através de push   | Checkbox | Valor que já está em produção |
#		| Notificar o titular através de e-mail | Checkbox | Valor que já está em produção |
#
#
# EXEMPLO DO LOG DE "ATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                               | VALOR_ANTERIOR_A_TRANSACAO           | VALOR_POSTERIOR_A_TRANSACAO          |
#	| Produto                                        | Aplicativo                           | Aplicativo                           |
#	| Funcionalidade                                 | Regras                               | Regras                               |
#	| Transação                                      | Ativação de regra                    | Ativação de regra                    |
#	| Analista                                       | BSI90896 - José Silva                | BSI90896 - José Silva                |
#	| Data/Hora da transação                         | 30/09/2019 às 15:30:21               | 30/09/2019 às 15:30:21               |
#	| Regra                                          | Depósitos elevados por boletos de PF | Depósitos elevados por boletos de PF |
#	| Situação                                       | Inativa                              | Ativa                                |
#	| Gravidade                                      | Média                                | Média                                |
#	| Operação do período de efetuação dos depósitos | Menor ou igual a                     | Menor ou igual a                     |
#	| Período de efetuação dos depósitos             | 12                                   | 12                                   |
#	| Tempo do período de efetuação dos depósitos    | Horas                                | Horas                                |
#	| Operação da qtde. de depósitos efetuados       | Maior ou igual a                     | Maior ou igual a                     |
#	| Qtde. de depósitos efetuados                   | 2                                    | 2                                    |
#	| Operação do valor de cada depósito (R$)        | Maior ou igual a                     | Maior ou igual a                     |
#	| Valor de cada depósito (R$)                    | R$ 500,00                            | R$ 500,00                            |
#	| Operação do período de abertura da conta       | Maior que                            | Maior que                            |
#	| Período de abertura da conta                   | 90                                   | 90                                   |
#	| Tempo do período de abertura da conta          | Dias                                 | Dias                                 |
#	| Tipos de contas selecionados                   | 240 – Pessoa Física Digital          | 240 – Pessoa Física Digital          |
#	| Bloqueio automático                            | Ativo                                | Ativo                                |
#	| Notificar titular através de push              | Inativo                              | Inativo                              |
#	| Notificar titular através de e-mail            | Inativo                              | Inativo                              |
#
#
# EXEMPLO DO LOG DE "INATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                               | VALOR_ANTERIOR_A_TRANSACAO           | VALOR_POSTERIOR_A_TRANSACAO          |
#	| Produto                                        | Aplicativo                           | Aplicativo                           |
#	| Funcionalidade                                 | Regras                               | Regras                               |
#	| Transação                                      | Inativação de regra                  | Inativação de regra                  |
#	| Analista                                       | BSI90896 - José Silva                | BSI90896 - José Silva                |
#	| Data/Hora da transação                         | 30/09/2019 às 15:30:21               | 30/09/2019 às 15:30:21               |
#	| Regra                                          | Depósitos elevados por boletos de PF | Depósitos elevados por boletos de PF |
#	| Situação                                       | Ativa                                | Inativa                              |
#	| Gravidade                                      | Média                                | Média                                |
#	| Operação do período de efetuação dos depósitos | Menor ou igual a                     | Menor ou igual a                     |
#	| Período de efetuação dos depósitos             | 12                                   | 12                                   |
#	| Tempo do período de efetuação dos depósitos    | Horas                                | Horas                                |
#	| Operação da qtde. de depósitos efetuados       | Maior ou igual a                     | Maior ou igual a                     |
#	| Qtde. de depósitos efetuados                   | 2                                    | 2                                    |
#	| Operação do valor de cada depósito (R$)        | Maior ou igual a                     | Maior ou igual a                     |
#	| Valor de cada depósito (R$)                    | R$ 500,00                            | R$ 500,00                            |
#	| Operação do período de abertura da conta       | Maior que                            | Maior que                            |
#	| Período de abertura da conta                   | 90                                   | 90                                   |
#	| Tempo do período de abertura da conta          | Dias                                 | Dias                                 |
#	| Tipos de contas selecionados                   | 240 – Pessoa Física Digital          | 240 – Pessoa Física Digital          |
#	| Bloqueio automático                            | Ativo                                | Ativo                                |
#	| Notificar titular através de push              | Inativo                              | Inativo                              |
#	| Notificar titular através de e-mail            | Inativo                              | Inativo                              |
#
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                               | VALOR_ANTERIOR_A_TRANSACAO           | VALOR_POSTERIOR_A_TRANSACAO          |
#	| Produto                                        | Aplicativo                           | Aplicativo                           |
#	| Funcionalidade                                 | Regras                               | Regras                               |
#	| Transação                                      | Alteração de regra                   | Alteração de regra                   |
#	| Analista                                       | BSI90896 - José Silva                | BSI90896 - José Silva                |
#	| Data/Hora da transação                         | 30/09/2019 às 15:30:21               | 30/09/2019 às 15:30:21               |
#	| Regra                                          | Depósitos elevados por boletos de PF | Depósitos elevados por boletos de PF |
#	| Situação                                       | Inativa                              | Ativa                                |
#	| Gravidade                                      | Média                                | Média                                |
#	| Operação do período de efetuação dos depósitos | Menor ou igual a                     | Menor ou igual a                     |
#	| Período de efetuação dos depósitos             | 12                                   | 12                                   |
#	| Tempo do período de efetuação dos depósitos    | Horas                                | Horas                                |
#	| Operação da qtde. de depósitos efetuados       | Maior ou igual a                     | Maior ou igual a                     |
#	| Qtde. de depósitos efetuados                   | 2                                    | 2                                    |
#	| Operação do valor de cada depósito (R$)        | Maior ou igual a                     | Maior ou igual a                     |
#	| Valor de cada depósito (R$)                    | R$ 500,00                            | R$ 1.0000,00                         |
#	| Operação do período de abertura da conta       | Maior que                            | Maior que                            |
#	| Período de abertura da conta                   | 90                                   | 90                                   |
#	| Tempo do período de abertura da conta          | Dias                                 | Dias                                 |
#	| Tipos de contas selecionados                   | 240 – Pessoa Física Digital          | 240 – Pessoa Física Digital          |
#	| Bloqueio automático                            | Ativo                                | Ativo                                |
#	| Notificar titular através de push              | Inativo                              | Inativo                              |
#	| Notificar titular através de e-mail            | Inativo                              | Inativo                              |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################