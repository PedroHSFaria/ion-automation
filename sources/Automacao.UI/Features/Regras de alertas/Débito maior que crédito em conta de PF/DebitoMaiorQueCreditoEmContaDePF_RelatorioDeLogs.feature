﻿#language: pt-br

Funcionalidade: Débito maior que crédito em conta de PF - Relatório de log
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder extrair o relatório de log
	Para saber quando a regra "Débito maior que crédito em conta de PF" foi ativada, inativa e/ou alterada
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"

@automatizar
Cenario: Relatório de log de ativação a regra "Débito maior que crédito em conta de PF"
	Quando a regra "Débito maior que crédito em conta de PF" está inativa
	E aciono o comando "Ativar" da regra "Débito maior que crédito em conta de PF"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de inativação a regra "Débito maior que crédito em conta de PF"
	Quando a regra "Débito maior que crédito em conta de PF" está ativa
	E aciono o comando "Inativar" da regra "Débito maior que crédito em conta de PF"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra 
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de alteração d regra "Débito maior que crédito em conta de PF"
	Quando clico em "Editar" na regra "Débito maior que crédito em conta de PF"
	E altero um ou mais parâmetros da regra "Débito maior que crédito em conta de PF"
	E clico em "Salvar"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"
	
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# EXEMPLO DO LOG DE "ATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                           | VALOR_ANTERIOR_A_TRANSACAO              | VALOR_POSTERIOR_A_TRANSACAO             |
#	| Produto                                                    | Aplicativo                              | Aplicativo                              |
#	| Funcionalidade                                             | Regras                                  | Regras                                  |
#	| Transação                                                  | Ativação de regra                       | Ativação de regra                       |
#	| Analista                                                   | BSI90896 - José Silva                   | BSI90896 - José Silva                   |
#	| Data/Hora da transação                                     | 30/09/2019 às 15:30:21                  | 30/09/2019 às 15:30:21                  |
#	| Regra                                                      | Débito maior que crédito em conta de PF | Débito maior que crédito em conta de PF |
#	| Situação                                                   | Inativa                                 | Ativa                                   |
#	| Gravidade                                                  | Média                                   | Média                                   |
#	| Operação do período de efetuação das movimentações         | Menor ou igual a                        | Menor ou igual a                        |
#	| Período de efetuação das movimentações                     | 12                                      | 12                                      |
#	| Tempo do período de efetuação das movimentações            | Horas                                   | Horas                                   |
#	| Operação da qtde. de transações efetuadas                  | Maior ou igual a                        | Maior ou igual a                        |
#	| Débito com valor acima de (%)                              | 50                                      | 50                                      |
#	| Operação do somatório dos valores de crédito em conta (R$) | Maior ou igual a                        | Maior ou igual a                        |
#	| Somatório dos valores de crédito em conta (R$)             | R$ 500,00                               | R$ 500,00                               |
#	| Operação do período de abertura da conta                   | Maior que                               | Maior que                               |
#	| Período de abertura da conta                               | 90                                      | 90                                      |
#	| Tempo do período de abertura da conta                      | Dias                                    | Dias                                    |
#	| Tipos de contas selecionados                               | 240 – Pessoa Física Digital             | 240 – Pessoa Física Digital             |
#	| Bloqueio automático                                        | Ativo                                   | Ativo                                   |
#	| Notificar titular através de push                          | Inativo                                 | Inativo                                 |
#	| Notificar titular através de e-mail                        | Inativo                                 | Inativo                                 |
#
#
# EXEMPLO DO LOG DE "INATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                           | VALOR_ANTERIOR_A_TRANSACAO              | VALOR_POSTERIOR_A_TRANSACAO             |
#	| Produto                                                    | Aplicativo                              | Aplicativo                              |
#	| Funcionalidade                                             | Regras                                  | Regras                                  |
#	| Transação                                                  | Ativação de regra                       | Ativação de regra                       |
#	| Analista                                                   | BSI90896 - José Silva                   | BSI90896 - José Silva                   |
#	| Data/Hora da transação                                     | 30/09/2019 às 15:30:21                  | 30/09/2019 às 15:30:21                  |
#	| Regra                                                      | Débito maior que crédito em conta de PF | Débito maior que crédito em conta de PF |
#	| Situação                                                   | Ativa                                   | Inativa                                 |
#	| Gravidade                                                  | Média                                   | Média                                   |
#	| Operação do período de efetuação das movimentações         | Menor ou igual a                        | Menor ou igual a                        |
#	| Período de efetuação das movimentações                     | 12                                      | 12                                      |
#	| Tempo do período de efetuação das movimentações            | Horas                                   | Horas                                   |
#	| Operação da qtde. de transações efetuadas                  | Maior ou igual a                        | Maior ou igual a                        |
#	| Débito com valor acima de (%)                              | 50                                      | 50                                      |
#	| Operação do somatório dos valores de crédito em conta (R$) | Maior ou igual a                        | Maior ou igual a                        |
#	| Somatório dos valores de crédito em conta (R$)             | R$ 500,00                               | R$ 500,00                               |
#	| Operação do período de abertura da conta                   | Maior que                               | Maior que                               |
#	| Período de abertura da conta                               | 90                                      | 90                                      |
#	| Tempo do período de abertura da conta                      | Dias                                    | Dias                                    |
#	| Tipos de contas selecionados                               | 240 – Pessoa Física Digital             | 240 – Pessoa Física Digital             |
#	| Bloqueio automático                                        | Ativo                                   | Ativo                                   |
#	| Notificar titular através de push                          | Inativo                                 | Inativo                                 |
#	| Notificar titular através de e-mail                        | Inativo                                 | Inativo                                 |
#
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                           | VALOR_ANTERIOR_A_TRANSACAO              | VALOR_POSTERIOR_A_TRANSACAO             |
#	| Produto                                                    | Aplicativo                              | Aplicativo                              |
#	| Funcionalidade                                             | Regras                                  | Regras                                  |
#	| Transação                                                  | Alteração de regra                      | Alteração de regra                      |
#	| Analista                                                   | BSI90896 - José Silva                   | BSI90896 - José Silva                   |
#	| Data/Hora da transação                                     | 30/09/2019 às 15:30:21                  | 30/09/2019 às 15:30:21                  |
#	| Regra                                                      | Débito maior que crédito em conta de PF | Débito maior que crédito em conta de PF |
#	| Situação                                                   | Inativa                                 | Ativa                                   |
#	| Gravidade                                                  | Média                                   | Média                                   |
#	| Operação do período de efetuação das movimentações         | Menor ou igual a                        | Menor ou igual a                        |
#	| Período de efetuação das movimentações                     | 12                                      | 12                                      |
#	| Tempo do período de efetuação das movimentações            | Horas                                   | Horas                                   |
#	| Operação da qtde. de transações efetuadas                  | Maior ou igual a                        | Maior ou igual a                        |
#	| Débito com valor acima de (%)                              | 50                                      | 50                                      |
#	| Operação do somatório dos valores de crédito em conta (R$) | Maior ou igual a                        | Maior ou igual a                        |
#	| Somatório dos valores de crédito em conta (R$)             | R$ 500,00                               | R$ 1.000,00                             |
#	| Operação do período de abertura da conta                   | Maior que                               | Maior que                               |
#	| Período de abertura da conta                               | 90                                      | 90                                      |
#	| Tempo do período de abertura da conta                      | Dias                                    | Dias                                    |
#	| Tipos de contas selecionados                               | 240 – Pessoa Física Digital             | 240 – Pessoa Física Digital             |
#	| Bloqueio automático                                        | Ativo                                   | Ativo                                   |
#	| Notificar titular através de push                          | Inativo                                 | Inativo                                 |
#	| Notificar titular através de e-mail                        | Inativo                                 | Inativo                                 |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################