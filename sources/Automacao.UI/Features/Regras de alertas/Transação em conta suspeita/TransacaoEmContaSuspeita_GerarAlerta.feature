﻿#language: pt-br

Funcionalidade: Transação em conta suspeita - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física cuja conta está cadastrada na lista de "Contas suspeitas" efetuar transações que atendam aos parâmetros da regra “Transação em conta suspeita”, utilizando o Aplicativo do BS2
	Para que possa tratar ou analisar a possível fraude

@automatizar
Esquema do Cenário: Gerar alerta de fraude de "Transação em conta suspeita"
	Dado que são efetuadas <transacoes> em contas cadastradas na lista "Contas suspeitas" que se encaixam nas seguintes características
      | CARACTERISTICA                                                                 | PARAMETRO                          |
      | Qtde. de transações efetuadas                                                  | Quantidade parametrizada na regra  |
      | Valor de cada transação (R$)                                                   | Valor parametrizado na regra       |
      | Período de abertura da conta                                                   | Período parametrizado na regra     |
      | Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias) | Período parametrizado na regra     |
      | Transações                                                                     | Transações parametrizadas na regra |
      | Tipos de contas                                                                | Tipo parametrizado na regra        |
   Quando o sistema validar as <transacoes>
   Então deve ser gerado um alerta de fraude da regra “Transação em conta suspeita” com situação igual a "Aguardando análise”
   Exemplos: 
	| transacoes                  |
	| TED                         |
	| Transferência interna       |
	| Pagamento de boleto         |
	| Pagamento de concessionária |
	| Aplicação                   |
	| Resgate de investimento     |

@automatizar
Esquema do Cenário: Gerar novo alerta de fraude de "Transação em conta suspeita" quando o anterior já foi analisado como "Fraude"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Transação em conta suspeita"
    E a situação do alerta é igual a "Fraude"
    Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então deve ser gerado um alerta de fraude da regra “Transação em conta suspeita” com situação igual a "Aguardado análise”    
   Exemplos: 
	| transacoes                  |
	| TED                         |
	| Transferência interna       |
	| Pagamento de boleto         |
	| Pagamento de concessionária |
	| Aplicação                   |
	| Resgate de investimento     |

@automatizar
Esquema do Cenário: Gerar novo alerta de fraude de "Transação em conta suspeita" quando o anterior já foi analisado como "Não fraude"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Transação em conta suspeita"
    E a situação do alerta é igual a "Não fraude"
    Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então deve ser gerado um alerta de fraude da regra “Transação em conta suspeita” com situação igual a "Aguardado análise”    
   Exemplos: 
	| transacoes                  |
	| TED                         |
	| Transferência interna       |
	| Pagamento de boleto         |
	| Pagamento de concessionária |
	| Aplicação                   |
	| Resgate de investimento     |

@automatizar
Esquema do Cenario: Não gerar alerta de fraude de "Transação em conta suspeita"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Transação em conta suspeita"
	E a análise do alerta não foi realizada
	Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então não deve ser gerado alerta de fraude da regra “Transação em conta suspeita”
	E as novas <transacoes> devem ser adicionadas no alerta já existente 
   Exemplos: 
	| transacoes                  |
	| TED                         |
	| Transferência interna       |
	| Pagamento de boleto         |
	| Pagamento de concessionária |
	| Aplicação                   |
	| Resgate de investimento     |

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "Transação em conta suspeita"
	Dado que o sistema gerou um alerta de “Transação em conta suspeita”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Transação em conta suspeita"

@automatizar
Cenario: Ver transações de um alerta de fraude de "Transação em conta suspeita"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Transação em conta suspeita"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/61965230
#
############################################ OBSERVAÇÕES ########################################################	
#	
#		As transações do Digital devem ser recuperadas das seguintes tabelas:
#
#			| Transação           			| Tabela 							|  
#			| TED 							| tra.TRANS_APP_TED 				|
#			| Transferência interna 		| tra.TRANS_APP_TRANSFER			|
#			| Pagamento de boleto 			| tra.TRANS_APP_BILL				| 
#			| Pagamento de concessionária	| tra.TRANS_APP_CONCESSIONAIRES 	|
#			| Aplicacação 					| tra.TRANS_APP_APPLICATION			|
#			| Resgate de investimento 		| tra.TRANS_APP_RESCUE				|
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = '333182'
#
########################################## FIM DAS OBSERVAÇÕES #################################################
