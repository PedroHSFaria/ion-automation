﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Automacao.UI.Features.RegrasDeAlertas.PagamentosDeBoletosDePF
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Pagamentos de boletos de PF - Gerar alerta")]
    public partial class PagamentosDeBoletosDePF_GerarAlertaFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "PagamentosDeBoletosDePF_GerarAlerta.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("pt-br"), "Pagamentos de boletos de PF - Gerar alerta", @"	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física efetuar transações que atendam aos parâmetros da regra “Pagamentos de boletos de PF”, utilizando o Aplicativo do BS2
	Para que possa tratar ou analisar a possível fraude", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Gerar alerta de fraude de \"Pagamentos de boletos de PF\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void GerarAlertaDeFraudeDePagamentosDeBoletosDePF()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Gerar alerta de fraude de \"Pagamentos de boletos de PF\"", null, new string[] {
                        "automatizar"});
#line 9
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line hidden
            TechTalk.SpecFlow.Table table61 = new TechTalk.SpecFlow.Table(new string[] {
                        "CARACTERISTICA",
                        "PARAMETRO"});
            table61.AddRow(new string[] {
                        "Período de efetuação dos pagamentos",
                        "Período parametrizado na regra"});
            table61.AddRow(new string[] {
                        "Qtde. de boletos pagos",
                        "Quantidade parametrizada na regra"});
            table61.AddRow(new string[] {
                        "Valor de cada pagamento (R$)",
                        "Valor parametrizado na regra"});
            table61.AddRow(new string[] {
                        "Período de abertura da conta",
                        "Período parametrizado na regra"});
            table61.AddRow(new string[] {
                        "Período do último crédito em conta",
                        "Período parametrizado na regra"});
            table61.AddRow(new string[] {
                        "Transações",
                        "Transações parametrizada na regra"});
            table61.AddRow(new string[] {
                        "Tipos de contas",
                        "Tipo parametrizado na regra"});
#line 10
 testRunner.Given("que são efetuados \"pagamentos de boletos\" que se encaixam nas seguintes caracterí" +
                    "sticas", ((string)(null)), table61, "Dado ");
#line 19
   testRunner.When("o sistema validar os \"pagamentos de boletos\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 20
   testRunner.Then("deve ser gerado um alerta de fraude da regra “Pagamentos de boletos de PF” com si" +
                    "tuação igual a \"Aguardando análise\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Gerar novo alerta de fraude de \"Pagamentos de boletos de PF\" quando o anterior já" +
            " foi analisado como \"Fraude\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void GerarNovoAlertaDeFraudeDePagamentosDeBoletosDePFQuandoOAnteriorJaFoiAnalisadoComoFraude()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Gerar novo alerta de fraude de \"Pagamentos de boletos de PF\" quando o anterior já" +
                    " foi analisado como \"Fraude\"", null, new string[] {
                        "automatizar"});
#line 23
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 24
    testRunner.Given("que já tenham sido efetuados \"pagamentos de boletos\" que tenham gerado um alerta " +
                    "de fraude da regra \"Pagamentos de boletos de PF\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 25
    testRunner.And("a situação do alerta é igual a \"Fraude\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 26
    testRunner.When("novos \"pagamentos de boletos\" com as mesmas características das que geraram o ale" +
                    "rta de fraude forem efetuados", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 27
 testRunner.Then("deve ser gerado um alerta de fraude da regra “Pagamentos de boletos de PF” com si" +
                    "tuação igual a \"Aguardado análise”", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Gerar novo alerta de fraude de \"Pagamentos de boletos de PF\" quando o anterior já" +
            " foi analisado como \"Não fraude\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void GerarNovoAlertaDeFraudeDePagamentosDeBoletosDePFQuandoOAnteriorJaFoiAnalisadoComoNaoFraude()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Gerar novo alerta de fraude de \"Pagamentos de boletos de PF\" quando o anterior já" +
                    " foi analisado como \"Não fraude\"", null, new string[] {
                        "automatizar"});
#line 30
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 31
    testRunner.Given("que já tenham sido efetuados \"pagamentos de boletos\" que tenham gerado um alerta " +
                    "de fraude da regra \"Pagamentos de boletos de PF\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 32
    testRunner.And("a situação do alerta é igual a \"Não fraude\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 33
    testRunner.When("novos \"pagamentos de boletos\" com as mesmas características das que geraram o ale" +
                    "rta de fraude", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 34
 testRunner.Then("deve ser gerado um alerta de fraude da regra “Pagamentos de boletos de PF” com si" +
                    "tuação igual a \"Aguardado análise”", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Não gerar alerta de fraude de \"Pagamentos de boletos de PF\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void NaoGerarAlertaDeFraudeDePagamentosDeBoletosDePF()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Não gerar alerta de fraude de \"Pagamentos de boletos de PF\"", null, new string[] {
                        "automatizar"});
#line 37
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 38
    testRunner.Given("que já tenham sido efetuados \"pagamentos de boletos\" que tenham gerado um alerta " +
                    "de fraude da regra \"Pagamentos de boletos de PF\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 39
 testRunner.And("a análise do alerta não foi realizada", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 40
 testRunner.When("novos \"pagamentos de boletos\" com as mesmas características das que geraram o ale" +
                    "rta de fraude forem efetuados", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 41
 testRunner.Then("não deve ser gerado alerta de fraude da regra “Pagamentos de boletos de PF”", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 42
 testRunner.And("as novos \"pagamentos de boletos\" devem ser adicionados no alerta já existente", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Ver detalhes de um alerta de fraude de \"Pagamentos de boletos de PF\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void VerDetalhesDeUmAlertaDeFraudeDePagamentosDeBoletosDePF()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Ver detalhes de um alerta de fraude de \"Pagamentos de boletos de PF\"", null, new string[] {
                        "automatizar"});
#line 45
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 46
 testRunner.Given("que o sistema gerou um alerta de “Pagamentos de boletos de PF”", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 47
 testRunner.And("estou na tela \"Alertas de fraudes\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 48
 testRunner.When("acionar o comando \"Ver detalhes\" desse alerta gerado", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 49
 testRunner.Then("o sistema deve apresentar a tela \"Detalhes do alerta\" do alerta de fraude da regr" +
                    "a “Pagamentos de boletos de PF\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Entao ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Ver transações de um alerta de fraude de \"Pagamentos de boletos de PF\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void VerTransacoesDeUmAlertaDeFraudeDePagamentosDeBoletosDePF()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Ver transações de um alerta de fraude de \"Pagamentos de boletos de PF\"", null, new string[] {
                        "automatizar"});
#line 52
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 53
 testRunner.Given("que estou na tela \"Detalhes do alerta\" do alerta de fraude da regra \"Pagamentos d" +
                    "e boletos de PF\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 54
 testRunner.When("acionar o comando \"Ver transações\" desse alerta de fraude", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 55
 testRunner.Then("o sistema deve apresentar a modal “Transações” com os dados das transações efetua" +
                    "das para geração do alerta", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
