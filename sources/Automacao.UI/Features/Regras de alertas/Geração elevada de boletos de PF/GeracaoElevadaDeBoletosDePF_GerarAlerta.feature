﻿#language: pt-br

Funcionalidade: Geração elevada de boletos de PF - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física efetuar transações que atendam aos parâmetros da regra “Geração elevada de boletos de PF”, utilizando o Aplicativo do BS2
	Para que possa tratar ou analisar a possível fraude

@automatizar
Cenário: Gerar alerta de fraude de "Geração elevada de boletos de PF"
  Dado que são efetuadas "gerações de boletos" que se encaixam nas seguintes características
      | CARACTERISTICA                      | PARAMETRO                         |
      | Período de geração dos boletos      | Período parametrizado na regra    |
      | Qtde. de boletos gerados            | Quantidade parametrizada na regra |
      | Período de abertura da conta        | Período parametrizado na regra    |
      | Tipos de contas                     | Tipo parametrizado na regra       |
   Quando o sistema validar as "gerações de boletos"
   Então deve ser gerado um alerta de fraude da regra “Geração elevada de boletos de PF” com situação igual a "Aguardando análise”

@automatizar
Cenário: Gerar novo alerta de fraude de "Geração elevada de boletos de PF" quando o anterior já foi analisado como "Fra
    Dado que já tenham sido efetuadas "gerações de boletos" que tenham gerado um alerta de fraude da regra  "Geração elevada de boletos de PF"
    E a situação do alerta é igual a "Fraude"
    Quando novas "gerações de boletos" com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então deve ser gerado um alerta de fraude da regra “Geração elevada de boletos de PF” com situação igual a "Aguardado análise”    

@automatizar
Cenário: Gerar novo alerta de fraude de "Geração elevada de boletos de PF" quando o anterior já foi analisado
    Dado que já tenham sido efetuadas "gerações de boletos" que tenham gerado um alerta de fraude da regra "Geração elevada de boletos de PF"
    E a situação do alerta é igual a "Não fraude"
    Quando novas "gerações de boletos" com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então deve ser gerado um alerta de fraude da regra “Geração elevada de boletos de PF” com situação igual a "Aguardado análise”    

@automatizar
Cenario: Não gerar alerta de fraude de "Geração elevada de boletos de PF"
    Dado que já tenham sido efetuadas "gerações de boletos" que tenham gerado um alerta de fraude da regra "Geração elevada de boletos de PF"
	E a análise do alerta não foi realizada
	Quando novas "gerações de boletos" com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então não deve ser gerado alerta de fraude da regra “Geração elevada de boletos de PF”
	E as novas "gerações de boletos" devem ser adicionadas no alerta já existente 

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "Geração elevada de boletos de PF"
	Dado que o sistema gerou um alerta de “Geração elevada de boletos de PF”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Geração elevada de boletos de PF"

@automatizar
Cenario: Ver transações de um alerta de fraude de "Geração elevada de boletos de PF"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Geração elevada de boletos de PF"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/61965221
#
############################################## OBSERVAÇÕES #####################################################	
#	
#		As transações do tipo "Geração de boleto" devem ser recuperadas da view "sgrviwboletoentrada" do CCO.	
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = 'Número da conta do titular'
#
########################################## FIM DAS OBSERVAÇÕES #################################################