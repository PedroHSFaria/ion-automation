﻿#language: pt-br

Funcionalidade: Transação fora do horário habitual - Editar regra
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder editar os parâmetros da regra  “Transação fora do horário habitual”
	Para que os alertas gerados sejam mais efetivos
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"
	E o sistema apresenta a tela "Regras de alertas"

@automatizar
Cenario: Ativar a regra "Transação fora do horário habitual"
	Quando a regra "Transação fora do horário habitual" está inativa
	E aciono o comando "Ativar" da regra "Transação fora do horário habitual"
	Então o sistema deve "ativar" a regra "Transação fora do horário habitual"
	E deve aplicar a data/hora de corte na regra "Transação fora do horário habitual"
	E o sistema deve apresentar a mensagem "Como a regra foi ativada, a seguinte data/hora de corte foi adicionada: DD/MM/AAAA às HH:MM:SS."
	E o sistema deve gravar o log de "Ativação" da regra "Transação fora do horário habitual" 

@automatizar
Cenario: Inativar a regra "Transação fora do horário habitual"
	Quando a regra "Transação fora do horário habitual" está ativa
	E aciono o comando "Inativar" da regra "Transação fora do horário habitual"
	Então o sistema deve "inativar" a regra "Transação fora do horário habitual"
	E deve gravar o log de "Inativação" da regra "Transação fora do horário habitual" 

@automatizar
Cenario: Apresentar os parâmetros da regra "Transação fora do horário habitual"
	Quando clico em "Editar" na regra "Transação fora do horário habitual"
	Então o sistema apresenta a tela de edição da regra "Transação fora do horário habitual"
	E deve apresentar os seguintes parâmetros
	| NOME DO PARÂMETRO                                                              |
	| Gravidade                                                                      |
	| Período de efetuação das transações                                            |
	| Qtde. de transações efetuadas                                                  |
	| Valor de cada transação (R$)                                                   |
	| Período de abertura da conta                                                   |
	| Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias) |
	| Transações                                                                     |
	| Tipos de contas                                                                |  

@automatizar
Cenario: Editar parâmetros da regra "Transação fora do horário habitual" 
	Quando clico em "Editar" na regra "Transação fora do horário habitual"
	E o sistema apresenta a tela de edição da regra "Transação fora do horário habitual"
	E que estou na tela de edição dos parâmetros da regra "Transação fora do horário habitual"
	E altero um ou mais parâmetros da regra "Transação fora do horário habitual"
	E aciono o comando "Salvar"
	Então o sistema deve atualizar os parâmetros da regra
	E deve aplicar a data/hora de corte na regra
	E o sistema deve redirecionar para a tela "Regras de alertas"
	E o sistema deve apresentar a mensagem "Os parâmetros da regra foram editados com sucesso e como houve alterações, a seguinte data/hora de corte foi adicionada: DD/MM/AAAA às HH:MM:SS. A partir de agora os alertas serão gerados baseados nestes parâmetros."
	E deve gravar o log de "Alteração" da regra "Transação fora do horário habitual"

@automatizar
Cenario: Não editar parâmetros da regra "Transação fora do horário habitual"
	Quando clico em "Editar" na regra "Transação fora do horário habitual"
	E o sistema apresenta a tela de edição da regra "Transação fora do horário habitual"
	E clico em "Salvar" sem que tenha realizado alterações
	Então o sistema apresenta a tela "Regras de alertas"
	E o sistema deve apresentar a mensagem "Como não houve alterações nos parâmetros da regra, a data/hora de corte não foi atualizada, permanecendo: DD/MM/AAAA às HH:MM:SS."
	E não devem ter sido realiazadas alterações na regra "Transação fora do horário habitual"

@automatizar
Cenario: Parâmetros obrigatórios não preenchidos na edição dos parâmetros da regra "Transação fora do horário habitual"
	Quando clico em "Editar" na regra "Transação fora do horário habitual"
	E o sistema apresenta a tela de edição da regra "Transação fora do horário habitual"
	E não preencher todos os parâmetros obrigatórios
	E clico em "Salvar"
	Então o sistema deve apresentar a mensagem "É obrigatório selecionar pelo menos um tipo de conta." no campo <Tipos de contas> 
	E o sistema deve apresentar a mensagem “O preenchimento do campo <Nome do campo> é obrigatório.” nos demais campos obrigatórios não preenchidos

@automatizar
Cenario: Cancelar edição dos parâmetros da regra "Transação fora do horário habitual"
	Quando clico em "Editar" na regra "Transação fora do horário habitual"
	E o sistema apresenta a tela de edição da regra "Transação fora do horário habitual"
	E clico em "Cancelar"
	Então o sistema apresenta a tela "Regras de alertas"
	E não devem ter sido realiazadas alterações na regra "Transação fora do horário habitual"
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/61965207
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# CAMPOS DA TELA "EDITAR PARÂMETROS":
#
#	Gravidade
#		| PARAMETRO | TIPO     | VALORES                           | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL                 |
#		| Gravidade | Combobox | "Baixa", "Média", "Alta" e "Info" | Sim         | Não          | N/A     | N/A         | Valor que já está em produção | 
#
#	Período de efetuação das transações
#		| PARAMETRO       | TIPO | VALORES          | OBRIGATORIO | DESABILITADO | MASCARA  | PLACEHOLDER | VALOR_INICIAL                 |
#		| Horário inicial | Time | Qualquer horário | Sim         | Não          | HH:MM:SS | N/A         | Valor que já está em produção |
#		| Horário final   | Time | Qualquer horário | Sim         | Não          | HH:MM:SS | N/A         | Valor que já está em produção |	
#
#	Qtde. de transações efetuadas 
#		| PARAMETRO               | TIPO     | VALORES                         | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER             | VALOR_INICIAL                 |
#		| Sinal                   | Combobox | "Maior que" e Maior ou igual a" | Sim         | Não          | N/A     | N/A                     | Valor que já está em produção |
#		| Quantidade (transações) | Integer  | Qualquer valor inteiro          | Sim         | Não          | N/A     | Quantidade (transações) | Valor que já está em produção |
#		
#	Valor de cada transação (R$)    
#		| PARAMETRO  | TIPO     | VALORES                          | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL                 |
#		| Sinal      | Combobox | "Maior que" e "Maior ou igual a" | Sim         | Não          | N/A     | N/A         | Valor que já está em produção |
#		| Valor (R$) | String   | Qualquer valor real              | Sim         | Não          | R$ 0,00 | Valor (R$)  | Valor que já está em produção |
#	
#	Período de abertura da conta 
#		| PARAMETRO          | TIPO    | VALORES                | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER        | VALOR_INICIAL |
#		| Sinal              | String  | "Menor que"            | Sim         | Sim          | N/A     | N/A                | Menor que     |
#		| Quantidade (tempo) | Integer | Qualquer valor inteiro | Sim         | Não          | N/A     | Quantidade (tempo) | 90            |
#		| Tempo              | String  | "Dias"                 | Sim         | Sim          | N/A     | N/A                | Dias          |
#				
#	Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias) 
#		| PARAMETRO          | TIPO    | VALORES                          | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER        | VALOR_INICIAL |
#		| Sinal              | String  | "Maior que" e "Maior ou igual a" | Sim         | Não          | N/A     | N/A                | Maior que     |
#		| Quantidade (tempo) | Integer | Qualquer valor inteiro           | Sim         | Não          | N/A     | Quantidade (tempo) | 2             |
#		| Tempo              | String  | "Dias"                           | Sim         | Sim          | N/A     | N/A                | Dias          |
#
#	Transações 
#		| PARAMETRO    | TIPO    | VALORES                                                                | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL                 |
#		| Disponíveis  | LisBox  | Tipos de contas que não foram transferidos para o campo <Selecionados> | Não         | Não          | N/A     | N/A         | Valor que já está em produção |
#		| Selecionados | ListBox | Tipos de contas que foram transferidos do campo <Disponíveis>          | Sim         | Não          | N/A     | N/A         | Valor que já está em produção |
#
#	Tipos de contas 
#		| PARAMETRO    | TIPO    | VALORES                                                                | OBRIGATORIO | DESABILITADO | MASCARA                                                                    | PLACEHOLDER | VALOR_INICIAL                 |
#		| Disponíveis  | LisBox  | Tipos de contas que não foram transferidos para o campo <Selecionados> | Não         | Não          | Código do tipo da conta (com 3 dígitos "000") – Descrição do tipo da conta | N/A         | Valor que já está em produção |
#		| Selecionados | ListBox | Tipos de contas que foram transferidos do campo <Disponíveis>          | Sim         | Não          | Código do tipo da conta (com 3 dígitos "000") – Descrição do tipo da conta | N/A         | Valor que já está em produção |
#
#	bloquear contas automaticamente
#		| TIPO     | VALOR_INICIAL                 |
#		| Checkbox | Valor que já está em produção |
#
#	Informe se deseja ou não notificar o titular da conta
#		| PARAMETRO                             | TIPO     | VALOR_INICIAL                 |
#		| Notificar o titular através de push   | Checkbox | Valor que já está em produção |
#		| Notificar o titular através de e-mail | Checkbox | Valor que já está em produção |
#
#
# EXEMPLO DO LOG DE "ATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO        |
#	| Produto                                                              | Aplicativo                         | Aplicativo                         |
#	| Funcionalidade                                                       | Regras                             | Regras                             |
#	| Transação                                                            | Ativação de regra                  | Ativação de regra                  |
#	| Analista                                                             | BSI90896 - José Silva              | BSI90896 - José Silva              |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21             |
#	| Regra                                                                | Transação fora do horário habitual | Transação fora do horário habitual |
#	| Situação                                                             | Inativa                            | Ativa                              |
#	| Gravidade                                                            | Média                              | Média                              |
#	| Horário inicial de efetuação das transações                          | 00:00:00                           | 00:00:00                           |
#	| Horário final de efetuação das transações                            | 04:00:00                           | 04:00:00                           |
#	| Operação da qtde. de transações efetuadas                            | Maior ou igual a                   | Maior ou igual a                   |
#	| Qtde. de transções efetuadas                                         | 2                                  | 2                                  |
#	| Operação do valor de cada transação (R$)                             | Maior ou igual a                   | Maior ou igual a                   |
#	| Valor de cada transação (R$)                                         | R$ 500,00                          | R$ 500,00                          |
#	| Operação do período de abertura da conta                             | Maior que                          | Maior que                          |
#	| Período de abertura da conta                                         | 90                                 | 90                                 |
#	| Tempo do período de abertura da conta                                | Dias                               | Dias                               |
#	| Operação do período que o titular não transacionou para o favorecido | Maior que                          | Maior que                          |
#	| Período que o titular não transacionou para o favorecido             | 2                                  | 2                                  |
#	| Tempo do período que o titular não transacionou para o favorecido    | Dias                               | Dias                               |
#	| Transações                                                           | TED                                | TED                                |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital        |
#	| Bloqueio automático                                                  | Ativo                              | Ativo                              |
#	| Notificar titular através de push                                    | Inativo                            | Inativo                            |
#	| Notificar titular através de e-mail                                  | Inativo                            | Inativo                            |
#
#
# EXEMPLO DO LOG DE "INATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO        |
#	| Produto                                                              | Aplicativo                         | Aplicativo                         |
#	| Funcionalidade                                                       | Regras                             | Regras                             |
#	| Transação                                                            | Inativação de regra                | Inativação de regra                |
#	| Analista                                                             | BSI90896 - José Silva              | BSI90896 - José Silva              |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21             |
#	| Regra                                                                | Transação fora do horário habitual | Transação fora do horário habitual |
#	| Situação                                                             | Ativa                              | Inativa                            |
#	| Gravidade                                                            | Média                              | Média                              |
#	| Horário inicial de efetuação das transações                          | 00:00:00                           | 00:00:00                           |
#	| Horário final de efetuação das transações                            | 04:00:00                           | 04:00:00                           |
#	| Operação da qtde. de transações efetuadas                            | Maior ou igual a                   | Maior ou igual a                   |
#	| Qtde. de transções efetuadas                                         | 2                                  | 2                                  |
#	| Operação do valor de cada transação (R$)                             | Maior ou igual a                   | Maior ou igual a                   |
#	| Valor de cada transação (R$)                                         | R$ 500,00                          | R$ 500,00                          |
#	| Operação do período de abertura da conta                             | Maior que                          | Maior que                          |
#	| Período de abertura da conta                                         | 90                                 | 90                                 |
#	| Tempo do período de abertura da conta                                | Dias                               | Dias                               |
#	| Operação do período que o titular não transacionou para o favorecido | Maior que                          | Maior que                          |
#	| Período que o titular não transacionou para o favorecido             | 2                                  | 2                                  |
#	| Tempo do período que o titular não transacionou para o favorecido    | Dias                               | Dias                               |
#	| Transações                                                           | TED                                | TED                                |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital        |
#	| Bloqueio automático                                                  | Ativo                              | Ativo                              |
#	| Notificar titular através de push                                    | Inativo                            | Inativo                            |
#	| Notificar titular através de e-mail                                  | Inativo                            | Inativo                            |
#
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE REGRA": 
#
# 	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO        |
# 	| Produto                                                              | Aplicativo                         | Aplicativo                         |
# 	| Funcionalidade                                                       | Regras                             | Regras                             |
# 	| Transação                                                            | Alteração de regra                 | Alteração de regra                 |
# 	| Analista                                                             | BSI90896 - José Silva              | BSI90896 - José Silva              |
# 	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21             |
# 	| Regra                                                                | Transação fora do horário habitual | Transação fora do horário habitual |
# 	| Situação                                                             | Ativa                              | Ativa                              |
# 	| Gravidade                                                            | Média                              | Média                              |
# 	| Horário inicial de efetuação das transações                          | 00:00:00                           | 00:00:00                           |
# 	| Horário final de efetuação das transações                            | 04:00:00                           | 04:00:00                           |
# 	| Operação da qtde. de transações efetuadas                            | Maior ou igual a                   | Maior ou igual a                   |
# 	| Qtde. de transções efetuadas                                         | 2                                  | 2                                  |
# 	| Operação do valor de cada transação (R$)                             | Maior ou igual a                   | Maior ou igual a                   |
# 	| Valor de cada transação (R$)                                         | R$ 500,00                          | R$ 500,00                          |
# 	| Operação do período de abertura da conta                             | Maior que                          | Maior que                          |
# 	| Período de abertura da conta                                         | 90                                 | 90                                 |
# 	| Tempo do período de abertura da conta                                | Dias                               | Dias                               |
# 	| Operação do período que o titular não transacionou para o favorecido | Maior que                          | Maior que                          |
# 	| Período que o titular não transacionou para o favorecido             | 2                                  | 2                                  |
# 	| Tempo do período que o titular não transacionou para o favorecido    | Dias                               | Dias                               |
# 	| Transações                                                           | TED                                | TED, Transferência interna         |
# 	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital        |
# 	| Bloqueio automático                                                  | Ativo                              | Ativo                              |
# 	| Notificar titular através de push                                    | Inativo                            | Inativo                            |
# 	| Notificar titular através de e-mail                                  | Inativo                            | Inativo                            |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################