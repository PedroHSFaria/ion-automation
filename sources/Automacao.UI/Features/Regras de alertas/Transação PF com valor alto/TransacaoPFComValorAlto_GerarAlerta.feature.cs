﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Automacao.UI.Features.RegrasDeAlertas.TransacaoPFComValorAlto
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Transação PF com valor alto - Gerar alerta")]
    public partial class TransacaoPFComValorAlto_GerarAlertaFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "TransacaoPFComValorAlto_GerarAlerta.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("pt-br"), "Transação PF com valor alto - Gerar alerta", @"	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física efetuar transações que atendam aos parâmetros da regra “Transação PF com valor alto”, utilizando o Aplicativo BS2
	Para que possa tratar ou analisar a possível fraude", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Gerar alerta de fraude de \"Transação PF com valor alto\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        [NUnit.Framework.TestCaseAttribute("TED", null)]
        [NUnit.Framework.TestCaseAttribute("Transferência interna", null)]
        public virtual void GerarAlertaDeFraudeDeTransacaoPFComValorAlto(string transacao, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "automatizar"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Gerar alerta de fraude de \"Transação PF com valor alto\"", null, @__tags);
#line 9
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line hidden
            TechTalk.SpecFlow.Table table87 = new TechTalk.SpecFlow.Table(new string[] {
                        "CARACTERISTICA",
                        "PARAMETRO"});
            table87.AddRow(new string[] {
                        "Valor da transação (R$)",
                        "Valor parametrizado na regra"});
            table87.AddRow(new string[] {
                        "Período de abertura da conta",
                        "Período parametrizado na regra"});
            table87.AddRow(new string[] {
                        "Período do último crédito em conta",
                        "Período parametrizado na regra"});
            table87.AddRow(new string[] {
                        "Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias)",
                        "Período parametrizado na regra"});
            table87.AddRow(new string[] {
                        "Transações",
                        "Transações parametrizada na regra"});
            table87.AddRow(new string[] {
                        "Tipos de contas",
                        "Tipo parametrizado na regra"});
#line 10
 testRunner.Given(string.Format("que é efetuada uma {0} que se encaixa nas seguintes características", transacao), ((string)(null)), table87, "Dado ");
#line 18
   testRunner.When(string.Format("o sistema validar a {0}", transacao), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 19
   testRunner.Then("deve ser gerado um alerta de fraude da regra “Transação PF com valor alto” com si" +
                    "tuação igual a \"Aguardando análise”", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Ver detalhes de um alerta de fraude de \"Transação PF com valor alto\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void VerDetalhesDeUmAlertaDeFraudeDeTransacaoPFComValorAlto()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Ver detalhes de um alerta de fraude de \"Transação PF com valor alto\"", null, new string[] {
                        "automatizar"});
#line 26
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 27
 testRunner.Given("que o sistema gerou um alerta de “Transação PF com valor alto”", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 28
 testRunner.And("estou na tela \"Alertas de fraudes\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 29
 testRunner.When("acionar o comando \"Ver detalhes\" desse alerta gerado", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 30
 testRunner.Then("o sistema deve apresentar a tela \"Detalhes do alerta\" do alerta de fraude da regr" +
                    "a “Transação PF com valor alto\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Entao ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Ver transações de um alerta de fraude de \"Transação PF com valor alto\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void VerTransacoesDeUmAlertaDeFraudeDeTransacaoPFComValorAlto()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Ver transações de um alerta de fraude de \"Transação PF com valor alto\"", null, new string[] {
                        "automatizar"});
#line 33
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 34
 testRunner.Given("que estou na tela \"Detalhes do alerta\" do alerta de fraude da regra \"Transação PF" +
                    " com valor alto\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 35
 testRunner.When("acionar o comando \"Ver transações\" desse alerta de fraude", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 36
 testRunner.Then("o sistema deve apresentar a modal “Transações” com os dados das transações efetua" +
                    "das para geração do alerta", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
