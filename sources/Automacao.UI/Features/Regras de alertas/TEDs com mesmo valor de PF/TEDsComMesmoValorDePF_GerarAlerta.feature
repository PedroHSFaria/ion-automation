﻿#language: pt-br

Funcionalidade: Transação de mesmo valor de pf - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física que está cadastrado na lista de "Destinatários e remetentes supeitos" efetuar transações que atendam aos parâmetros da regra “Transação de mesmo valor de pf”, utilizando o Aplicativo do BS2
	Para que possa tratar ou analisar a possível fraude

Contexto:
	Dado que estou logado como Administrador
	E clico no menu Alertas
	E o sistema apresenta a pagina Alertas

@automatizar
Cenário: Gerar alerta de fraude de "Transação de mesmo valor de pf"
	Dado que são efetuados "recebimentos de TEDs com o mesmo valor para pf"
	Quando o sistema validar os "recebimentos de TEDs"
	Então deve ser gerado um alerta de fraude da regra “Transação ted com o mesmo valor” com situação igual a "Aguardando análise”

@automatizar
Cenário: Gerar novo alerta de fraude de "Transação de mesmo valor de pf" quando o anterior já foi analisado como "Fraude"
    Dado que já tenham sido efetuados "recebimentos de TEDs" que tenham gerado um alerta de fraude da regra "Transação de mesmo valor de pf"
    E a situação do alerta é igual a "Fraude"
    Quando novos "recebimentos de TEDs" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então deve ser gerado um alerta de fraude da regra “Transação de mesmo valor de pf” com situação igual a "Aguardado análise”    

@automatizar
Cenário: Gerar novo alerta de fraude de "Transação de mesmo valor de pf" quando o anterior já foi analisado como "Não fraude"
    Dado que já tenham sido efetuados "recebimentos de TEDs" que tenham gerado um alerta de fraude da regra "Transação de mesmo valor de pf"
    E a situação do alerta é igual a "Não fraude"
    Quando novos "recebimentos de TEDs" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então deve ser gerado um alerta de fraude da regra “Transação de mesmo valor de pf” com situação igual a "Aguardado análise”    

@automatizar
Cenario: Não gerar alerta de fraude de "Transação de mesmo valor de pf"
    Dado que já tenham sido efetuados "recebimentos de TEDs" que tenham gerado um alerta de fraude da regra "Transação de mesmo valor de pf"
	E a análise do alerta não foi realizada
	Quando novos "recebimentos de TEDs" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então não deve ser gerado alerta de fraude da regra “Transação de mesmo valor de pf”
	E as novos "recebimentos de TEDs" devem ser adicionados no alerta já existente 

@automatizadoProntoPraSubir
Cenario: Ver detalhes de um alerta de fraude de "Transação de mesmo valor de pf"
	Dado que o sistema gerou um alerta de “Transação de mesmo valor de pf”
	E estou na tela Alertas de fraudes
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Transação de mesmo valor de pf"

@automatizadoProntoPraSubir
Cenario: Ver transações de um alerta de fraude de "Transação de mesmo valor de pf"
	Dado que o sistema gerou um alerta de “Transação de mesmo valor de pf” 
	E aciono o comando ver detalhes do aelrta
	E que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Transação de mesmo valor de pf" 
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/62177782
#
############################################## OBSERVAÇÕES #####################################################	
#	
#	As transações do tipo "Recebimento de TED" devem ser recuperadas da view "ccoviwtedsrecebidas" do CCO.	
#
#	Para verificar se o remetente da transação está na lista de destinatários/remetentes suspeitos:
#
#		| CAMPO                 | ATRIBUTO NA TABELA  |
#		| CPF/CNPJ do remetente | b.cplcgccpffav      |
#		| Agência do remetente  | b.cplcodagefav      |
#		| Conta do remetente    | b.cplnumctafav      |
#
#	Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = '333182'
#
########################################## FIM DAS OBSERVAÇÕES #################################################