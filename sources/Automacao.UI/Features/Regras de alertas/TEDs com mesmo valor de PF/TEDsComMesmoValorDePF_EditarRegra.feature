﻿#language: pt-br

Funcionalidade: TEDs com mesmo valor de PF - Editar regra
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder editar os parâmetros da regra  “TEDs com mesmo valor de PF”
	Para que os alertas gerados sejam mais efetivos
	
Contexto:	
	Dado que estou logado como Administrador
	E clico no menu Configurações
	E clico no submenu Regras de alertas
	E o sistema apresenta a pagina Regras de alertas

@automatizado
Cenario: Apresentar os parâmetros da regra TEDs com mesmo valor de PF
	Quando clico em Editar na regra TEDs com mesmo valor de PF
	Então o sistema apresenta a tela de edição da regra TEDs com mesmo valor de PF.
	E deve apresentar os seguintes parâmetros.
	| NOME DO PARÂMETRO                                                              |
	| Gravidade                                                                      |
	| Período de efetuação das TEDs                                                  |
	| Qtde. de TEDs efetuadas                                                        |
	| Valor da TED (R$)                                                              |
	| Período de abertura da conta                                                   |
	| Período do último crédito em conta                                             |
	| Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias) |
	| Tipos de contas                                                                |  

@automatizadoProntoPraSubir
Cenario: Editar parâmetros da regra TEDs com mesmo valor de PF 
	Quando clico em Editar na regra TEDs com mesmo valor de PF
	E o sistema apresenta a tela de edição da regra TEDs com mesmo valor de PF.
	E altero um ou mais parâmetros da regra TEDs com mesmo valor de PF
	E aciono o comando Salvar
	Entao o sistema deve apresentar a mensagem Os parâmetros da regra foram editados com sucesso
	E deve aplicar a data/hora de corte na regra
	E o sistema deve redirecionar para a tela Regras de alertas
	E deve gravar o log de Alteração da regra TEDs com mesmo valor de PF

@automatizado
Cenario: Não editar parâmetros da regra TEDs com mesmo valor de PF
	Quando clico em Editar na regra TEDs com mesmo valor de PF
	E o sistema apresenta a tela de edição da regra TEDs com mesmo valor de PF.
	E aciono o comando Salvar
	Então o sistema deve apresentar a mensagem Como não houve alterações nos parâmetros da regra
	E o sistema apresenta a tela Regras de alertas
	E não devem ter sido realiazadas alterações na regra TEDs com mesmo valor de PF

@automatizar
Cenario: Parâmetros obrigatórios não preenchidos da regra TEDs com mesmo valor de PF
	Quando clico em Editar na regra TEDs com mesmo valor de PF
	E o sistema apresenta a tela de edição da regra TEDs com mesmo valor de PF.
	E não preencher todos os parâmetros obrigatórios.
	E aciono o comando Salvar
	Então o sistema deve apresentar a mensagem É obrigatório selecionar pelo menos um tipo de conta. no campo <Tipos de contas> 

@automatizado
Cenario: Cancelar edição dos parâmetros da regra TEDs com mesmo valor de PF
	Quando clico em Editar na regra TEDs com mesmo valor de PF
	E o sistema apresenta a tela de edição da regra TEDs com mesmo valor de PF.
	E aciono o comando Cancelar
	Então o sistema apresenta a tela Regras de alertas
	E não devem ter sido realiazadas alterações na regra TEDs com mesmo valor de PF
 
 @automatizadoProntoPraSubir
 Cenario: Verificar se foi gerado um log de ativacao da regra
	Quando Ativo a regra Ted com mesmo valor para pf
	E Exibe a mensagem exibindo que a regra foi ativada
	Então seleciono o menu logs
	E Verifico se gerou um log de ativacao da regra
 
 @automatizadoProntoPraSubir
 Cenario: Verificar se foi gerado um log de alteracao da regra
	Dado clico em Editar na regra TEDs com mesmo valor de PF
	E o sistema apresenta a tela de edição da regra TEDs com mesmo valor de PF.
	E altero um ou mais parâmetros da regra TEDs com mesmo valor de PF
	Quando aciono o comando Salvar
	Então o sistema deve apresentar a mensagem Os parâmetros da regra foram editados com sucesso
	E seleciono o menu logs
	E Verifico se gerou um log de alteracao da regra 

#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/61965228
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# CAMPOS DA TELA EDITAR PARÂMETROS:
#
#	Gravidade
#		| PARAMETRO | TIPO     | VALORES                           | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL                 |
#		| Gravidade | Combobox | Baixa, Média, Alta e Info | Sim         | Não          | N/A     | N/A         | Valor que já está em produção | 
#
#	Período de efetuação das TEDs
#		| PARAMETRO          | TIPO     | VALORES                          | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER        | VALOR_INICIAL                 |
#		| Sinal              | String   | Menor ou igual a               | Sim         | Sim          | N/A     | N/A                | Valor que já está em produção |
#		| Quantidade (tempo) | Integer  | Qualquer valor inteiro           | Sim         | Não          | N/A     | Quantidade (tempo) | Valor que já está em produção |
#		| Tempo              | Combobox | Segundos, Minutos ou Horas | Sim         | Não          | N/A     | N/A                | Valor que já está em produção |
#
#	Qtde. de TEDs efetuadas  
#		| PARAMETRO               | TIPO     | VALORES                         | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER             | VALOR_INICIAL                 |
#		| Sinal                   | Combobox | Maior que e Maior ou igual a | Sim         | Não          | N/A     | N/A                     | Valor que já está em produção |
#		| Quantidade (TEDs)       | Integer  | Qualquer valor inteiro          | Sim         | Não          | N/A     | Quantidade (TEDs)	    | Valor que já está em produção |
#		
#	Valor da TED (R$)    
#		| PARAMETRO  | TIPO     | VALORES                          | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL                 |
#		| Sinal      | Combobox | Maior que e Maior ou igual a | Sim         | Não          | N/A     | N/A         | Valor que já está em produção |
#		| Valor (R$) | String   | Qualquer valor real              | Sim         | Não          | R$ 0,00 | Valor (R$)  | Valor que já está em produção |
#	
#	Período de abertura da conta 
#		| PARAMETRO          | TIPO    | VALORES                | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER        | VALOR_INICIAL |
#		| Sinal              | String  | Menor que            | Sim         | Sim          | N/A     | N/A                | Menor que     |
#		| Quantidade (tempo) | Integer | Qualquer valor inteiro | Sim         | Não          | N/A     | Quantidade (tempo) | 90            |
#		| Tempo              | String  | Dias                 | Sim         | Sim          | N/A     | N/A                | Dias          |
#		
#	Período do último crédito em conta
#		| PARAMETRO          | TIPO     | VALORES                                 | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER        | VALOR_INICIAL |
#		| Sinal              | String   | Menor que                             | Sim         | Sim          | N/A     | N/A                | Menor que     |
#		| Quantidade (tempo) | Integer  | Qualquer valor interiro                 | Sim         | Não          | N/A     | Quantidade (tempo) | 24            |
#		| Tempo              | Combobox | Segundos, Minutos, Horas e Dias | Sim         | Não          | N/A     | N/A                | Horas         |
#		
#	Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias) 
#		| PARAMETRO          | TIPO    | VALORES                          | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER        | VALOR_INICIAL |
#		| Sinal              | String  | Maior que e Maior ou igual a | Sim         | Não          | N/A     | N/A                | Maior que     |
#		| Quantidade (tempo) | Integer | Qualquer valor inteiro           | Sim         | Não          | N/A     | Quantidade (tempo) | 2             |
#		| Tempo              | String  | Dias                           | Sim         | Sim          | N/A     | N/A                | Dias          |
#
#	Tipos de contas 
#		| PARAMETRO    | TIPO    | VALORES                                                                | OBRIGATORIO | DESABILITADO | MASCARA                                                                    | PLACEHOLDER | VALOR_INICIAL                 |
#		| Disponíveis  | LisBox  | Tipos de contas que não foram transferidos para o campo <Selecionados> | Não         | Não          | Código do tipo da conta (com 3 dígitos 000) – Descrição do tipo da conta | N/A         | Valor que já está em produção |
#		| Selecionados | ListBox | Tipos de contas que foram transferidos do campo <Disponíveis>          | Sim         | Não          | Código do tipo da conta (com 3 dígitos 000) – Descrição do tipo da conta | N/A         | Valor que já está em produção |
#
#	bloquear contas automaticamente
#		| TIPO     | VALOR_INICIAL                 |
#		| Checkbox | Valor que já está em produção |
#
#	Informe se deseja ou não notificar o titular da conta
#		| PARAMETRO                             | TIPO     | VALOR_INICIAL                 |
#		| Notificar o titular através de push   | Checkbox | Valor que já está em produção |
#		| Notificar o titular através de e-mail | Checkbox | Valor que já está em produção |
#
#
# EXEMPLO DO LOG DE ATIVAÇÃO DE REGRA: 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                                              | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                                       | Regras                      | Regras                      |
#	| Transação                                                            | Ativação de regra           | Ativação de regra           |
#	| Analista                                                             | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                                                | TEDs com mesmo valor de PF  | TEDs com mesmo valor de PF  |
#	| Situação                                                             | Inativa                     | Ativa                       |
#	| Gravidade                                                            | Média                       | Média                       |
#	| Operação do período de efetuação das TEDs                            | Menor ou igual a            | Menor ou igual a            |
#	| Período de efetuação das TEDs                                        | 12                          | 12                          |
#	| Tempo do período de efetuação das TEDs                               | Horas                       | Horas                       |
#	| Operação da qtde. de TEDs efetuadas                                  | Maior ou igual a            | Maior ou igual a            |
#	| Qtde. de TEDs efetuadas                                              | 2                           | 2                           |
#	| Operação do valor da TED (R$)                                        | Maior ou igual a            | Maior ou igual a            |
#	| Valor da TED (R$)                                                    | R$ 500,00                   | R$ 500,00                   |
#	| Operação do período de abertura da conta                             | Maior que                   | Maior que                   |
#	| Período de abertura da conta                                         | 90                          | 90                          |
#	| Tempo do período de abertura da conta                                | Dias                        | Dias                        |
#	| Operação do período do último crédito em conta                       | Maior que                   | Maior que                   |
#	| Período do último crédito em conta                                   | 24                          | 24                          |
#	| Tempo do período do último crédito em conta                          | Horas                       | Horas                       |
#	| Operação do período que o titular não transacionou para o favorecido | Maior que                   | Maior que                   |
#	| Período que o titular não transacionou para o favorecido             | 2                           | 2                           |
#	| Tempo do período que o titular não transacionou para o favorecido    | Dias                        | Dias                        |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                                                  | Ativo                       | Ativo                       |
#	| Notificar titular através de push                                    | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail                                  | Inativo                     | Inativo                     |
#
#
# EXEMPLO DO LOG DE INATIVAÇÃO DE REGRA: 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                                              | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                                       | Regras                      | Regras                      |
#	| Transação                                                            | Inativação de regra         | Inativação de regra         |
#	| Analista                                                             | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                                                | TEDs com mesmo valor de PF  | TEDs com mesmo valor de PF  |
#	| Situação                                                             | Ativa                       | Inativa                     |
#	| Gravidade                                                            | Média                       | Média                       |
#	| Operação do período de efetuação das TEDs                            | Menor ou igual a            | Menor ou igual a            |
#	| Período de efetuação das TEDs                                        | 12                          | 12                          |
#	| Tempo do período de efetuação das TEDs                               | Horas                       | Horas                       |
#	| Operação da qtde. de TEDs efetuadas                                  | Maior ou igual a            | Maior ou igual a            |
#	| Qtde. de TEDs efetuadas                                              | 2                           | 2                           |
#	| Operação do valor da TED (R$)                                        | Maior ou igual a            | Maior ou igual a            |
#	| Valor da TED (R$)                                                    | R$ 500,00                   | R$ 500,00                   |
#	| Operação do período de abertura da conta                             | Maior que                   | Maior que                   |
#	| Período de abertura da conta                                         | 90                          | 90                          |
#	| Tempo do período de abertura da conta                                | Dias                        | Dias                        |
#	| Operação do período do último crédito em conta                       | Maior que                   | Maior que                   |
#	| Período do último crédito em conta                                   | 24                          | 24                          |
#	| Tempo do período do último crédito em conta                          | Horas                       | Horas                       |
#	| Operação do período que o titular não transacionou para o favorecido | Maior que                   | Maior que                   |
#	| Período que o titular não transacionou para o favorecido             | 2                           | 2                           |
#	| Tempo do período que o titular não transacionou para o favorecido    | Dias                        | Dias                        |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                                                  | Ativo                       | Ativo                       |
#	| Notificar titular através de push                                    | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail                                  | Inativo                     | Inativo                     |
#
#
# EXEMPLO DO LOG DE ALTERAÇÃO DE REGRA: 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                                              | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                                       | Regras                      | Regras                      |
#	| Transação                                                            | Alteração de regra          | Alteração de regra          |
#	| Analista                                                             | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                                                | TEDs com mesmo valor de PF  | TEDs com mesmo valor de PF  |
#	| Situação                                                             | Ativa                       | Ativa                       |
#	| Gravidade                                                            | Média                       | Média                       |
#	| Operação do período de efetuação das TEDs                            | Menor ou igual a            | Menor ou igual a            |
#	| Período de efetuação das TEDs                                        | 12                          | 12                          |
#	| Tempo do período de efetuação das TEDs                               | Horas                       | Horas                       |
#	| Operação da qtde. de TEDs efetuadas                                  | Maior ou igual a            | Maior ou igual a            |
#	| Qtde. de TEDs efetuadas                                              | 2                           | 2                           |
#	| Operação do valor da TED (R$)                                        | Maior ou igual a            | Maior ou igual a            |
#	| Valor da TED (R$)                                                    | R$ 500,00                   | R$ 1.000,00                 |
#	| Operação do período de abertura da conta                             | Maior que                   | Maior que                   |
#	| Período de abertura da conta                                         | 90                          | 90                          |
#	| Tempo do período de abertura da conta                                | Dias                        | Dias                        |
#	| Operação do período do último crédito em conta                       | Maior que                   | Maior que                   |
#	| Período do último crédito em conta                                   | 24                          | 24                          |
#	| Tempo do período do último crédito em conta                          | Horas                       | Horas                       |
#	| Operação do período que o titular não transacionou para o favorecido | Maior que                   | Maior que                   |
#	| Período que o titular não transacionou para o favorecido             | 2                           | 2                           |
#	| Tempo do período que o titular não transacionou para o favorecido    | Dias                        | Dias                        |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                                                  | Ativo                       | Ativo                       |
#	| Notificar titular através de push                                    | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail                                  | Inativo                     | Inativo                     |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################