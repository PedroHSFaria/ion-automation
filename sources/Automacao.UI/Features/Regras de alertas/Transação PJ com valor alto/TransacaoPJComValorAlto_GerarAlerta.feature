﻿#language: pt-br

Funcionalidade: Transação PJ com valor alto - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa jurídica efetuar transações que atendam aos parâmetros da regra “Transação PJ com valor alto”
	Para que possa tratar ou analisar a possível fraude

@automatizar
Esquema do Cenário: Gerar alerta de fraude de "Transação PJ com valor alto"
	Dado que é efetuada uma <transacao> que se encaixa nas seguintes características
      | CARACTERISTICA                                                                 | PARAMETRO                         |
      | Valor da transação (R$)                                                        | Valor parametrizado na regra      |
      | Período de abertura da conta                                                   | Período parametrizado na regra    |
      | Período do último crédito em conta                                             | Período parametrizado na regra    |
      | Transações                                                                     | Transações parametrizada na regra |
	  | Tipos de contas                                                                | Tipo parametrizado na regra       |
   Quando o sistema validar a <transacao>
   Então deve ser gerado um alerta de fraude da regra “Transação PJ com valor alto” com situação igual a "Aguardando análise”
   Exemplos: 
	| transacao                            |
	| 780 TRANSF.ENT.CTAS INTERN. BANKING  |
	| 781 TRANSF.ENT.CTAS DIF. INTER.BANK. |
	| 778 TED C REMETIDA INTERNET BANKING  |  
	| 779 TED D REMETIDA INTERNET BANKING  |  

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "Transação PJ com valor alto"
	Dado que o sistema gerou um alerta de “Transação PJ com valor alto”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Transação PJ com valor alto"

@automatizar
Cenario: Ver transações de um alerta de fraude de "Transação PJ com valor alto"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Transação PJ com valor alto"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/61967354
#
############################################ OBSERVAÇÕES ########################################################	
#
#		As transações do CCO devem ser recuperadas da tabela "tb_bs2_cc": 
#
#			| hstcod | hstdes 								| 
#			| 780 	 | TRANSF.ENT.CTAS INTERN. BANKING		|
#			| 781 	 | TRANSF.ENT.CTAS DIF. INTER.BANK.		|
#			| 778 	 | TED C REMETIDA INTERNET BANKING		|
#			| 779 	 | TED D REMETIDA INTERNET BANKING		|
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = '333182'
#
#		Para verificar o útimo crédito em conta, pode-se efetuar a consulta abaixo na tabela "tb_bs2_cc" do CCO:
#
#			use FINFINDBS_PRD
#			select * from tb_bs2_cc
#			where ctanum = 'Número da conta do titular'
#			and hstnat = 'C'
#			ordre by hdrdata desc
#
########################################## FIM DAS OBSERVAÇÕES #################################################