﻿#language: pt-br

Funcionalidade: Transação PJ com valor alto - Relatório de log
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder extrair o relatório de log
	Para saber quando a regra "Transação PJ com valor alto" foi ativada, inativa e/ou alterada
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"

@automatizar
Cenario: Relatório de log de ativação a regra "Transação PJ com valor alto"
	Quando a regra "Transação PJ com valor alto" está inativa
	E aciono o comando "Ativar" da regra "Transação PJ com valor alto"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de inativação a regra "Transação PJ com valor alto"
	Quando a regra "Transação PJ com valor alto" está ativa
	E aciono o comando "Inativar" da regra "Transação PJ com valor alto"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra 
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de alteração d regra "Transação PJ com valor alto"
	Quando clico em "Editar" na regra "Transação PJ com valor alto"
	E altero um ou mais parâmetros da regra "Transação PJ com valor alto"
	E clico em "Salvar"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"
	
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# EXEMPLO DO LOG DE "ATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                               | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                        | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                 | Regras                      | Regras                      |
#	| Transação                                      | Ativação de regra           | Ativação de regra           |
#	| Analista                                       | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                         | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                          | Transação PJ com valor alto | Transação PJ com valor alto |
#	| Situação                                       | Inativa                     | Ativa                       |
#	| Gravidade                                      | Média                       | Média                       |
#	| Operação do valor da transação (R$)            | Maior ou igual a            | Maior ou igual a            |
#	| Valor da transação (R$)                        | R$ 500,00                   | R$ 500,00                   |
#	| Operação do período de abertura da conta       | Maior que                   | Maior que                   |
#	| Período de abertura da conta                   | 90                          | 90                          |
#	| Tempo do período de abertura da conta          | Dias                        | Dias                        |
#	| Operação do período do último crédito em conta | Maior que                   | Maior que                   |
#	| Período do último crédito em conta             | 24                          | 24                          |
#	| Tempo do período do último crédito em conta    | Horas                       | Horas                       |
#	| Transações                                     | Ted                         | Ted                         |
#	| Tipos de contas selecionados                   | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                            | Ativo                       | Ativo                       |
#	| Notificar titular através de push              | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail            | Inativo                     | Inativo                     |
#
#
# EXEMPLO DO LOG DE "INATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                               | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                        | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                 | Regras                      | Regras                      |
#	| Transação                                      | Inativação de regra         | Inativação de regra         |
#	| Analista                                       | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                         | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                          | Transação PJ com valor alto | Transação PJ com valor alto |
#	| Situação                                       | Ativa                       | Inativa                     |
#	| Gravidade                                      | Média                       | Média                       |
#	| Operação do valor da transação (R$)            | Maior ou igual a            | Maior ou igual a            |
#	| Valor da transação (R$)                        | R$ 500,00                   | R$ 500,00                   |
#	| Operação do período de abertura da conta       | Maior que                   | Maior que                   |
#	| Período de abertura da conta                   | 90                          | 90                          |
#	| Tempo do período de abertura da conta          | Dias                        | Dias                        |
#	| Operação do período do último crédito em conta | Maior que                   | Maior que                   |
#	| Período do último crédito em conta             | 24                          | 24                          |
#	| Tempo do período do último crédito em conta    | Horas                       | Horas                       |
#	| Transações                                     | Ted                         | Ted                         |
#	| Tipos de contas selecionados                   | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                            | Ativo                       | Ativo                       |
#	| Notificar titular através de push              | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail            | Inativo                     | Inativo                     |
#
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                               | VALOR_ANTERIOR_A_TRANSACAO  | VALOR_POSTERIOR_A_TRANSACAO |
#	| Produto                                        | Aplicativo                  | Aplicativo                  |
#	| Funcionalidade                                 | Regras                      | Regras                      |
#	| Transação                                      | Inativação de regra         | Inativação de regra         |
#	| Analista                                       | BSI90896 - José Silva       | BSI90896 - José Silva       |
#	| Data/Hora da transação                         | 30/09/2019 às 15:30:21      | 30/09/2019 às 15:30:21      |
#	| Regra                                          | Transação PJ com valor alto | Transação PJ com valor alto |
#	| Situação                                       | Ativa                       | Ativa                       |
#	| Gravidade                                      | Média                       | Média                       |
#	| Operação do valor da transação (R$)            | Maior ou igual a            | Maior ou igual a            |
#	| Valor da transação (R$)                        | R$ 500,00                   | R$ 1.000,00                 |
#	| Operação do período de abertura da conta       | Maior que                   | Maior que                   |
#	| Período de abertura da conta                   | 90                          | 90                          |
#	| Tempo do período de abertura da conta          | Dias                        | Dias                        |
#	| Operação do período do último crédito em conta | Maior que                   | Maior que                   |
#	| Período do último crédito em conta             | 24                          | 24                          |
#	| Tempo do período do último crédito em conta    | Horas                       | Horas                       |
#	| Transações                                     | Ted                         | Ted                         |
#	| Tipos de contas selecionados                   | 240 – Pessoa Física Digital | 240 – Pessoa Física Digital |
#	| Bloqueio automático                            | Ativo                       | Ativo                       |
#	| Notificar titular através de push              | Inativo                     | Inativo                     |
#	| Notificar titular através de e-mail            | Inativo                     | Inativo                     |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################