﻿#language: pt-br

Funcionalidade: TEDs de outra titularidade de PF - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física receber transações que atendam aos parâmetros da regra “TEDs de outra titularidade de PF”
	Para que possa tratar ou analisar a possível fraude

@automatizar
Cenário: Gerar alerta de fraude de "TEDs de outra titularidade de PF"
  Dado que são efetuados "recebimentos de TEDs" de mesma titularidade que se encaixam nas seguintes características
      | CARACTERISTICA                                                                 | PARAMETRO                         |
      | Período de efetuação das TEDs                                                  | Período parametrizado na regra    |
      | Qtde. de TEDs efetuadas                                                        | Quantidade parametrizada na regra |
      | Valor de cada TED (R$)                                                         | Valor parametrizado na regra      |
      | Período de abertura da conta                                                   | Período parametrizado na regra    |
      | Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias) | Período parametrizado na regra    |
      | Tipos de contas                                                                | Tipo parametrizado na regra       |
   Quando o sistema validar os "recebimentos de TEDs"
   Então deve ser gerado um alerta de fraude da regra “TEDs de outra titularidade de PF” com situação igual a "Aguardando análise”

@automatizar
Cenário: Gerar novo alerta de fraude de "TEDs de outra titularidade de PF" quando o anterior já foi analisado como "Fraude"
    Dado que já tenham sido efetuados "recebimentos de TEDs" de mesma titularidade que tenham gerado um alerta de fraude da regra  "TEDs de outra titularidade de PF"
    E a situação do alerta é igual a "Fraude"
    Quando novos "recebimentos de TEDs" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então deve ser gerado um alerta de fraude da regra “TEDs de outra titularidade de PF” com situação igual a "Aguardado análise”    

@automatizar
Cenário: Gerar novo alerta de fraude de "TEDs de outra titularidade de PF" quando o anterior já foi analisado como "Não fraude"
    Dado que já tenham sido efetuados "recebimentos de TEDs" de mesma titularidade que tenham gerado um alerta de fraude da regra "TEDs de outra titularidade de PF"
    E a situação do alerta é igual a "Não fraude"
    Quando novos "recebimentos de TEDs" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então deve ser gerado um alerta de fraude da regra “TEDs de outra titularidade de PF” com situação igual a "Aguardado análise”    

@automatizar
Cenario: Não gerar alerta de fraude de "TEDs de outra titularidade de PF"
    Dado que já tenham sido efetuados "recebimentos de TEDs" que tenham gerado um alerta de fraude da regra "TEDs de outra titularidade de PF"
	E a análise do alerta não foi realizada
	Quando novos "recebimentos de TEDs" com as mesmas características das que geraram o alerta de fraude forem efetuados
	Então não deve ser gerado alerta de fraude da regra “TEDs de outra titularidade de PF”
	E os novos "recebimentos de TEDs" devem ser adicionados no alerta já existente 

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "TEDs de outra titularidade de PF"
	Dado que o sistema gerou um alerta de “TEDs de outra titularidade de PF”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “TEDs de outra titularidade de PF"

@automatizar
Cenario: Ver transações de um alerta de fraude de "TEDs de outra titularidade de PF"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "TEDs de outra titularidade de PF"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/61965220
#
############################################## OBSERVAÇÕES #####################################################	
#	
#		As transações do tipo "Recebimento de TED" devem ser recuperadas da view "ccoviwtedsrecebidas" do CCO.	
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = '333182'
#
########################################## FIM DAS OBSERVAÇÕES #################################################