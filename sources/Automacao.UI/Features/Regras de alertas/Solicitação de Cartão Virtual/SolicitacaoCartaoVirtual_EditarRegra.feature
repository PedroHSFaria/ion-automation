﻿#language: pt-br

Funcionalidade: SolicitacaoCartaoVirtual_EditarRegra
	Como Responsável pelas parametrizações dos alertas de fraude do BS2
	Quero Realizar a edção da regra "Solicitação Cartão Virtual"
	Para que os alertas gerados sejam mais efetivos

Contexto: 
	Dado que estou logado como Administrador
	E clico no menu Configurações
	E clico no submenu Regras de alertas
	E o sistema apresenta a pagina Regras de alertas

@automatizado
Cenário: Editar regra Solicitacao Cartao Virtual
	Dado que clico em editar a regra Solicitacao Cartao Virtual
	E estou na pagina da regra Solicitacao Cartao Virtual
	Quando Edito todos os parametros da regra
	Então Clico em Salvar
	E gera a mensagem de confirmacao de edicao de regra

@automatizar1704
Cenário: Nao preenchimento de campos obrigatórios regra Solicitacao Cartao Virtual
	Dado que clico em editar a regra Solicitacao Cartao Virtual
	E estou na pagina da regra Solicitacao Cartao Virtual
	E Nao preencho algum campo obrigatorio
	Quando clico em salvar
	Entao exibe a mensagem "O Preenchimento do campo é obrigatório"

@automatizar1704
Cenário: Cancelar Edicao de regra Solicitacao Cartao Virtual
	Dado que clico em editar a regra Solicitacao Cartao Virtual
	E estou na pagina da regra Solicitacao Cartao Virtual
	E preencho os campos
	Quando clico em cancelar
	Entao sou redirecionado para a tela de regras de alertas		


