﻿#language: pt-br

Funcionalidade: Transação para destinatário suspeito - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física que está cadastrado na lista de "Destinatários e remetentes supeitos" efetuar transações que atendam aos parâmetros da regra “Transação para destinatário suspeito”, utilizando o Aplicativo do BS2
	Para que possa tratar ou analisar a possível fraude

@automatizar
Esquema do Cenário: Gerar alerta de fraude de "Transação para destinatário suspeito"
	Dado que são efetuadas <transacoes> para destinatários cadastrados na lista de "Destinatários e remetentes suspeitos" que se encaixam nas seguintes características
      | CARACTERISTICA                | PARAMETRO                         |
      | Qtde. de transações efetuadas | Quantidade parametrizada na regra |
      | Valor de cada transação (R$)  | Valor parametrizado na regra      |
      | Período de abertura da conta  | Período parametrizado na regra    |
      | Transações                    | Transações parametizadas na regra |
      | Tipos de contas               | Tipo parametrizado na regra       |
   Quando o sistema validar as <transacoes>
   Então deve ser gerado um alerta de fraude da regra “Transação para destinatário suspeito” com situação igual a "Aguardando análise”
   Exemplos: 
	| transacoes            |
	| TED                   |
	| Transferência interna |

@automatizar
Esquema do Cenário: Gerar novo alerta de fraude de "Transação para destinatário suspeito" quando o anterior já foi analisado como "Fraude"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Transação para destinatário suspeito"
    E a situação do alerta é igual a "Fraude"
    Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então deve ser gerado um alerta de fraude da regra “Transação para destinatário suspeito” com situação igual a "Aguardado análise”    
   Exemplos: 
	| transacoes            |
	| TED                   |
	| Transferência interna |

@automatizar
Esquema do Cenário: Gerar novo alerta de fraude de "Transação para destinatário suspeito" quando o anterior já foi analisado como "Não fraude"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Transação para destinatário suspeito"
    E a situação do alerta é igual a "Não fraude"
    Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então deve ser gerado um alerta de fraude da regra “Transação para destinatário suspeito” com situação igual a "Aguardado análise”    
   Exemplos: 
	| transacoes            |
	| TED                   |
	| Transferência interna |

@automatizar
Esquema do Cenario: Não gerar alerta de fraude de "Transação para destinatário suspeito"
    Dado que já tenham sido efetuadas <transacoes> que tenham gerado um alerta de fraude da regra "Transação para destinatário suspeito"
	E a análise do alerta não foi realizada
	Quando novas <transacoes> com as mesmas características das que geraram o alerta de fraude forem efetuadas
	Então não deve ser gerado alerta de fraude da regra “Transação para destinatário suspeito”
	E as novas <transacoes> devem ser adicionadas no alerta já existente 
   Exemplos: 
	| transacoes            |
	| TED                   |
	| Transferência interna |

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "Transação para destinatário suspeito"
	Dado que o sistema gerou um alerta de “Transação para destinatário suspeito”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Transação para destinatário suspeito"

@automatizar
Cenario: Ver transações de um alerta de fraude de "Transação para destinatário suspeito"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Transação para destinatário suspeito"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/62177026
#
############################################ OBSERVAÇÕES ########################################################	
#	
#	As transações do Digital devem ser recuperadas das seguintes tabelas:
#
#			| Transação           			| Tabela 							|  
#			| TED 							| tra.TRANS_APP_TED 				|
#			| Transferência interna 		| tra.TRANS_APP_TRANSFER			|
#
#	Para verificar se o favorecido da transação está na lista de destinatários/remetentes suspeitos:
#
#	TED:
#
#		| CAMPO                  | ATRIBUTO NA TABELA                              |
#		| CPF/CNPJ do favorecido | "REQUEST_FAV_CPFCNPJ" ou "RESPONSE_FAV_CPFCNPJ" |
#		| Agência do favorecido  | "REQUEST_FAV_BRANCH" ou "RESPONSE_FAV_BRANCH"   |
#		| Conta do favorecido    | "REQUEST_FAV_ACCOUNT" ou "RESPONSE_FAV_ACCOUNT" |
#
#	TRANSFERÊNCIA INTERNA
#		
#		| CAMPO                  | ATRIBUTO NA TABELA                              |
#		| CPF/CNPJ do favorecido | "RESPONSE_FAV_CPFCNPJ"                          |
#		| Agência do favorecido  | "REQUEST_FAV_BRANCH" ou "RESPONSE_FAV_BRANCH"   |
#		| Conta do favorecido    | "REQUEST_FAV_ACCOUNT" ou "RESPONSE_FAV_ACCOUNT" |
#
#	Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = '333182'
#
########################################## FIM DAS OBSERVAÇÕES #################################################