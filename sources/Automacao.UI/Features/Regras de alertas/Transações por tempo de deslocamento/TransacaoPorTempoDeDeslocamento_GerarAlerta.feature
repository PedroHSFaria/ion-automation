﻿#language: pt-br

Funcionalidade: Transações por tempo de deslocamento - Gerar alerta
	Como responsável pelo monitoramento de fraude do BS2
	Quero ser alertado quando um cliente do tipo pessoa física efetuar transações que atendam aos parâmetros da regra “Transações por tempo de deslocamento”, utilizando o Aplicativo do BS2
	Para que possa tratar ou analisar a possível fraude

@automatizar
Esquema do Cenário: Gerar alerta de fraude de "Transações por tempo de deslocamento"
	Dado que é efetuada uma <transacao> que se encaixa nas seguintes características
      | CARACTERISTICA                                                                 | PARAMETRO                          |
      | Período de efetuação das transações                                            | Período parametrizado na regra     |
      | Distância                                                                      | Distância parametrizada na regra   |
      | Valor da transação (R$)                                                        | Valor parametrizado na regra       |
      | Período de abertura da conta                                                   | Período parametrizado na regra     |
      | Titular não transacionou para o favorecido no período (Ex.: Há mais de 2 dias) | Período parametrizado na regra     |
      | Transações                                                                     | Transações parametrizadas na regra |
      | Tipos de contas                                                                | Tipos parametrizados na regra      |
   Quando o sistema validar a <transacao>
   Então deve ser gerado um alerta de fraude da regra “Transações por tempo de deslocamento” com situação igual a "Aguardando análise”
   Exemplos: 
	| transacao                   |
	| TED                         |
	| Transferência interna       |
	| Pagamento de boleto         |
	| Pagamento de concessionária |
	| Aplicação                   |
	| Resgate de investimento     |

@automatizar
Cenario: Ver detalhes de um alerta de fraude de "Transações por tempo de deslocamento"
	Dado que o sistema gerou um alerta de “Transações por tempo de deslocamento”
	E estou na tela "Alertas de fraudes"
	Quando acionar o comando "Ver detalhes" desse alerta gerado
	Entao o sistema deve apresentar a tela "Detalhes do alerta" do alerta de fraude da regra “Transações por tempo de deslocamento"

@automatizar
Cenario: Ver transações de um alerta de fraude de "Transações por tempo de deslocamento"
	Dado que estou na tela "Detalhes do alerta" do alerta de fraude da regra "Transações por tempo de deslocamento"
	Quando acionar o comando "Ver transações" desse alerta de fraude
	Então o sistema deve apresentar a modal “Transações” com os dados das transações efetuadas para geração do alerta
 
#
################################ PROTÓTIPO ######################################
#
#	Tela parâmetros da regra: https://marvelapp.com/7e20jdb/screen/62051629
#
############################################ OBSERVAÇÕES ########################################################	
#	
#		As transações do Digital devem ser recuperadas das seguintes tabelas:
#
#			| Transação           			| Tabela 							|  
#			| TED 							| tra.TRANS_APP_TED 				|
#			| Transferência interna 		| tra.TRANS_APP_TRANSFER			|
#			| Pagamento de boleto 			| tra.TRANS_APP_BILL				| 
#			| Pagamento de concessionária	| tra.TRANS_APP_CONCESSIONAIRES 	|
#			| Aplicacação 					| tra.TRANS_APP_APPLICATION			|
#			| Resgate de investimento 		| tra.TRANS_APP_RESCUE				|
#
#		Para verificar a data de abertura da conta, pode efetuar a consulta abaixo na tabela "tb_ctacor" do CCO:
#
#			use FINFINDBS_PRD
#			select ctadat  DATAABERTURA
#			from finfindbs_prd..tb_ctacor ct
#			where ctanum = '333182'
#
########################################## FIM DAS OBSERVAÇÕES #################################################