﻿#language: pt-br

Funcionalidade: Transações por tempo de deslocamento - Relatório de regra
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder extrair o relatório de log
	Para saber quando a regra "Transações por tempo de deslocamento" foi ativada, inativa e/ou alterada
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu "Regras de alertas"

@automatizar
Cenario: Relatório de log de ativação a regra "Transações por tempo de deslocamento"
	Quando a regra "Transações por tempo de deslocamento" está inativa
	E aciono o comando "Ativar" da regra "Transações por tempo de deslocamento"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de inativação a regra "Transações por tempo de deslocamento"
	Quando a regra "Transações por tempo de deslocamento" está ativa
	E aciono o comando "Inativar" da regra "Transações por tempo de deslocamento"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra 
	E me mantêm na tela "Logs"

@automatizar
Cenario: Relatório de log de alteração d regra "Transações por tempo de deslocamento"
	Quando clico em "Editar" na regra "Transações por tempo de deslocamento"
	E altero um ou mais parâmetros da regra "Transações por tempo de deslocamento"
	E clico em "Salvar"
	E clico no menu "Logs"
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em "Filtrar"
	E aciono o comando "exportar para Excel"
	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs da regra
	E me mantêm na tela "Logs"
	
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# EXEMPLO DO LOG DE "ATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO        |
#	| Produto                                                              | Aplicativo                         | Aplicativo                         |
#	| Funcionalidade                                                       | Regras                             | Regras                             |
#	| Transação                                                            | Ativação de regra                  | Ativação de regra                  |
#	| Analista                                                             | BSI90896 - José Silva              | BSI90896 - José Silva              |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21             |
#	| Regra                                                                | Transações por tempo de deslocamento | Transações por tempo de deslocamento |
#	| Situação                                                             | Inativa                            | Ativa                              |
#	| Gravidade                                                            | Média                              | Média                              |
#	| Operação do período de efetuação das transações                      | Menor ou igual a                   | Menor ou igual a                   |
#	| Período de efetuação das transações                                  | 12                                 | 12                                 |
#	| Tempo do período de efetuação das transações                         | Horas                              | Horas                              |
#	| Operação da qtde. de transações efetuadas                            | Maior ou igual a                   | Maior ou igual a                   |
#	| Qtde. de transações efetuadas                                        | 2                                  | 2                                  |
#	| Operação do valor de cada transação (R$)                             | Maior ou igual a                   | Maior ou igual a                   |
#	| Valor de cada transação (R$)                                         | R$ 500,00                          | R$ 500,00                          |
#	| Operação do período de abertura da conta                             | Maior que                          | Maior que                          |
#	| Período de abertura da conta                                         | 90                                 | 90                                 |
#	| Tempo do período de abertura da conta                                | Dias                               | Dias                               |
#	| Operação do período que o titular não transacionou para o favorecido | Maior que                          | Maior que                          |
#	| Período que o titular não transacionou para o favorecido             | 2                                  | 2                                  |
#	| Tempo do período que o titular não transacionou para o favorecido    | Dias                               | Dias                               |
#	| Transações                                                           | TED                                | TED                                |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital        |
#	| Bloqueio automático                                                  | Ativo                              | Ativo                              |
#	| Notificar titular através de push                                    | Inativo                            | Inativo                            |
#	| Notificar titular através de e-mail                                  | Inativo                            | Inativo                            |
#
#
# EXEMPLO DO LOG DE "INATIVAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO        |
#	| Produto                                                              | Aplicativo                         | Aplicativo                         |
#	| Funcionalidade                                                       | Regras                             | Regras                             |
#	| Transação                                                            | Inativação de regra                | Inativação de regra                |
#	| Analista                                                             | BSI90896 - José Silva              | BSI90896 - José Silva              |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21             |
#	| Regra                                                                | Transações por tempo de deslocamento | Transações por tempo de deslocamento |
#	| Situação                                                             | Ativa                              | Inativa                            |
#	| Gravidade                                                            | Média                              | Média                              |
#	| Operação do período de efetuação das transações                      | Menor ou igual a                   | Menor ou igual a                   |
#	| Período de efetuação das transações                                  | 12                                 | 12                                 |
#	| Tempo do período de efetuação das transações                         | Horas                              | Horas                              |
#	| Operação da qtde. de transações efetuadas                            | Maior ou igual a                   | Maior ou igual a                   |
#	| Qtde. de transações efetuadas                                        | 2                                  | 2                                  |
#	| Operação do valor de cada transação (R$)                             | Maior ou igual a                   | Maior ou igual a                   |
#	| Valor de cada transação (R$)                                         | R$ 500,00                          | R$ 500,00                          |
#	| Operação do período de abertura da conta                             | Maior que                          | Maior que                          |
#	| Período de abertura da conta                                         | 90                                 | 90                                 |
#	| Tempo do período de abertura da conta                                | Dias                               | Dias                               |
#	| Operação do período que o titular não transacionou para o favorecido | Maior que                          | Maior que                          |
#	| Período que o titular não transacionou para o favorecido             | 2                                  | 2                                  |
#	| Tempo do período que o titular não transacionou para o favorecido    | Dias                               | Dias                               |
#	| Transações                                                           | TED                                | TED                                |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital        |
#	| Bloqueio automático                                                  | Ativo                              | Ativo                              |
#	| Notificar titular através de push                                    | Inativo                            | Inativo                            |
#	| Notificar titular através de e-mail                                  | Inativo                            | Inativo                            |
#
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE REGRA": 
#
#	| NOME_DO_ATRIBUTO                                                     | VALOR_ANTERIOR_A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO        |
#	| Produto                                                              | Aplicativo                         | Aplicativo                         |
#	| Funcionalidade                                                       | Regras                             | Regras                             |
#	| Transação                                                            | Alteração de regra                 | Alteração de regra                 |
#	| Analista                                                             | BSI90896 - José Silva              | BSI90896 - José Silva              |
#	| Data/Hora da transação                                               | 30/09/2019 às 15:30:21             | 30/09/2019 às 15:30:21             |
#	| Regra                                                                | Transações por tempo de deslocamento | Transações por tempo de deslocamento |
#	| Situação                                                             | Inativa                            | Ativa                              |
#	| Gravidade                                                            | Média                              | Média                              |
#	| Operação do período de efetuação das transações                      | Menor ou igual a                   | Menor ou igual a                   |
#	| Período de efetuação das transações                                  | 12                                 | 12                                 |
#	| Tempo do período de efetuação das transações                         | Horas                              | Horas                              |
#	| Operação da qtde. de transações efetuadas                            | Maior ou igual a                   | Maior ou igual a                   |
#	| Qtde. de transações efetuadas                                        | 2                                  | 2                                  |
#	| Operação do valor de cada transação (R$)                             | Maior ou igual a                   | Maior ou igual a                   |
#	| Valor de cada transação (R$)                                         | R$ 500,00                          | R$ 1.000,00                        |
#	| Operação do período de abertura da conta                             | Maior que                          | Maior que                          |
#	| Período de abertura da conta                                         | 90                                 | 90                                 |
#	| Tempo do período de abertura da conta                                | Dias                               | Dias                               |
#	| Operação do período que o titular não transacionou para o favorecido | Maior que                          | Maior que                          |
#	| Período que o titular não transacionou para o favorecido             | 2                                  | 2                                  |
#	| Tempo do período que o titular não transacionou para o favorecido    | Dias                               | Dias                               |
#	| Transações                                                           | TED                                | TED                                |
#	| Tipos de contas selecionados                                         | 240 – Pessoa Física Digital        | 240 – Pessoa Física Digital        |
#	| Bloqueio automático                                                  | Ativo                              | Ativo                              |
#	| Notificar titular através de push                                    | Inativo                            | Inativo                            |
#	| Notificar titular através de e-mail                                  | Inativo                            | Inativo                            |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################