﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Automacao.UI.Features.Login
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Perfil de Acesso de Administrador")]
    public partial class PerfilDeAcessoDeAdministradorFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "PerfilAcessoAdministrador.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("pt-br"), "Perfil de Acesso de Administrador", "\tComo usuário Administrador do sistema Cactus\r\n\tEu quero visualizar os menus do s" +
                    "istema de acordo com o meu perfil de acesso\r\n\tPara que não haja acessos a página" +
                    "s e menus não autorizados", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 8
#line 9
 testRunner.Given("que eu acesso o sistema Cactus como Administrador", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Visualizar os menus do sistema de acordo com o meu perfil de acesso")]
        [NUnit.Framework.CategoryAttribute("automatizado")]
        [NUnit.Framework.CategoryAttribute("VerificarAlterado")]
        public virtual void VisualizarOsMenusDoSistemaDeAcordoComOMeuPerfilDeAcesso()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Visualizar os menus do sistema de acordo com o meu perfil de acesso", null, new string[] {
                        "automatizado",
                        "VerificarAlterado"});
#line 17
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 18
 testRunner.When("eu realizo o login no sistema como Administrador", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line hidden
            TechTalk.SpecFlow.Table table11 = new TechTalk.SpecFlow.Table(new string[] {
                        "menu"});
            table11.AddRow(new string[] {
                        "Dashboard"});
            table11.AddRow(new string[] {
                        "Alertas"});
            table11.AddRow(new string[] {
                        "Listas"});
            table11.AddRow(new string[] {
                        "Submenu Bancos confiáveis"});
            table11.AddRow(new string[] {
                        "Submenu Contas bloqueadas"});
            table11.AddRow(new string[] {
                        "Submenu Contas suspeitas"});
            table11.AddRow(new string[] {
                        "Submenu Destinatários e Remetentes Suspeitos"});
            table11.AddRow(new string[] {
                        "Submenu Dispositivos suspeitos"});
            table11.AddRow(new string[] {
                        "Submenu Informações dos titulares"});
            table11.AddRow(new string[] {
                        "Submenu Lista positiva"});
            table11.AddRow(new string[] {
                        "Submenu Motivos de devolução"});
            table11.AddRow(new string[] {
                        "Bureau"});
            table11.AddRow(new string[] {
                        "Onboarading"});
            table11.AddRow(new string[] {
                        "Submenu Onboarding PF"});
            table11.AddRow(new string[] {
                        "Submenu Onboarding PJ"});
            table11.AddRow(new string[] {
                        "Logs"});
            table11.AddRow(new string[] {
                        "Configurações"});
            table11.AddRow(new string[] {
                        "Submenu Regras de alertas"});
            table11.AddRow(new string[] {
                        "Painel Alerta de Fraudes"});
            table11.AddRow(new string[] {
                        "Painel Quantidade de alertas por gravidade"});
            table11.AddRow(new string[] {
                        "Painel Top 10"});
            table11.AddRow(new string[] {
                        "Painel Alertas Gerados"});
            table11.AddRow(new string[] {
                        "Painel Alertas gerados x Tratados x Fraudes por dia"});
#line 19
 testRunner.Then("os seguintes menus são exibidos para o perfil de acesso de Administrador", ((string)(null)), table11, "Então ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Ao visualizar a tela de Alertas que carrega quando selecionamos o Menu Alertas")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        [NUnit.Framework.TestCaseAttribute("Todos", null)]
        [NUnit.Framework.TestCaseAttribute("Comuns", null)]
        [NUnit.Framework.TestCaseAttribute("Funcionários", null)]
        public virtual void AoVisualizarATelaDeAlertasQueCarregaQuandoSelecionamosOMenuAlertas(string tiposDeAlerta, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "automatizar"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Ao visualizar a tela de Alertas que carrega quando selecionamos o Menu Alertas", null, @__tags);
#line 46
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 47
 testRunner.When("eu realizo o login no sistema como Administrador", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 48
 testRunner.Then(string.Format("devem ser apresentados todos os {0}", tiposDeAlerta), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 49
 testRunner.And(string.Format("deve ser apresentado um filtro de {0}", tiposDeAlerta), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
