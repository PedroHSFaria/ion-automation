﻿#language: pt-br

Funcionalidade: Login
	Como usuário do sistema Cactus
	Eu quero realizar login
	Para que possa usufruir do sistema

Contexto: 
	Dado que eu acesso a página de login

@automatizado
Cenario: Autenticar informando dados válidos
	Quando eu preencho os campos com dados válidos
	E eu clico em Entrar
	Então o sistema deve realizar o login com sucesso

@automatizado
Cenario: Autenticar informando dados inválidos
	Quando eu preencho os campos com BSI99999 e 123456
	E eu clico em Entrar
	Então o sistema deve exibir a mensagem "O usuário ou senha está incorreto."

@automatizado
Cenario: Autenticar sem infromar usuário
	Quando eu preencho os campos com "" e 123456
	E eu clico em Entrar
	Então o sistema deve exibir a mensagem "O preenchimento do campo Usuário é obrigatório."

@automatizado
Cenario: Autenticar sem informar senha
	Quando eu preencho os campos com BSI99999 e ""
	E eu clico em Entrar
	Então o sistema deve exibir a mensagem "O preenchimento do campo Senha é obrigatório."

@automatizado
Cenario: Autenticar sem informar usuário e senha
	Quando eu clico em Entrar 
	Então o sistema deve exibir a mensagem "O preenchimento do campo Usuário é obrigatório."
	E o sistema deve exibir a mensagem "O preenchimento do campo Senha é obrigatório."