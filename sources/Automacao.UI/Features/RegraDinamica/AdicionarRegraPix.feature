﻿#language: pt-br

Funcionalidade: Adicionar uma nova configuração de regra dinâmica antifraude para o produto pix
	Como Usuario do sistema
	Quero Adicionar uma nova regra dinâmica pra o produto pix
	Para Monitorar antifraude do produto pix
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu "Configurações"
	E clico no submenu Regras de dinamicas
	E o sistema exibe a tela Regras de dinamicas

@automatizando0707
Cenário: Adicionar uma nova regra para o produto pix
	Dado teste
	E the second number is 70 
	Quando the two numbers are added
	Entao the result should be 120
