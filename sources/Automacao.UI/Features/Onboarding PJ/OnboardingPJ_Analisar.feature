﻿#language: pt-br

Funcionalidade: Onboarding PJ - Alterar a situação da proposta para "Em análise"
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero analisar uma proposta
	Para que possa responder os formulários

Contexto: 
	Dado que estou logado no sistema
	E clico em "Onboarding"
	E clico em "Pessoa jurídica"
	E o sistema apresenta a tela "Onboarding PJ" contendo as propostas do onboarding de PJ da plataforma Bs2 Empresas

@automatizar
Cenario: Alterar a situação da proposta para "Em análise"
	Quando clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	Então o sistema me redireciona para a tela "Proposta" com o formulário da empresa aberto
	E altera a situação da proposta para "Em análise"
	E apresenta a mensagem "Situação alterada para: “Em análise”. Você tem até às 23:59:59 de hoje para finalizar a análise dessa proposta."
	E adiciona a "Data/Hora início da análise" na proposta
	E me coloca como o "Responsável" por analisar essa proposta
	E o sistema grava o log "Alteração de situação"

@automatizar
Cenario: Proposta sendo analisada por outro analista
	Quando clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	E outro usuário clicou em "Analisar" na mesma proposta um pouco antes de mim
	Então o sistema apresenta a mensagem "Desculpe, não será possível analisar essa proposta, pois outro analista iniciou a análise."
	Quando clico em "Ok"
	Então o sistema me retorna para a tela de listagem das propostas 

@automatizar
Cenario: Propostas não analisadas até 23:59:59 do dia
	Quando clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	Então o sistema me redireciona para a tela "Proposta" com o formulário da empresa aberto
	E altera a situação da proposta para "Em análise"
	E apresenta a mensagem "Situação alterada para: “Em análise”. Você tem até às 23:59:59 de hoje para finalizar a análise dessa proposta."
	E adiciona a "Data/Hora início da análise" na proposta
	E me coloca como o "Responsável" por analisar essa proposta
	E o sistema grava o log "Alteração de situação"
	Quando não alterar o status da proposta para um dos status abaixo até às 23:59:59 do dia atual
	| STATUS             |
	| Concluída          |
	| Pendente           |
	| Falha no envio     |
	| Aguardando análise |
	Então o sistema deve alterar a situação da proposta para a situação anterior a de "Em análise" às 00:00:00 do dia seguinte
	E deve limpar a "Data/Hora início da análise"
	E deve me remover de "Responsável" por analisar a proposta
	E o sistema grava o log "Alteração de situação"


######################################## PROTÓTIPOS #########################################
#
#	Mensagem de situação alterada para "Em análise": https://marvelapp.com/7e20jdb/screen/62378443
#	Tela "Proposta" - Formulário da empresa: https://marvelapp.com/7e20jdb/screen/62378446
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################									
#
#	MENSAGENS:
#
#		| MENSAGEM                                                                                                        | TEMPO DE EXIBIÇÃO |
#		| Situação alterada para: “Em análise”. Você tem até às 23:59:59 de hoje para finalizar a análise dessa proposta. | 5 segundos        |
#
#	FORMULÁRIOS: 
#
#		O sistema deve apresentar os seguintes formulários na seguinte ordem:
#			1) Um formulário para a empresa;
#			2) Um formulário para o sócio que preencheu a proposta (terá o quiz e as perguntas referentes ao quiz);
#			3) Um formulário para os demais sócios da proposta (não terá o quiz e não terá as perguntas referentes ao quiz).
#
#	FUNCIONALIDADE DO LOG "ONBOARDING PJ": 
#
#		EXEMPLO DO LOG DE "ALTERAÇÃO DE SITUAÇÃO": 
#
#			| NOME_DO_ATRIBUTO                                                                                                   | VALOR_ANTERIOR_A_TRANSACAO | VALOR_POSTERIOR_A_TRANSACAO |
#			| Produto                                                                                                            | Aplicativo                 | Aplicativo                  |
#			| Funcionalidade                                                                                                     | Onboarding PJ              | Onboarding PJ               |
#			| Transação                                                                                                          | Alteração de situação      | Alteração de situação       |
#			| Analista                                                                                                           | BSI90896 - José Silva      | BSI90896 - José Silva       |
#			| Data/Hora da transação                                                                                             | 30/09/2019 às 15:30:21     | 30/09/2019 às 15:30:21      |
#			| Seed                                                                                                               | 103986                     | 103986                      |
#			| Razão social                                                                                                       | Empresa A                  | Empresa A                   |
#			| CNPJ                                                                                                               | 09.163.687/0001-51         | 09.163.687/0001-51          |
#			| E-mail                                                                                                             | empresa@teste.com          | empresa@teste.com           |
#			| Data/Hora da abertura                                                                                              | 30/03/2018 às 14:49:23     | 30/03/2018 às 14:49:23      |
#			| Data/Hora início da análise                                                                                        |                            | 30/03/2018 às 15:00:00      |
#			| Data/Hora de conclusão                                                                                             |                            |                             |
#			| Situação                                                                                                           | Aguardando análise         | Em análise                  |
#			| FORMULÁRIO DA EMPRESA                                                                                              |                            |                             |
#			| 1. A empresa está ativa no bureau ou na Receita Federal?                                                           |                            |                             |
#			| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                              |                            |                             |
#			| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                              |                            |                             |
#			| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?                    |                            |                             |
#			| 5. O domínio do e-mail pertence à empresa da proposta?                                                             |                            |                             |
#			| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                                |                            |                             |
#			| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?                      |                            |                             |
#			| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?                      |                            |                             |
#			| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal?              |                            |                             |
#			| FORMULÁRIO DO SÓCIO QUE PREENCHEU A PROPOSTA                                                                       |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            |                             |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            |                             |
#			| 3. A foto do documento é válida?                                                                                   |                            |                             |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            |                             |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            |                             |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            |                             |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            |                             |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            |                             |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            |                             |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            |                             |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            |                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            |                             |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            |                             |
#			| 14. As perguntas do quiz foram exibidas?                                                                           |                            |                             |
#			| 15. O cliente acertou todas as perguntas do quiz?                                                                  |                            |                             |
#			| 16. O cliente errou mais de uma pergunta do quiz?                                                                  |                            |                             |
#			| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            |                             |
#			| FORMULÁRIO DOS DEMAIS SÓCIOS DA PROPOSTA                                                                           |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            |                             |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            |                             |
#			| 3. A foto do documento é válida?                                                                                   |                            |                             |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            |                             |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            |                             |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            |                             |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            |                             |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            |                             |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            |                             |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            |                             |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            |                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            |                             |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            |                             |
#			| 14. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            |                             |
#			| Observação                                                                                                         |                            |                             |
#
#	*Atualizar o "Relatório dos logs" para incluir os novos logs.
#
######################################################################################