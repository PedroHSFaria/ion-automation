﻿#language: pt-br
#Protótipos: https://marvelapp.com/7e20jdb/screen/62175366

Funcionalidade: Onboarding PJ - Formulário da empresa
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero acessar o formulário da empresa
	Para que possa respondê-lo

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	E clico em "Analisar" de uma proposta
	Então o sistema me redireciona para a tela "Proposta" com o formulário da empresa aberto
	E apresenta os demais formulários desabilitados 

@automatizar
Cenario: Responder as perguntas do formulário da empresa
	Quando respondo as perguntas do formulário da empresa
	| PERGUNTA                                                                                              | RESPOSTA        |
	| 1. A empresa está ativa no bureau ou na Receita Federal?                                              | Resposta válida |
	| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                 | Resposta válida |
	| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                 | Resposta válida |
	| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?       | Resposta válida |
	| 5. O domínio do e-mail pertence à empresa da proposta?                                                | Resposta válida |
	| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                   | Resposta válida |
	| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?         | Resposta válida |
	| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?         | Resposta válida |
	| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal? | Resposta válida |
	| Observação                                                                                            | Resposta válida |
	Então o sistema apresenta as perguntas respondidas de acordo com as opções marcadas

@automatizar
Cenario: Acessar site da Receita Federal
	Quando clico em "Receita Federal" em uma das perguntas que possui esse termo
	Então o sistema abre em uma nova guia o link http://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/cnpjreva_solicitacao.asp para que valide o CNPJ na Receita Federal

@automatizar
Cenario: Visualizar ajuda
	Quando passar o cursor do mouse sobre o ícone de "Ajuda" de uma pergunta
	Então o sistema deve apresentar o texto de ajuda da pergunta

@automatizar
Cenario: Maximizar todas as abas
	Quando todas as abas estão minimizadas
	E clico em "Maximizar todas as abas"
	Então o sistema expande todas as abas de todos os formulários

@automatizar	
Cenario: Minimizar todas as abas
	Quando todas as abas estão maximizadas
	E clico em "Minimizar todas as abas"
	Então o sistema minimiza todas as abas de todos os formulários 

@automatizar
Cenario: Maximizar uma aba
	Quando uma aba está minimizada
	E clico no "Maximizar" dessa aba
	Então o sistema expande a respectiva aba

@automatizar	
Cenario: Minimizar uma aba
	Quando uma aba está maximizada
	E clico no "Minimizar" dessa aba
	Então o sistema minimiza a respectiva aba

@automatizar
Cenario: Visualizar próximo formulário
	Quando respondo todas as perguntas obrigatórias do formulário da empresa
	E clico em "Próximo"
	Então o sistema habilita o formulário do sócio que preencheu a proposta
	E me redireciona para o formulário do sócio que preencheu a proposta

@automatizar
Cenario: Visualizar próximo formulário - Perguntas obrigatórias não preenchidas
	Quando não respondo todas as perguntas obrigatórias do formulário da empresa
	E clico em "Próximo"
	Então o sistema apresenta a mensagem "É preciso responder essa pergunta para concluir a proposta." em todas as perguntas obrigatórias não respondidas
	E me redireciona para a primeira pergunta obrigatória não respondida


######################################## PROTÓTIPO #########################################
#
#	Tela "Proposta" - Formulário da empresa: https://marvelapp.com/7e20jdb/screen/62378446
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
#	LINK "RECEITA FEDERAL":
#
#		- O sistema deve apresentar o termo "Receita Federal" nas perguntas como link.
#		- Ao clicar no link "Receita Federal", o sistema deve abrir em uma nova guia o link: http://www.receita.fazenda.gov.br/pessoajuridica/cnpj/cnpjreva/cnpjreva_solicitacao.asp
#
#	FORMULÁRIO DA EMPRESA:
#
#		| PERGUNTAS                                                                                             | TIPO        | OPÇÕES DE RESPOSTAS  | OBRIGATÓRIO                                   | PLACEHOLDER            | TAMANHO MÁXIMO |
#		| 1. A empresa está ativa no bureau ou na Receita Federal?                                              | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                 | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                 | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?       | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 5. O domínio do e-mail pertence à empresa da proposta?                                                | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                   | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?         | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?         | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal? | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| Observação                                                                                            | String      | N/A                  | Apenas quando o botão "Pendente" for acionado | Escreva sua observação | 500 caracteres |
#			
#		MENSAGENS ABAIXO DAS PERGUNTAS:
#
#			Pergunta 5:
#				Clique uma vez sobre o domínio do e-mail da proposta, para indicar com o ícone <ícone de duas barras verdes> que ele pertence à empresa. E clique duas vezes para indicar com o ícone <dois losangos vermelhos> que o domínio não pertence à empresa.
#
#		AJUDA (ÍCONES DE INTERROGAÇÃO AO LADO DAS PERGUNTAS):
#
#			Pergunta 1: A empresa está ativa no bureau ou na Receita Federal? 
#
#				Quando o status da empresa não for retornado pelo bureau, deve-se efetuar a consulta na Receita Federal, utilizando o CNPJ da empresa. Se o status da empresa for igual a "Ativa", você deve responder essa pergunta com a opção "Sim". Caso o status da empresa seja diferente de "Ativa", você deve responder com a opção "Não". E caso não seja possível consultar o status, deve responder como "N/A". 
#
#			Pergunta 2: A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?  
#
#				Quando a razão social não for retornada pelo bureau, deve-se efetuar a consulta na Receita Federal, utilizando o CNPJ da empresa. Se a razão social informada na proposta for igual a retornada pelo bureau, você deve responder essa pergunta com a opção "Sim". Caso a razão social informada na proposta seja diferente da retornada pelo bureau, você deve responder com a opção "Não". E caso não seja possível consultar a razão social, deve responder como "N/A".
#			
#			Pergunta 3: O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal? 
#
#				Quando o endereço da empresa não for retornado pelo bureau, deve-se efetuar a consulta na Receita Federal, utilizando o CNPJ da empresa. Se o endereço informado na proposta for igual a um dos retornados pelo bureau, você deve responder essa pergunta com a opção "Sim". Caso o endereço informado na proposta seja diferente dos retornados pelo bureau, você deve responder com a opção "Não". E caso não seja possível consultar o endereço da empresa, deve responder como "N/A".
#
#			Pergunta 4: O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?    
#
#				Quando o telefone da empresa não for retornado pelo bureau, deve-se efetuar a consulta na Receita Federal, utilizando o CNPJ da empresa. Se o telefone informado na proposta for igual a um dos retornados pelo bureau, você deve responder essa pergunta com a opção "Sim". Caso o telefone informado na proposta seja diferente dos retornados pelo bureau, você deve responder com a opção "Não". E caso não seja possível consultar o telefone da empresa, deve responder como "N/A".
#
#			Pergunta 5: O domínio do e-mail pertence à empresa da proposta?    
#
#				Quando o domínio do e-mail informado na proposta pertencer à empresa, você deve responder essa pergunta com a opção "Sim". Caso o domínio informado na proposta não pertença à empresa, você deve responder com a opção "Não". E caso o domínio da proposta não seja exibido, deve responder como "N/A".
#
#			Pergunta 6: O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?          
#
#				Quando o e-mail da empresa não for retornado pelo bureau, deve-se efetuar a consulta na Receita Federal, utilizando o CNPJ da empresa. Se o e-mail informado na proposta for igual a um dos retornados pelo bureau, você deve responder essa pergunta com a opção "Sim". Caso o e-mail informado na proposta seja diferente dos retornados pelo bureau, você deve responder com a opção "Não". E caso não seja possível consultar o e-mail da empresa, deve responder como "N/A".
#				
#			Pergunta 7: A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?    
#
#				Quando a quantidade de sócios da empresa não for retornada pelo bureau, deve-se efetuar a consulta na Receita Federal, utilizando o CNPJ da empresa. Se a quantidade de sócios da proposta for igual a retornada pelo bureau, você deve responder essa pergunta com a opção "Sim". Caso a quantidade de sócios da proposta seja diferente da retornada pelo bureau, você deve responder com a opção "Não". E caso não seja possível consultar a quantidade de sócios, deve responder como "N/A".
#
#			Pergunta 8: Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?    
#
#				Quando os nomes dos sócios da empresa não forem retornados pelo bureau, deve-se efetuar a consulta na Receita Federal, utilizando o CNPJ da empresa. Se os nomes de todos os sócios da proposta forem iguais aos retornados pelo bureau, você deve responder essa pergunta com a opção "Sim". Caso o nome de algum sócio da proposta não conferir com o retornado pelo bureau, você deve responder com a opção "Não". E caso não seja possível consultar os nomes dos sócios, deve responder como "N/A".
#
#			Pergunta 9: O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal?
#
#				Quando o percentual de participação dos sócios não forem retornados pelo bureau, deve-se efetuar a consulta na Receita Federal, utilizando o CNPJ da empresa. Se o percentual de participação de todos os sócios da proposta forem iguais aos retornados pelo bureau, você deve responder essa pergunta com a opção "Sim". Caso o percentual de participação de algum sócio da proposta não conferir com o retornado pelo bureau, você deve responder com a opção "Não". E caso não seja possível consultar o percentual de participação dos sócios, deve responder como "N/A".
#
#		COMPORTAMENTOS DAS PERGUNTAS:
#
#			Pergunta 1: A empresa está ativa no bureau ou na Receita Federal?
#				- Se o status da empresa retornado pelo bureau for igual a "Ativa", os sistema deve destacar o status em verde. Se for diferente, deve destacá-lo em vermelho.
#
#			Pergunta 2: A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?
#				- Se a razão social da proposta for igual a razão social retornada pelo bureau, o sistema deve destacar ambas em verde. 
#				- Caso a razão social da proposta não seja igual a razão social retornada pelo bureau, o sistema deve destacar ambas em vermelho.
#				- Se nenhuma razão social for retornada pelo bureau, o sistema não deve destacar a razão social da proposta.
#
#			Pergunta 3:
#				- N/A
#
#			Pergunta 4: O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?
#				- Se o telefone da proposta for igual a um dos telefones retornados pelo bureau (DDD + número do telefone), o sistema deve destacar ambos em verde e os demais telefones retornados pelo bureau devem ser destacados em vermelho. 
#				- Caso o telefone da proposta não seja igual a um dos telefones retornados pelo bureau, o sistema deve destacar todos os telefones em vermelho, tanto o da proposta quanto os retornados pelo bureau. 
#				- Se nenhum telefone for retornado pelo bureau, o sistema não deve destacar os telefones.
#
#			Pergunta 5: O domínio do e-mail pertence à empresa da proposta?
#				- O sistema deve apresentar o campo "Domínio do e-mail" que deve conter apenas o domínio do e-mail da empresa informado na proposta. 
#				- Ao clicar uma vez sobre o campo "Domínio do e-mail", o sistema deve checar o campo como validado e deve destacá-lo em verde. 
#				- Ao clicar duas vezes sobre o campo não destacado (cor preta) ou clicar uma vez quando ele já estiver destacado em verde, o sistema deve checar o campo como não validado e deve destacá-lo em vermelho.
#				- O sistema deve responder automaticamente essa pergunta com a opção de resposta "Sim" quando o campo "Domínio do e-mail" for validado, ou seja, quando o campo estiver destacado em verde.
#				- O sistema deve responder automaticamente essa pergunta com a opção de resposta "Não" quando o campo "Domínio do e-mail" não for validado, ou seja, quando o campo estiver destacado em vermelho.
#				- Se o usuário responder essa pergunta manualmente, ao selecionar a opção de resposta "Sim", o sistema deve checar automaticamente o campo "Domínio do e-mail" como validado e deve destacá-lo em verde. 
#				- Ao selecionar a opção de resposta "Não", o sistema deve checar automaticamente o campo "Domínio do e-mail" como não validado e deve destacá-lo em vermelho.
#
#			Pergunta 6: O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?
#				- Se o e-mail da proposta for igual a um dos e-mails retornados pelo bureau, o sistema deve destacar ambos em verde e os demais e-mails retornados pelo bureau devem ser destacados em vermelho. 
#				- Caso o e-mail da proposta não seja igual a um dos e-mails retornados pelo bureau, o sistema deve destacar todos os e-mails em vermelho, tanto o da proposta quanto os retornados pelo bureau. 
#				- Se nenhum e-mail for retornado pelo bureau, o sistema não deve destacar os e-mails.
#
#			Pergunta 7: A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?
#				- Se a quantidade de sócios da proposta for igual a quantidade retornada pelo bureau, o sistema deve destacar ambas em verde.
#				- Caso a quantidade de sócios da proposta não seja igual a quantidade retornada pelo bureau, o sistema deve destacar ambas em vermelho.
#				- Se nenhum sócio for retornado pelo bureau, o sistema não deve destacar a quantidade de sócios da proposta.
#				
#			Pergunta 8: Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?
#				- Se houver sócios da proposta retornados pelo bureau, o sistema deve destacar ambos em verde e os demais sócios da proposta que não batem com os sócios retornados pelo bureau devem ser destacados em vermelho. 
#				- Caso nenhum sócio da proposta bata com os sócios retornados pelo bureau, o sistema deve destacar todos os sócios em vermelho. 
#				- Se nenhum sócio for retornado pelo bureau, o sistema não deve destacar os sócios da proposta. 
#
#			Pergunta 9: O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal?
#				- Para comparar o percentual de participação dos sócios da proposta com o percentual retornado pelo bureau, o sistema internamente deve efetuar a comparação utilizado o "Percentual de participação do sócio" e o "CPF/CNPJ do sócio", para ser mais assertivo. Pórém na tela, o sistema deve apresentar o "Percentual de participação do sócio" e o "Nome do sócio", ou seja, o "CPF/CNPJ do sócio" será utilizado apenas internamente, o mesmo não será exposto na tela.
#				- Se o percentual de participação do sócio da proposta bater com o retornado pelo bureau, o sistema deve destacar ambos em verde e os demais percentuais da proposta que não batem com os retornados pelo bureau devem ser destacados em vermelho.
#				- Caso nenhum percentual da proposta bata com os percentuais retornados pelo bureau, o sistema deve destacar todos os percentuais em vermelho.
#				- Se nenhum percentual for retornado pelo bureau, o sistema não deve destacar os percentuais.
#
#	BOTÕES "PRÓXIMO" E "ANTERIOR":
#		- O sistema deve apresentar o botão "Próximo" em todos os formulários, exceto o último.
#		- O sistema deve apresentar o botão "Anterior" em todos os formulários, exceto o primeiro que é o da empresa.
#
################################ ESPECIFICAÇÃO TÉCNICA ###################################	
#
#	ATRIBUTOS DOS DADOS DO BUREAU
#
#		- Status da empresa: Utilizar o atributo "descricao" do objeto "bureau".
#
#			Exemplo:
#
#			    "situacao":{ 
#                "data":"2014-12-19T03:00:00Z",
#			    "descricao":"ATIVA"
#				 },   
#
#		- Razão social: Utilizar o atributo "razaoSocial" do objeto "bureau".
#
#			Exemplo:
#
#				"razaoSocial":"GLEISON HENRIQUE ALVES MEI"
#			
#		- Endereço: Utilizar a máscara "Rua, Número, Bairro, Município – UF, CEP" e utilizar os atributos "logradouro", "numero", "bairro", "municipio", "uf" e "cep" do objeto "bureau".
#
#			Exemplo: 
#				"endereco":{ 
#				    "bairro":"JARDIM DA BARRAGEM III",
#					 "bairroOriginal":"JARDIM DA BARRAGEM III",
#					 "cep":"72920533",
#					 "enderecoResidencial":false,
#					 "logradouro":"QUADRA 1A",
#					 "mesoRegiao":"LESTE GOIANO",
#					 "municipio":"AGUAS LINDAS DE GOIAS",
#					 "municipioOriginal":"AGUAS LINDAS DE GOIAS",
#					 "numero":"60",
#					 "precisao":"SEM INFORMACAO",
#					 "uf":"GO"
#					},                  
#
#		- Telefones: Utilizar o atributo "numero" do objeto "bureau".
#
#			"telefones":[ 
#			{ 
#				 "numero":"61996993003"
#			 }
#			],
#
#		- E-mails: Utilizar o atributo "emailRF" do objeto "bureau".
#
#			Exemplo:
#				 "info":{ 
#				 "emailRF":"HENRYALVESHENRIQUE@GMAIL.COM",
#				 "idadeEmpresa":4,
#				 "qsaDivergente":false
#				},  
#
#		- Sócios: Utilizar o atributo "nome" do obejeto "bureau". 
#
#			Exemplo:
#				"socios":[ 
#				 { 
#				   "documento":"72501154134",
#				   "falecido":false,
#				   "nivelPep":"NAO IDENTIFICADO",
#				   "nome":"GLEISON HENRIQUE ALVES",
#				   "participacaoSocietaria":100,
#				   "qualificacao":"SOCIO-ADMINISTRADOR"
#				}
#				],
#
#		- Percentual de participação dos sócios: Utilizar os atributos "documento", "nome" e "participacaoSocietaria" do objeto "bureau".
#
#				"socios":[ 
#			    { 
#			       "documento":"72501154134",
#			       "falecido":false,
#			       "nivelPep":"NAO IDENTIFICADO",
#			       "nome":"GLEISON HENRIQUE ALVES",
#			       "participacaoSocietaria":100,
#			       "qualificacao":"SOCIO-ADMINISTRADOR"
#			    }
#			 ],
#			 			 
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################