﻿#language: pt-br

Funcionalidade: Onboarding PJ - Listagem das propostas na visão dos perfis "Administrador", "Coordenador BPO" e "Analista"
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO" ou "Analista"
	Quero visualizar as propostas do onboarding de PJ da plataforma Bs2 Empresas
	Para que possa consultar as propostas já respondidas e as pendentes de análise

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	Então o sistema apresenta a tela "Onboarding PJ" contendo as propostas do onboarding de PJ da plataforma Bs2 Empresas

@automatizar
Cenario: Filtrar propostas do onboarding de PJ
	Quando preencho os campos de filtros desejados
	| CAMPOS DE FILTRO            | VALOR           |
	| Seed                        | Valor inteiro   |
	| Situação                    | Situação válida |
	| Data/Hora de abertura       | Período válido  |
	| Data/Hora de conclusão      | Período válido  |
	| Razão social                | Nome válido     |
	| CNPJ                        | CNPJ válido     |
	| E-mail                      | E-mail válido   |
	| Responsável                 | Nome válido     |
	| Fraude                      | Opção válida    |
	| Natureza jurídica permitida | Opção válida    |
	E clico em "Filtrar"
	Então o sistema recupera e apresenta as propostas de acordo com os filtros aplicados

@automatizar
Cenario: Limpar filtro
	Quando preencho os campos de filtros desejados
	| CAMPOS DE FILTRO            | VALOR           |
	| Seed                        | Valor inteiro   |
	| Situação                    | Situação válida |
	| Data/Hora de abertura       | Período válido  |
	| Data/Hora de conclusão      | Período válido  |
	| Razão social                | Nome válido     |
	| CNPJ                        | CNPJ válido     |
	| E-mail                      | E-mail válido   |
	| Responsável                 | Nome válido     |
	| Fraude                      | Opção válida    |
	| Natureza jurídica permitida | Opção válida    |
	E clico em "Filtrar"
	E clico em "Limpar"
	Então o sistema limpa os filtros aplicados anteriormente
	E recupera e apresenta os registros sem nenhum filtro aplicado

@automatizar
Cenario: Nenhuma proposta de onboarding de PJ foi efetuada
	Quando nenhuma proposta de onboarding de PJ foi efetuada
	Então o sistema apresenta a mensagem "Não existem propostas para serem listadas no momento."

@automatizar
Cenario: Nenhuma proposta de onboarding de PJ foi encontrada
	Quando preencho os campos de filtros desejados
	| CAMPOS DE FILTRO            | VALOR           |
	| Seed                        | Valor inteiro   |
	| Situação                    | Situação válida |
	| Data/Hora de abertura       | Período válido  |
	| Data/Hora de conclusão      | Período válido  |
	| Razão social                | Nome válido     |
	| CNPJ                        | CNPJ válido     |
	| E-mail                      | E-mail válido   |
	| Responsável                 | Nome válido     |
	| Fraude                      | Opção válida    |
	| Natureza jurídica permitida | Opção válida    |
	E clico em "Filtrar"
	E o sistema não encontra nenhuma proposta de acordo com os filtros aplicados
	Então apresenta a mensagem "Não existem propostas para serem listadas no momento."


######################################## PROTÓTIPOS #########################################
#
#	Opção de menu "Onboarding": https://marvelapp.com/7e20jdb/screen/62378250
#	Tela "Onboarding PJ": https://marvelapp.com/7e20jdb/screen/62378439
#	Tela "Proposta": https://marvelapp.com/7e20jdb/screen/62378252
#	Opções de exportação dos relatórios: https://marvelapp.com/7e20jdb/screen/62473850
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################									
#
#	CAMPOS DE FILTROS DA TELA "ONBOARDING PJ":
#
#		| CAMPO                       | TIPO     | VALORES                                                                               | OBRIGATORIO | DESABILITADO | MASCARA                | PLACEHOLDER            | VALOR_INICIAL      |
#		| Seed                        | Inteiro  | ID da solicitação                                                                     | Não         | Não          | N/A                    | ID da solicitação      | N/A                |
#		| Situação                    | Combobox | "Todas", "Aguardando análise", "Em análise", "Pendente", Concluída e "Falha no envio" | Não         | Não          | N/A                    | N/A                    | Aguardando análise |
#		| Data/Hora de abertura       | DateTime | Intervalo de data/hora                                                                | Não         | Não          | DD/MM/AAAA às HH:MM:SS | Data/Hora da proposta  | N/A                |
#		| Data/Hora de conclusão      | DateTime | Intervalo de data/hora                                                                | Não         | Não          | DD/MM/AAAA às HH:MM:SS | Data/Hora de conclusão | N/A                |
#		| Razão social                | String   | Nome da empresa                                                                       | Não         | Não          | N/A                    | Nome da empresa        | N/A                |
#		| CNPJ                        | String   | CNPJ da empresa                                                                       | Não         | Não          | 99.999.999/9999-99     | CNPJ da empresa        | N/A                |
#		| E-mail                      | String   | E-mail da empresa                                                                     | Não         | Não          | teste@teste.com        | E-mail da empresa      | N/A                |
#		| Responsável                 | String   | Nome do responsável atribuído a proposta                                              | Não         | Não          | N/A                    | Nome do responsável    | N/A                |
#		| Fraude                      | Combobox | "Todas", "Sim" e "Não"                                                                | Não         | Não          | N/A                    | N/A                    | Todas              |
#		| Natureza jurídica permitida | Combobox | "Todas", "Sim" e "Não"                                                                | Não         | Não          | N/A                    | N/A                    | Todas              |
#	
#	CAMPOS DE FILTROS CUJA BUSCA DEVE SER PARCIAL: 
#
#		| CAMPO        |
#		| Razão social |
#		| CNPJ         |
#		| E-mail       |
#		| Responsável  |
#
#	COLUNAS DA TELA "ONBOARDING PJ":
#		
#		| COLUNA                 | TIPO     | VALORES                                                                                  | MASCARA                |
#		| Seed                   | Inteiro  | ID da solicitação                                                                        | N/A                    |
#		| Razão social           | String   | Nome da empresa                                                                          | N/A                    |
#		| CNPJ                   | String   | CNPJ da empresa                                                                          | 99.999.999/9999-99     |
#		| E-mail                 | String   | E-mail da empresa                                                                        | teste@teste.com        |
#		| Data/Hora de abertura  | DateTime | Data/Hora cuja proposta foi gerada                                                       | DD/MM/AAAA às HH:MM:SS |
#		| Responsável            | String   | Nome do responsável atribuído a proposta                                                 | N/A                    |
#		| Data/Hora de conclusão | DateTime | Data/hora cuja situação da proposta foi alterada para "Concluída"                        | DD/MM/AAAA às HH:MM:SS |
#		| Situação               | String   | "Aguardando análise", "Em análise", "Pendente", Concluída e "Falha no envio"             | N/A                    |
#		| Fraude                 | String   | "Sim" e "Não"                                                                            | N/A                    |
#		| Natureza permitida     | String   | "Sim" e "Não"                                                                            | N/A                    |
#		| Ações                  | N/A      | Botões "Analisar"e "Ver detalhes" e Ícones "Proposta sem contrato" e "Proposta sem foto" | N/A                    |
#	
#	ORDENAÇÃO DA LISTA DE PROPOSTAS DO ONBOARDING DE PJ:
#
#		| CRITÉRIO                                                            | ORDENAÇÃO                                                                                                                   |  
#		| Contrato e foto                                                     | "Proposta com contrato e foto", "Proposta apenas com contrato", "Proposta apenas com foto" e "Proposta sem contrato e foto" |
#		| Consulta dos dados dos sócios já efetuada no bureau da BigData Corp | "Sim" e "Não"                                                                                                               |
#		| Situação                                                            | "Aguardando análise", "Em análise", "Pendente", "Falha no envio" e "Concluída"                                              |
#		| Data/Hora de abertura                                               | Ordem crescente                                                                                                             |
#		
#	TOOLTIPS:
#
#		| BOTAO/ICONE                                       | TOOLTIP               |
#		| Analisar (ícone "lápis")                          | Analisar              |
#		| Proposta sem contrato (ícone "documento cortado") | Proposta sem contrato |
#		| Proposta sem foto (ícone "câmera cortada")        | Proposta sem foto     |  
#
################################# GRUPOS AD ####################################									
#
#	GRUPOS DE FUNCIONALIDADES NO AD:
#		
#		AMBIENTES "DSV", "QA" E "HML": 
#
#			| GRUPOS DE FUNCIONALIDADES                      |
#			| FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_HML           |
#			| FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_HML           |
#			| FRAUDPREV_RELATORIO_FORMPOSITIVACAOPJ_VIEW_HML |
#
#		AMBIENTE "PRD": 
#
#			| GRUPOS DE FUNCIONALIDADES                      |
#			| FRAUDPREV_RELATORIO_FORMPOSITIVACAOPJ_VIEW_PRD |
#			| FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_PRD           |
#			| FRAUDPREV_RELATORIO_FORMPOSITIVACAOPJ_VIEW_PRD |
#
#	GRUPOS DE USUÁRIOS (PERFIS DE ACESSOS) ATRIBUÍDOS AOS GRUPOS DE FUNCIONALIDADES: 
#	
#		AMBIENTES "DSV", "QA" E "HML": 
#
#			| GRUPOS DE USUÁRIOS           | GRUPOS DE FUNCIONALIDADES                                                                                                   |
#			| FRAUDPREV_ADMIN_HML          | FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_HML, FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_HML e FRAUDPREV_RELATORIO_FORMPOSITIVACAOPJ_VIEW_HML |
#			| FRAUDPREV_COORDENADORBPO_HML | FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_HML, FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_HML e FRAUDPREV_RELATORIO_FORMPOSITIVACAOPJ_VIEW_HML |                                                                                                                            
#			| FRAUDPREV_ANALISTA_HML       | FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_HML e FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_HML                                                 |
#			| FRAUDPREV_BPO_HML            | FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_HML e FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_HML                                                 |
#
#		AMBIENTE "PRD": 
#
#			| GRUPOS DE USUÁRIOS           | GRUPOS DE FUNCIONALIDADES                                                                                                   |
#			| FRAUDPREV_ADMIN_PRD          | FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_PRD, FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_PRD e FRAUDPREV_RELATORIO_FORMPOSITIVACAOPJ_VIEW_PRD |
#			| FRAUDPREV_COORDENADORBPO_PRD | FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_PRD, FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_PRD e FRAUDPREV_RELATORIO_FORMPOSITIVACAOPJ_VIEW_PRD |                                                                                                                            
#			| FRAUDPREV_ANALISTA_PRD       | FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_PRD e FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_PRD                                                 |
#			| FRAUDPREV_BPO_PRD            | FRAUDPREV_FORMPOSITIVACAOPJ_VIEW_PRD e FRAUDPREV_FORMPOSITIVACAOPJ_EDIT_PRD                                                 |
#	
################################# IDS PARA RECUPERAR OS DOCUMENTOS DO ÁBARIS ####################################									
#
#	Tipo de Documento: BS2 Empresas (ID = 52)
#	Índices: CNPJ (ID = 38) e Categoria Documento (ID = 103) 
#
#	O padrão utilizado para o preenchimento dos índices na inclusão dos documentos é a seguinte:
#
#		•	CNPJ
#			o	CNPJ da Empresa
#
#		•	Categoria Documento (Temos as 2 opções)
#			o	Contrato Social (Documento da Empresa)
#			o	Documento de Identificação (Documentos dos sócios)
#
#
################################# REGRAS DE NEGÓCIO ###################################	
#
#	| ID    | REGRA DE NEGÓCIO                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
#	| RN001 | O sistema deve atualizar automaticamente a lista de propostas de 1 em 1 minuto, para que as novas propostas sejam apresentadas na listagem sem que o usuário precise atualizar a tela ou efetuar algum filtro.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
#	| RN002 | O sistema deve manter a tela “Proposta” aberta até que a proposta seja concluída ou até que o usuário opte por [Sair] do mesmo. Sendo assim, quando o usuário que estiver na tela “Proposta” e clicar em outra opção de menu e depois acessar a opção de menu “Onboarding” novamente, o sistema deve apresentar a tela “Proposta” da respectiva proposta aberta para análise anteriormente, ou seja, a tela de edição da proposta que ainda se encontra “Em análise”. Se a proposta já não estiver mais “Em análise”, o sistema não deve apresentar a tela “Proposta” da respectiva proposta, quando o usuário acessar novamente a opção de menu “Onboarding”. Pois pode acontecer do usuário não conseguir concluir a proposta dentro do prazo estabelecido, então o sistema alterará a situação da proposta automaticamente. Então nesse caso, quando o usuário acessar novamente a opção de menu “Onboarding”, o sistema deve apresentar a tela de listagem das propostas. E nesse caso, como o analista poderá navegar entre as demais telas do sistema e voltar para a proposta, para concluí-la, o sistema deverá armazenar as respostas na memória do navegador (ex.: em cache), para que o analista não perca as suas respostas caso tenha que acessar outra página antes de concluir a proposta. |
#	| RN003 | O sistema deve habilitar a proposta para edição apenas se a empresa tiver ao menos um contrato social e ao menos uma foto para cada um dos sócios no Ábaris. Ex.: Se a empresa tem 3 sócios, o sistema poderá habilitar a proposta para edição apenas se ela tiver ao menos um contrato social (Categoria do Ábaris: "Contrato Social") e no mínimo 3 fotos dos sócios no Ábaris (Categoria do Ábaris: "Documento de Identificação").                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
#	| RN004 | O sistema deve ter um serviço que efetuará a consulta dos dados dos sócios da empresa, de 10 em 10 segundos, nos datasets "Endereços", "Telefones" e "E-mails" do bureau da BigData Corp, para compor as perguntas dos formulários dos sócios. Sendo que, antes de efetuar a consulta diretamente no bureau da BigData, o sistema deve verificar se já existe uma consulta nos últimos 3 meses com os dados necessários (Datasets: "Endereços", "Telefones" e "E-mails") para o respectivo sócio, na tabela "BUREAU" do banco "BBSFRAUDE". Se existir, o sistema não deve efetuar uma nova consulta na BigData, deve apenas recuperar a consulta existente na base "BBSFRAUDE". Se não existir, o sistema deve efetuar a consulta no bureau da BigDataCorp e deve gravar o resultado dessa consulta na tabela "BUREAU" e deve gravar o log na tabela "BUREAU_LOG" do banco BBSFRAUDE.                                                                                                                                                                                                                                                                                                                                                                                                                     |
#	| RN005 | O sistema não deve apresentar o botão "Analisar" quando uma proposta estiver com situação igual a "Em análise" e estiver atribuída para outro usuário que não seja o logado no sistema.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
#
################################# TOKEN PARA CONSULTAS NA BIGDATA CORP REFERENTES AO ONBOARDING PJ ###################################	
#
#	Para efetuar as consultas dos dados dos sócios no bureau da BigData Corp, é necessário usar o token abaixo, para diferenciarmos os custos das consultas:
#
#		Token: 39aa7e89-5f68-4cf7-839f-1feb2d8d39f7
#
#	*Esse token é da conta do Igor Theodoro, responsável pelo PJ. Não podemos usar o token que já usamos hoje no Cactus, que é da conta da Keyla Silva.
#
######################################################################################