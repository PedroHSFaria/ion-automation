﻿#language: pt-br

Funcionalidade: Onboarding PJ - Ver detalhes de uma proposta
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero visualizar os detalhes de uma proposta com situação igual a "Concluída" ou "Falha no envio"
	Para que possa visualizar as propostas já concluídas ou que estão com falha de envio

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	Então o sistema apresenta a tela "Onboarding PJ" contendo as propostas do onboarding de PJ da plataforma Bs2 Empresas

@automatizar
Cenario: Ver detalhes de uma proposta com situação igual a "Concluída" ou "Falha no envio"
	Quando clico em "Ver detalhes" de uma proposta com um dos seguintes status
	| STATUS         |
	| Concluída      |
	| Falha no envio |
	Então o sistema me redireciona para a tela "Proposta" com o formulário da empresa aberto no modo somente leitura

@automatizar
Cenario: Visualizar formulário do sócio que preencheu a proposta
	Quando clico em "Ver detalhes" de uma proposta com um dos seguintes status
	| STATUS         |
	| Concluída      |
	| Falha no envio |
	Então o sistema me redireciona para a tela "Proposta" com o formulário da empresa aberto no modo somente leitura
	Quando clico em "Sócio 1"
	Então o sistema apresenta o formulário do sócio que preencheu a proposta no modo somente leitura

@automatizar
Cenario: Visualizar formulário dos demais sócios da proposta
	Quando clico em "Ver detalhes" de uma proposta com um dos seguintes status
	| STATUS         |
	| Concluída      |
	| Falha no envio |
	E a proposta possui mais de um sócio
	Então o sistema me redireciona para a tela "Proposta" com o formulário da empresa aberto no modo somente leitura
	Quando clico em "Sócio X", ou seja, do sócio 2 em diante
	Então o sistema apresenta o formulário do sócio no modo somente leitura

@automatizar
Cenario: Sair do modo somente leitura da proposta
	Quando clico em "Ver detalhes" de uma proposta com um dos seguintes status
	| STATUS         |
	| Concluída      |
	| Falha no envio |
	E a proposta possui mais de um sócio
	Então o sistema me redireciona para a tela "Proposta" com o formulário da empresa aberto no modo somente leitura
	Quando clico em "Voltar"
	Então o sistema me retorna para a tela de listagem das propostas


######################################## PROTÓTIPOS #########################################
#
#	Tela "Proposta" no modo leitura: https://marvelapp.com/7e20jdb/screen/62590710
#
#############################################################################################									
