﻿#language: pt-br

Funcionalidade: Onboarding PJ - Relatório do percentual das respostas
	Como um usuário do Cactus com um dos seguintes perfis de acesso: Administrador ou Coordenador BPO
	Quero poder extrair o relatório do percentual das respostas
	Para validar a efetividade das perguntas do formulário do BPO de PJ

@automatizar
Esquema do Cenario: Exportar "Relatório do percentual das respostas"
	Dado que estou logado com o <perfil de acesso>	
	E existem registros retornados na lista de propostas de acordo com os filtros aplicados
	Quando clico em "exportar para Excel"
	E clico em "Relatório do percentual das respostas"
	Então o sistema efetua o download do "Relatório do percentual das respostas" no formato ".xlsx", contendo as perguntas do formulário e o percentual das respostas de acordo com os filtros aplicados
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |

######################################## PROTÓTIPOS #########################################
#
#	Relatório do onboarding: https://marvelapp.com/7e20jdb/screen/62534588
#
############################### ESPECIFICAÇÃO DE CAMPOS ####################################									
#
#	COLUNAS DO RELATÓRIO DO PERCENTUAL DAS RESPOSTAS:
#		
#		| COLUNA                       | TIPO    | VALORES                                                  | MASCARA |
#		| Pergunta                     | Inteiro | Lista das perguntas dos formulários                      | N/A     |
#		| Percentual de resposta "Sim" | String  | Percentual = (Amostra (valor menor) / Valor total) * 100 | 100,00% |
#		| Percentual de resposta "Não" | String  | Percentual = (Amostra (valor menor) / Valor total) * 100 | 100,00% |
#		| Percentual de resposta "N/A" | String  | Percentual = (Amostra (valor menor) / Valor total) * 100 | 100,00% |
#
#	PERGUNTAS:
#
#		SEÇÃO "FORMULÁRIO DA EMPRESA":
#
#			| PERGUNTA                                                                                              |
#			| 1. A empresa está ativa no bureau ou na Receita Federal?                                              |
#			| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                 |
#			| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                 |
#			| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?       |
#			| 5. O domínio do e-mail pertence à empresa da proposta?                                                |
#			| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                   |
#			| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?         |
#			| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?         |
#			| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal? |
#
#		SEÇÃO "FORMULÁRIO DO SÓCIO QUE PREENCHEU A PROPOSTA":
#
#			| PERGUNTA                                                                                                           |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |
#			| 3. A foto do documento é válida?                                                                                   |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |
#			| 14. As perguntas do quiz foram exibidas?                                                                           |
#			| 15. O cliente acertou todas as perguntas do quiz?                                                                  |
#			| 16. O cliente errou mais de uma pergunta do quiz?                                                                  |
#			| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |
#
#		SEÇÃO "FORMULÁRIO DOS DEMAIS SÓCIOS DA PROPOSTA":
#
#			| PERGUNTA                                                                                                           |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |
#			| 3. A foto do documento é válida?                                                                                   |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |
#			| 14. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |
#		
#	PERCENTUAL DE RESPOSTA "SIM":
#
#		O sistema deve considerar apenas as propostas com situação igual a “Concluída” e “Falha no envio”, que são as propostas cujo formulário já foi respondido. Sendo que, se houver filtro aplicado na tela no momento da exportação, o sistema deve levar em consideração apenas os dados retornados de acordo com o filtro aplicado.
#
#		O sistema deve recuperar a quantidade de propostas que a pergunta X foi respondida com a opção de resposta “Sim”, deve considerar a quantidade total de propostas e deve efetuar o seguinte cálculo: 
#
#			Dividir o tamanho da amostra (valor menor) pelo valor total.
#
#		Exemplo: Considerando que em 30 propostas, a pergunta 1 foi respondida com “Sim” em 15 propostas, a amostra é o valor de 15 e o valor total é de 30. Então basta dividir 15 por 30. O resultado será 0,5, multiplicando por 100 dá 50%. 
#		
#			Total de propostas (valor total) = 30
#			Quantidade de propostas cuja pergunta 1 foi respondida com “Sim” (amostra) = 15
#
#			Cálculo:
#
#				Percentual = (Amostra (valor menor) / Valor total) * 100
#				Percentual = (15 / 30) * 100
#				Percentual = 0,5 * 100
#				Percentual = 50,00%
#
#	PERCENTUAL DE RESPOSTA "NÃO":
#
#		O sistema deve considerar apenas as propostas com situação igual a “Concluída” e “Falha no envio”, que são as propostas cujo formulário já foi respondido. Sendo que, se houver filtro aplicado na tela no momento da exportação, o sistema deve levar em consideração apenas os dados retornados de acordo com o filtro aplicado.
#
#		O sistema deve recuperar a quantidade de propostas que a pergunta X foi respondida com a opção de resposta “Não”, deve considerar a quantidade total de propostas e deve efetuar o seguinte cálculo: 
#
#			Dividir o tamanho da amostra (valor menor) pelo valor total.
#
#		Exemplo: Considerando que em 30 propostas, a pergunta 1 foi respondida com “Não” em 15 propostas, a amostra é o valor de 15 e o valor total é de 30. Então basta dividir 15 por 30. O resultado será 0,5, multiplicando por 100 dá 50%. 
#
#
#			Total de propostas (valor total) = 30
#			Quantidade de propostas cuja pergunta 1 foi respondida com “Não” (amostra) = 15
#
#			Cálculo:
#
#				Percentual = (Amostra (valor menor) / Valor total) * 100
#				Percentual = (15 / 30) * 100
#				Percentual = 0,5 * 100
#				Percentual = 50,00%
#
#	PERCENTUAL DE RESPOSTA "N/A":
#
#		O sistema deve considerar apenas as propostas com situação igual a “Concluída” e “Falha no envio”, que são as propostas cujo formulário já foi respondido. Sendo que, se houver filtro aplicado na tela no momento da exportação, o sistema deve levar em consideração apenas os dados retornados de acordo com o filtro aplicado.
#
#		O sistema deve recuperar a quantidade de propostas que a pergunta X foi respondida com a opção de resposta “N/A”, deve considerar a quantidade total de propostas e deve efetuar o seguinte cálculo: 
#
#			Dividir o tamanho da amostra (valor menor) pelo valor total.
#
#		Exemplo: Considerando que em 30 propostas, a pergunta 1 foi respondida com “N/A” em 15 propostas, a amostra é o valor de 15 e o valor total é de 30. Então basta dividir 15 por 30. O resultado será 0,5, multiplicando por 100 dá 50%. 
#
#			Total de propostas (valor total) = 30
#			Quantidade de propostas cuja pergunta 1 foi respondida com “N/A” (amostra) = 15
#
#			Cálculo:
#
#				Percentual = (Amostra (valor menor) / Valor total) * 100
#				Percentual = (15 / 30) * 100
#				Percentual = 0,5 * 100
#				Percentual = 50,00%
#
#######################################################################################		
