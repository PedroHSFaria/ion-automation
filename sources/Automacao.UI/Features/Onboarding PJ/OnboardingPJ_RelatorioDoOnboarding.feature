﻿#language: pt-br

Funcionalidade: Onboarding PJ - Relatório do onboarding
	Como um usuário do Cactus com um dos seguintes perfis de acesso: Administrador ou Coordenador BPO
	Quero poder extrair o relatório do onboarding
	Para visualizar as propostas de PJ de acordo com os filtros aplicados

@automatizar
Esquema do Cenario: Exportar "Relatório do onboarding"
	Dado que estou logado com o <perfil de acesso>	
	E clico em "Onboarding"
	E existem registros retornados na lista de propostas
	Quando clico em "exportar para Excel"
	E clico em "Relatório do onboarding"
	Então o sistema efetua o download do "Relatório do onboarding" no formato ".xlsx", contendo as propostas de PJ de acordo com os filtros aplicados 
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |

######################################## PROTÓTIPOS #########################################
#
#	Relatório do onboarding: https://marvelapp.com/7e20jdb/screen/62533424
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################									
#
#	COLUNAS DO RELATÓRIO DO ONBOARDING:
#		
#		| COLUNA                      | TIPO     | VALORES                                                                                                                                                                           | MASCARA                |
#		| Seed                        | Inteiro  | ID da solicitação                                                                                                                                                                 | N/A                    |
#		| Razão social                | String   | Nome da empresa                                                                                                                                                                   | N/A                    |
#		| CNPJ                        | String   | CNPJ da empresa                                                                                                                                                                   | 99.999.999/9999-99     |
#		| E-mail                      | String   | E-mail da empresa                                                                                                                                                                 | teste@teste.com        |
#		| Data/Hora de abertura       | DateTime | Data/Hora cuja proposta foi gerada                                                                                                                                                | DD/MM/AAAA às HH:MM:SS |
#		| Situação                    | String   | "Aguardando análise", "Em análise", "Pendente", Concluída e "Falha no envio"                                                                                                      | N/A                    |
#		| Fraude                      | String   | "Sim" e "Não"                                                                                                                                                                     | N/A                    |
#		| Natureza jurídica permitida | String   | "Sim" ou "Não"                                                                                                                                                                    | N/A                    |
#		| Data/Hora início da análise | DateTime | Data/Hora cuja situação da proposta foi alterada para "Em análise"                                                                                                                | DD/MM/AAAA às HH:MM:SS |
#		| Tempo médio de espera       | DateTime | Data/Hora início da análise - Data/Hora de abertura                                                                                                                               | 99 dias e HH:MM:SS     |
#		| Data/Hora de conclusão      | DateTime | Data/hora cuja situação da proposta foi alterada para "Concluída"                                                                                                                 | DD/MM/AAAA às HH:MM:SS |
#		| Tempo médio de análise      | DateTime | Data/Hora de conclusão - Data/Hora início da análise                                                                                                                              | 99 dias e HH:MM:SS     |
#		| Responsável                 | String   | Nome do responsável atribuído a proposta                                                                                                                                          | N/A                    |
#		| Falha no envio              | String   | "Sim" ou "Não"                                                                                                                                                                    | N/A                    |
#		| Qtde. de formulários        | Inteiro  | Quantidade de formulários contidos na proposta (Ex.: Se a empresa tem 3 sócios, a quantidade de formulários será 4, sendo 1 da empresa e 1 para cada sócio, que nesse caso são 3. | N/A                    |
#
#	ORDENAÇÃO DOS DADOS DO RELATÓRIO DO ONBOARDING:
#
#		Seguir a mesma ordenação da tela de listagem das propostas do onboarding de PJ: 
#
#			| CRITÉRIO                                                            | ORDENAÇÃO                                                                                                                   |  
#			| Contrato e foto                                                     | "Proposta com contrato e foto", "Proposta apenas com contrato", "Proposta apenas com foto" e "Proposta sem contrato e foto" |
#			| Consulta dos dados dos sócios já efetuada no bureau da BigData Corp | "Sim" e "Não"                                                                                                               |
#			| Situação                                                            | "Aguardando análise", "Em análise", "Pendente", "Falha no envio" e "Concluída"                                              |
#			| Data/Hora de abertura                                               | Ordem crescente                                                                                                             |
#
################################ REGRAS DE NEGÓCIO ###################################	
#
#	| ID    | REGRA DE NEGÓCIO                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
#	| RN001 | O sistema deve apresentar o valor "Sim" na coluna "Fraude" apenas quando a proposta for concluída através do botão "Fraude". E deve apresentar o valor "Não" quando a proposta for concluída de outra forma.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
#	| RN002 | O sistema deve apresentar o valor "Sim" na coluna "Natureza jurídica permitida" apenas quando a proposta for concluída através do botão "Natureza jurídica não permitida". E deve apresentar o valor "Não" quando a proposta for concluída de outra forma.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
#	| RN003 | O sistema deve apresentar o valor "Sim" na coluna "Falha no envio" apenas quando ocorreu um erro ao enviar a proposta para o BPM e o sistema enviou essa proposta automaticamente em uma das três tentativas que ele faz em caso de falha no envio. Pois no caso de falha ao enviar a proposta para o BPM, o sistema deve tentar mais três vezes enviar a proposta, sendo de forma automática e invisível para o usuário. Sendo assim, se em uma das três tentativas a proposta for enviada com sucesso, ou seja, for concluída, o sistema deverá alterar a situação para “Concluída” e deverá gravar a data/hora que a mesma foi concluída no campo <Data/Hora de conclusão>. E essa coluna deve ser preenchida com o valor “Não”, quando as propostas forem enviadas corretamente para o BPM assim que o usuário finalizar a mesma. E também deverá ser preenchida com o valor “Não” quando ocorrer erro, mas o sistema não conseguir enviar a proposta de forma automática em uma das três tentativas que ele faz em caso de falha e o usuário ter que analisar e concluir manualmente a proposta novamente. |
#         
#######################################################################################		
