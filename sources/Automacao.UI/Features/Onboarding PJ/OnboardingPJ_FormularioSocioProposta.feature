﻿#language: pt-br
#Protótipos: https://marvelapp.com/7e20jdb/screen/62175366

Funcionalidade: Onboarding PJ - Formulário do sócio que preencheu a proposta
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero acessar o formulário do sócio que preencheu a proposta
	Para que possa respondê-lo

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	E clico em "Analisar" de uma proposta
	E respondo todas as perguntas obrigatórias do formulário da empresa
	E clico em "Próximo"
	Então o sistema habilita o formulário do sócio que preencheu a proposta
	E me redireciona para o formulário do sócio que preencheu a proposta

@automatizar
Cenario: Responder as perguntas do formulário do sócio que preencheu a proposta
	Quando respondo as perguntas do formulário do sócio que preencheu a proposta
	| PERGUNTA                                                                                                           | RESPOSTA        |
	| 1. Os dados da proposta conferem com o documento?                                                                  | Resposta válida |
	| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           | Resposta válida |
	| 3. A foto do documento é válida?                                                                                   | Resposta válida |
	| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          | Resposta válida |
	| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        | Resposta válida |
	| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               | Resposta válida |
	| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  | Resposta válida |
	| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       | Resposta válida |
	| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 | Resposta válida |
	| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  | Resposta válida |
	| 11. A consulta do bureau retornou os dados do cliente?                                                             | Resposta válida |
	| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? | Resposta válida |
	| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     | Resposta válida |
	| 14. As perguntas do quiz foram exibidas?                                                                           | Resposta válida |
	| 15. O cliente acertou todas as perguntas do quiz?                                                                  | Resposta válida |
	| 16. O cliente errou mais de uma pergunta do quiz?                                                                  | Resposta válida |
	| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   | Resposta válida |
	| Obervação                                                                                                          | Resposta válida |
	Então o sistema apresenta as perguntas respondidas de acordo com as opções marcadas

@automatizar
Cenario: Acessar site da Receita Federal
	Quando clico em "Valide o documento na Receita Federal" na pergunta 12
	Então o sistema abre em uma nova guia o link https://servicos.receita.fazenda.gov.br/Servicos/CPF/ConsultaSituacao/ConsultaPublica.asp para que valide o CPF na Receita Federal

@automatizar
Cenario: Visualizar ajuda
	Quando passar o cursor do mouse sobre o ícone de "Ajuda" de uma pergunta
	Então o sistema deve apresentar o texto de ajuda da pergunta

@automatizar
Cenario: Maximizar todas as abas
	Quando todas as abas estão minimizadas
	E clico em "Maximizar todas as abas"
	Então o sistema expande todas as abas de todos os formulários

@automatizar	
Cenario: Minimizar todas as abas
	Quando todas as abas estão maximizadas
	E clico em "Minimizar todas as abas"
	Então o sistema minimiza todas as abas de todos os formulários 

@automatizar
Cenario: Maximizar uma aba
	Quando uma aba está minimizada
	E clico no "Maximizar" dessa aba
	Então o sistema expande a respectiva aba

@automatizar	
Cenario: Minimizar uma aba
	Quando uma aba está maximizada
	E clico no "Minimizar" dessa aba
	Então o sistema minimiza a respectiva aba

@automatizar
Cenario: Visualizar próximo formulário
	Quando respondo todas as perguntas obrigatórias do formulário do sócio que preencheu a proposta
	E clico em "Próximo"
	Então o sistema habilita o formulário do próximo sócio 
	E me redireciona para o formulário do próximo sócio

@automatizar
Cenario: Visualizar próximo formulário - Perguntas obrigatórias não preenchidas
	Quando não respondo todas as perguntas obrigatórias do formulário do sócio que preencheu a proposta
	E clico em "Próximo"
	Então o sistema apresenta a mensagem "É preciso responder essa pergunta para concluir a proposta." em todas as perguntas obrigatórias não respondidas
	E me redireciona para a primeira pergunta obrigatória não respondida

@automatizar
Cenario: Visualizar próximo formulário - Dado obrigatório não checado
	Quando não checo dados obrigátórios do formulário do sócio que preencheu a proposta
	E clico em "Próximo"
	Então o sistema apresenta a mensagem "É preciso indicar se esse dado confere ou não com o documento, clicando sobre o mesmo." em todos os dados obrigatórios não checados
	E me redireciona para o primeiro dado não checado

@automatizar
Cenario: Visualizar formulário anterior
	Quando clico em "Anterior"
	Então o sistema me redireciona para o formulário anterior
	   	   

######################################## PROTÓTIPO #########################################
#
#	Tela "Proposta" - Formulário do sócio que preencheu a proposta: https://marvelapp.com/7e20jdb/screen/62378448
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
#	FORMULÁRIO DA EMPRESA:
#
#		| PERGUNTAS                                                                                                          | TIPO        | OPÇÕES DE RESPOSTAS  | OBRIGATÓRIO                                   | PLACEHOLDER            | TAMANHO MÁXIMO |
#		| 1. Os dados da proposta conferem com o documento?                                                                  | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 3. A foto do documento é válida?                                                                                   | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 11. A consulta do bureau retornou os dados do cliente?                                                             | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 14. As perguntas do quiz foram exibidas?                                                                           | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 15. O cliente acertou todas as perguntas do quiz?                                                                  | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 16. O cliente errou mais de uma pergunta do quiz?                                                                  | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   | Radiobutton | "Sim", "Não" e "N/A" | Sim                                           | N/A                    | N/A            |
#		| Observação                                                                                                         | String      | N/A                  | Apenas quando o botão "Pendente" for acionado | Escreva sua observação | 500 caracteres |
#			
#	MENSAGENS ABAIXO DAS PERGUNTAS:
#
#			Pergunta 1:
#				Clique uma vez sobre o dado da proposta, para indicar com o ícone <ícone de duas barras verdes> que ele confere com o documento. E clique duas vezes para indicar com o ícone <dois losangos vermelhos> que não confere com o documento.
#
#			Pergunta 2:
#				Clique uma vez sobre o dado da proposta, para indicar com o ícone <ícone de duas barras verdes> que ele confere com o documento. E clique duas vezes para indicar com o ícone <dois losangos vermelhos> que não confere com o documento.
#
#			Pergunta 3:
#				Verifique a foto do documento avaliando três critérios, são eles: a foto é original, a foto é legível e o tipo de documento é válido.
#
#			Pergunta 11:
#				Atenção, os dados abaixo são os retornados na consulta do bureau e não são clicáveis.
#
#			Pergunta 13:
#				Clique uma vez sobre o dado do bureau, para indicar com o ícone <ícone de duas barras verdes> que ele confere com o documento. E clique duas vezes para indicar com o ícone <dois losangos vermelhos> que não confere com o documento.
#
#
#	AJUDA (ÍCONES DE INTERROGAÇÃO AO LADO DAS PERGUNTAS):
#
#			Pergunta 1: Os dados da proposta conferem com o documento? 
#				
#				Validar se os dados inseridos no onboarding estão de acordo com o documento enviado. Abaixo, dados a serem validados:
#					•	Nome completo
#					•	Data de nascimento
#					•	Data de expedição do documento
#					•	Nº do documento – CNH/RG/documento profissional
#					•	Nome da mãe
#					** Quando nome do sócio ou da mãe diferir apenas em um “sobrenome” considerar como “N/A”. 
#
#			Pergunta 2: O CPF  que consta na proposta é o mesmo do documento?        
#
#				Verificar se o documento enviado contém CPF, e, em caso positivo, validar se o número que consta no documento é o mesmo encaminhado no onboarding.
#
#				Obs: Caso o documento não possua o CPF no campo destinado para tal, deverá ser marcada a opção “N/A”.
#
#			Pergunta 3: A foto do documento é válida?   
#
#				Verificar se o documento é original e dentro dos parâmetros de documentos aceitos pelo BS2 - CNH/RG/Documento Profissional.
#
#				Sendo que, para que o documento seja considerado original, ele não poderá ser: xerox, passaporte, carteira de trabalho, foto da tela de celular e/ou computador, fotos ilegíveis (tremidas, escuras, cortadas) e documentos antigos com foto de criança, onde não é possível identificar que é a mesma pessoa no documento e selfie encaminhados.
#
#				** Documentos com mais de 10 anos de emissão podem ser aceitos, desde que seja possível reconhecer o sócio de acordo com a vídeo selfie. 
#			
#			Pergunta 4: A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau? 
#
#				Validar se o Estado/UF do endereço informado na proposta retornou compatível com algum Estado/UF vinculado ao cliente no Bureau.
#
#			Pergunta 5: O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau? 
#
#				Validar se o DDD do telefone informado na proposta retornou compatível com algum DDD vinculado ao cliente.
#
#			Pergunta 6: A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?  
#
#				Validar se a UF da proposta é compatível com a UF de algum DDD retornado pelo Bureau. 
#
#				Ex: Endereço da proposta é de MG e no Bureau retornou algum telefone com DDD 31. Nesse caso a resposta será “Sim”.
#
#			Pergunta 7: A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?  
#
#				Validar se a cidade informada na proposta retornou compatível com alguma cidade vinculada no Bureau.
#
#			Pergunta 8: O número do telefone da proposta confere com algum retornado pelo bureau?  
#
#				Validar se o nº do telefone informado na proposta é igual a algum telefone retornado pelo Bureau.
#
#			Pergunta 9: O endereço da proposta confere com algum retornado pelo bureau?
#
#				Validar se o endereço retornado pelo Bureau é igual a algum endereço inserido na proposta (Logradouro, nº, complemento, bairro, cidade e CEP).
#
#			Pergunta 10: O e-mail da proposta confere com algum retornado pelo bureau?
#
#				Validar se o e-mail informado pelo cliente é igual a algum e-mail retornado pelo Bureau. 
#				Se atentar ao domínio correto do e-mail (outlook, gmail e etc). 
#
#			Pergunta 11: A consulta do bureau retornou os dados do cliente?
#
#				Validar se o Bureau retornou no mínimo, os seguintes dados do cliente: Nome, CPF, Data de nascimento e Status do CPF na Receita Federal. Se faltar alguma dessas informações, a resposta deverá ser ”Não”.
#
#			Pergunta 12: Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? 
#
#				Caso não seja possível realizar a validação no site da Receita, a resposta para a pergunta deverá ser “N/A”. Se a consulta foi realizada e o CPF estiver com status diferente de “regular” e “pendente de regularização” a resposta deverá ser “Não”.
#
#			Pergunta 13: Os dados abaixo conferem com os dados do documento (foto)? 
#
#				Validar se os dados retornados pelo Bureau são compatíveis com os dados inseridos no cadastro. Abaixo, dados a serem validados:
#					•	Nome completo
#					•	Data de nascimento
#					•	Nome da mãe
#
#			Pergunta 14: As perguntas do quiz foram exibidas?     
#
#				Verificar se todas as cinco perguntas do quiz foram exibidas ao cliente. Caso tenha ocorrido falha na exibição, a resposta deverá ser “Não”.
#
#			Pergunta 15: O cliente acertou todas as perguntas do quiz? 
#
#				Validar se o cliente respondeu todas as perguntas de forma correta. O “N/A” só será cabível caso o quiz não tenha sido exibido para o cliente.
#
#			Pergunta 16: O cliente errou mais de uma pergunta do quiz?        
#
#				Validar se o cliente errou mais de uma pergunta do quiz, sendo que caso tenha errado apenas uma ou nenhuma, a resposta correta deverá ser “Não”. O “N/A” só será cabível caso o quiz não tenha sido exibido para o cliente.
#
#			Pergunta 17: De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?
#
#				Dar resposta de acordo com seu parecer final referente à proposta como um todo. Se há algum indício de fraude seja no comportamento do cliente, nos dados da proposta ou no documento apresentado.
#
#	COMPORTAMENTOS DAS PERGUNTAS:
#
#			Pergunta 1: Os dados da proposta conferem com o documento?       
#			
#				Campos: 
#					- Nome: O sistema deve recuperar do BPM o nome do sócio da empresa que preencheu a proposta. 
#					- Data de nascimento: O sistema deve recuperar do BPM a data de nascimento do sócio da empresa que preencheu a proposta.
#					- Mãe: O sisteam deve recuperar do BPM o nome da mãe do sócio da empresa que preencheu a proposta.
#					- Nº do documento: O sistema deve recuperar do BPM o número do documento (RG ou CNH) do sócio da empresa que preencheu a proposta.
#					- Data de expedição: O sistema deve recuperar do BPM a data de expedição do documento (RG ou CNH) do sócio da empresa que preencheu a proposta.
#
#				Comportamento: 
#					- Ao clicar uma vez sobre um dos campos listados acima, o sistema deve checar o campo como validado e deve destacá-lo em verde. Ao clicar duas vezes sobre o campo não destacado (cor preta) ou clicar uma vez quando ele já estiver destacado em verde, o sistema deve checar o campo como não validado e deve destacá-lo em vermelho.
#					- O sistema deve responder automaticamente essa pergunta com a opção de resposta “Sim” quando todos os campos forem validados, ou seja, quando todos os campos tiverem destacados em verde. 
#					- Se um dos campos estiver vazio, o sistema não deve permitir que o usuário marque-o como validado ou não. E o sistema deve responder automaticamente essa pergunta com a opção de resposta “N/A” quando todos os campos estiverem vazios OU quando houver ao menos um campo vazio e nenhum dos campos estiverem destacados em vermelho, pois o sistema deve responder automaticamente essa pergunta com a opção de resposta “Não” quando pelo menos um campo não for validado, ou seja, quando pelo menos um dos campos tiver destacado em vermelho.
#	
#			Pergunta 2: O CPF  que consta na proposta é o mesmo do documento?   
#
#				Campos: 
#					- CPF: O sistema deve recuperar do BPM o CPF do sócio da empresa que preencheu a proposta.
#
#				Comportamento: 
#					- Ao clicar uma vez sobre o campo <CPF>, o sistema deve checar o campo como validado e deve destacá-lo em verde. Ao clicar duas vezes sobre o campo não destacado (cor preta) ou clicar uma vez quando ele já estiver destacado em verde, o sistema deve checar o campo como não validado e deve destacá-lo em vermelho.
#					- O sistema deve responder automaticamente essa pergunta com a opção de resposta “Sim” quando o campo <CPF> for validado, ou seja, quando o campo <CPF> tiver destacado em verde. 
#					- O sistema deve responder automaticamente essa pergunta com a opção de resposta “Não” quando o campo <CPF> não for validado, ou seja, quando o campo <CPF> tiver destacado em vermelho. 
#					- Se o usuário responder essa pergunta manualmente, ao selecionar a opção de resposta “Sim”, o sistema deve checar automaticamente o campo <CPF> como validado e deve destacá-lo em verde. Ao selecionar a opção de resposta “Não”, o sistema deve checar automaticamente o campo <CPF> como não validado e deve destacá-lo em vermelho.
#				
#			Pergunta 3: A foto do documento é válida?
#
#				N/A
#
#			Pergunta 4: A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?     
#
#				Campos: 
#					- UF do endereço da proposta: O sistema deve apresentar o endereço da empresa informado no onboarding, retornada pelo BPM.
#					- UFs dos endereços retornados do bureau: O sistema deve apresentar a lista das UFs dos endereços do sócio, retornadas pelo bureau, essas UFs devem ser consultadas no bureau da BigData Corp. Essa lista pode vir vazia, nesse caso a lista deverá ser apresentada vazia.
#						- O sistema deve ter um serviço que efetuará a consulta dos dados dos sócios da empresa, de 10 em 10 segundos, nos datasets "Endereços", "Telefones" e "E-mails" do bureau da BigData Corp, para compor as perguntas dos formulários dos sócios. Sendo que, antes de efetuar a consulta diretamente no bureau da BigData, o sistema deve verificar se já existe uma consulta nos últimos 3 meses com os dados necessários (Datasets: "Endereços", "Telefones" e "E-mails") para o respectivo sócio, na tabela "BUREAU" do banco "BBSFRAUDE". Se existir, o sistema não deve efetuar uma nova consulta na BigData, deve apenas recuperar a consulta existente na base "BBSFRAUDE". Se não existir, o sistema deve efetuar a consulta no bureau da BigDataCorp e deve gravar o resultado dessa consulta na tabela "BUREAU" e deve gravar o log na tabela "BUREAU_LOG" do banco BBSFRAUDE.                                                                                                                                                                                                                                                                                                                                                                                                                     |
#						- Se mais de um endereço retornado for de uma mesma UF, o sistema só deve exibir a respectiva UF uma única vez na lista. 
#
#				Comportamento: 
#					- Se a UF da proposta for igual a uma das UFs retornadas pelo bureau, o sistema deve destacar ambas em verde e as demais UFs retornadas pelo bureau devem ser destacadas em vermelho. 
#					- Caso a UF da proposta não seja igual a uma das UFs retornadas pelo bureau, o sistema deve destacar todas as UFs em vermelho, tanto a da proposta quanto as retornadas pelo bureau. 
#					- Se nenhum endereço for retornado pelo bureau, o sistema não deve destacar as UFs.
#
#			Pergunta 5: O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?   
#			
#				Campos: 
#					- DDD da proposta: O sistema deve apresentar o DDD do telefone informado no onboarding, retornado pelo BPM.
#					- DDDs retornados do bureau: O sistema deve apresentar a lista dos DDDs dos telefones do sócio, retornados pelo bureau, esses DDDs devem ser consultados no bureau da BigData Corp. Essa lista pode vir vazia, nesse caso a lista deverá ser apresentada vazia. 
#						- O sistema deve ter um serviço que efetuará a consulta dos dados dos sócios da empresa, de 10 em 10 segundos, nos datasets "Endereços", "Telefones" e "E-mails" do bureau da BigData Corp, para compor as perguntas dos formulários dos sócios. Sendo que, antes de efetuar a consulta diretamente no bureau da BigData, o sistema deve verificar se já existe uma consulta nos últimos 3 meses com os dados necessários (Datasets: "Endereços", "Telefones" e "E-mails") para o respectivo sócio, na tabela "BUREAU" do banco "BBSFRAUDE". Se existir, o sistema não deve efetuar uma nova consulta na BigData, deve apenas recuperar a consulta existente na base "BBSFRAUDE". Se não existir, o sistema deve efetuar a consulta no bureau da BigDataCorp e deve gravar o resultado dessa consulta na tabela "BUREAU" e deve gravar o log na tabela "BUREAU_LOG" do banco BBSFRAUDE.                                                                                                                                                                                                                                                                                                                                                                                                                     |
#						- Se um mesmo DDD for retornado mais de uma vez, o sistema deve exibi-lo uma única vez na lista.
#
#				Comportamento: 
#					- Se o DDD do telefone da proposta for igual a um dos DDDs dos telefones retornados pelo bureau, o sistema deve destacar ambos em verde e os demais DDDs dos telefones retornadas pelo bureau devem ser destacados em vermelho. 
#					- Caso o DDD do telefone da proposta não seja igual a um dos DDDs dos telefones retornados pelo bureau, o sistema deve destacar todos os DDDs em vermelho, tanto o da proposta quanto os retornados pelo bureau. 
#					- Se nenhum DDD for retornado pelo bureau, o sistema não deve destacar os DDDs.
#
#			Pergunta 6: A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?  
#
#				Campos:
#					UF do endereço da proposta: O sistema deve apresentar a UF do endereço informado no onboarding, retornado pelo BPM.
#					UFs dos DDDs retornados do bureau: O sistema deve apresentar a lista dos DDDs dos telefones do sócio, retornados pelo bureau, esses DDDs devem ser consultados no bureau da BigData Corp. Essa lista pode vir vazia, nesse caso a lista deverá ser apresentada vazia. 
#						- Ao recuperar os DDD dos telefones, o sistema deve verificar qual é a UF de cada DDD e deve apresentá-las na tela. Para isso, deve-se utilizar a tabela “tra.UF_DDD”, que já possui as UF de acordo com o DDD.
#
#							use bbsfraude
#							select * from tra.UF_DDD
#
#						- Se mais de um DDD retornado for de uma mesma UF, o sistema só deve exibir a respectiva UF uma única vez na lista. 
#						- O sistema deve ter um serviço que efetuará a consulta dos dados dos sócios da empresa, de 10 em 10 segundos, nos datasets "Endereços", "Telefones" e "E-mails" do bureau da BigData Corp, para compor as perguntas dos formulários dos sócios. Sendo que, antes de efetuar a consulta diretamente no bureau da BigData, o sistema deve verificar se já existe uma consulta nos últimos 3 meses com os dados necessários (Datasets: "Endereços", "Telefones" e "E-mails") para o respectivo sócio, na tabela "BUREAU" do banco "BBSFRAUDE". Se existir, o sistema não deve efetuar uma nova consulta na BigData, deve apenas recuperar a consulta existente na base "BBSFRAUDE". Se não existir, o sistema deve efetuar a consulta no bureau da BigDataCorp e deve gravar o resultado dessa consulta na tabela "BUREAU" e deve gravar o log na tabela "BUREAU_LOG" do banco BBSFRAUDE.                                                                                                                                                                                                                                                                                                                                                                                                                     |
#						- Se um mesmo DDD for retornado mais de uma vez, o sistema deve exibi-lo uma única vez na lista.
#
#				Comportamento: 
#					- Se a UF da proposta for igual a uma das UFs dos DDDs retornados pelo bureau, o sistema deve destacar ambas em verde e as demais UFs dos DDDs retornados pelo bureau devem ser destacadas em vermelho. 
#					- Caso a UF da proposta não seja igual a uma das UFs dos DDDs retornados pelo bureau, o sistema deve destacar todas as UFs em vermelho, tanto a da proposta quanto as dos DDDs retornados pelo bureau. 
#					- Se nenhum DDD for retornado pelo bureau, o sistema não deve destacar as UFs.
#
#			Pergunta 7: A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?  
#
#				Campos: 
#					- Cidade do endereço da proposta: O sistema deve apresentar a cidade do endereço informado no onboarding, retornado pelo BPM.
#					- Cidades dos endereços retornados do bureau: O sistema deve apresentar a lista das cidades dos endereços do sócio, retornados pelo bureau, essas cidades devem ser consultadas no bureau da BigData Corp. Essa lista pode vir vazia, nesse caso a lista deverá ser apresentada vazia. 
#						- O sistema deve ter um serviço que efetuará a consulta dos dados dos sócios da empresa, de 10 em 10 segundos, nos datasets "Endereços", "Telefones" e "E-mails" do bureau da BigData Corp, para compor as perguntas dos formulários dos sócios. Sendo que, antes de efetuar a consulta diretamente no bureau da BigData, o sistema deve verificar se já existe uma consulta nos últimos 3 meses com os dados necessários (Datasets: "Endereços", "Telefones" e "E-mails") para o respectivo sócio, na tabela "BUREAU" do banco "BBSFRAUDE". Se existir, o sistema não deve efetuar uma nova consulta na BigData, deve apenas recuperar a consulta existente na base "BBSFRAUDE". Se não existir, o sistema deve efetuar a consulta no bureau da BigDataCorp e deve gravar o resultado dessa consulta na tabela "BUREAU" e deve gravar o log na tabela "BUREAU_LOG" do banco BBSFRAUDE.                                                                                                                                                                                                                                                                                                                                                                                                                     |
#						- Se mais de um endereço retornado for de uma mesma cidade, o sistema só deve exibir a respectiva cidade uma única vez na lista. 
#
#				Comportamento: 
#					- Se a cidade da proposta for igual a uma das cidades retornadas pelo bureau, o sistema deve destacar ambas em verde e as demais cidades retornadas pelo bureau devem ser destacadas em vermelho. 
#					- Caso a cidade da proposta não seja igual a uma das cidades retornadas pelo bureau, o sistema deve destacar todas as cidades em vermelho, tanto a da proposta quanto as retornadas pelo bureau. 
#					- Se nenhum endereço for retornado pelo bureau, o sistema não deve destacar as cidades.
#
#			Pergunta 8: O número do telefone da proposta confere com algum retornado pelo bureau? 
#			
#				Campos: 
#					- Telefone da proposta: O sistema deve apresentar o telefone informado no onboarding, retornado pelo BPM.
#					- Telefones retornados do bureau: O sistema deve apresentar a lista dos telefones do sócio, retornados pelo bureau, esses telefones devem ser consultados no bureau da BigData Corp. Essa lista pode vir vazia, nesse caso a lista deverá ser apresentada vazia. 
#						- O sistema deve ter um serviço que efetuará a consulta dos dados dos sócios da empresa, de 10 em 10 segundos, nos datasets "Endereços", "Telefones" e "E-mails" do bureau da BigData Corp, para compor as perguntas dos formulários dos sócios. Sendo que, antes de efetuar a consulta diretamente no bureau da BigData, o sistema deve verificar se já existe uma consulta nos últimos 3 meses com os dados necessários (Datasets: "Endereços", "Telefones" e "E-mails") para o respectivo sócio, na tabela "BUREAU" do banco "BBSFRAUDE". Se existir, o sistema não deve efetuar uma nova consulta na BigData, deve apenas recuperar a consulta existente na base "BBSFRAUDE". Se não existir, o sistema deve efetuar a consulta no bureau da BigDataCorp e deve gravar o resultado dessa consulta na tabela "BUREAU" e deve gravar o log na tabela "BUREAU_LOG" do banco BBSFRAUDE.                                                                                                                                                                                                                                                                                                                                                                                                                     |
#
#				Comportamento: 
#					- Se o telefone da proposta for igual a um dos telefones retornados pelo bureau (DDD + número do telefone), o sistema deve destacar ambos em verde e os demais telefones retornados pelo bureau devem ser destacados em vermelho. 
#					- Caso o telefone da proposta não seja igual a um dos telefones retornados pelo bureau, o sistema deve destacar todos os telefones em vermelho, tanto o da proposta quanto os retornados pelo bureau. 
#					- Se nenhum telefone for retornado pelo bureau, o sistema não deve destacar os telefones.
#
#			Pergunta 9: O endereço da proposta confere com algum retornado pelo bureau?
#
#				Campos: 
#					- Endereço da proposta: O sistema deve apresentar o endereço informado no onboarding (Máscara: Rua, Número – Complemento – Bairro, Município – UF, CEP), retornado pelo BPM.
#					- Endereços retornados do bureau: O sistema deve apresentar a lista dos endereços do sócio, retornados pelo bureau, esses endereços devem ser consultados no bureau da BigData Corp. Essa lista pode vir vazia, nesse caso a lista deverá ser apresentada vazia. 
#						- O sistema deve ter um serviço que efetuará a consulta dos dados dos sócios da empresa, de 10 em 10 segundos, nos datasets "Endereços", "Telefones" e "E-mails" do bureau da BigData Corp, para compor as perguntas dos formulários dos sócios. Sendo que, antes de efetuar a consulta diretamente no bureau da BigData, o sistema deve verificar se já existe uma consulta nos últimos 3 meses com os dados necessários (Datasets: "Endereços", "Telefones" e "E-mails") para o respectivo sócio, na tabela "BUREAU" do banco "BBSFRAUDE". Se existir, o sistema não deve efetuar uma nova consulta na BigData, deve apenas recuperar a consulta existente na base "BBSFRAUDE". Se não existir, o sistema deve efetuar a consulta no bureau da BigDataCorp e deve gravar o resultado dessa consulta na tabela "BUREAU" e deve gravar o log na tabela "BUREAU_LOG" do banco BBSFRAUDE.                                                                                                                                                                                                                                                                                                                                                                                                                     |
#
#				Comportamento:
#					N/A
#
#			Pergunta 10: O e-mail da proposta confere com algum retornado pelo bureau? 
#									
#				Campos: 
#					- E-mail da proposta: O sistema deve apresentar o e-mail informado no onboarding, retornado pelo BPM.
#					- E-mails retornados do bureau: O sistema deve apresentar a lista dos e-mails do sócio, retornados pelo bureau, esses e-mails devem ser consultados no bureau da BigData Corp. Essa lista pode vir vazia, nesse caso a lista deverá ser apresentada vazia. 
#						- O sistema deve ter um serviço que efetuará a consulta dos dados dos sócios da empresa, de 10 em 10 segundos, nos datasets "Endereços", "Telefones" e "E-mails" do bureau da BigData Corp, para compor as perguntas dos formulários dos sócios. Sendo que, antes de efetuar a consulta diretamente no bureau da BigData, o sistema deve verificar se já existe uma consulta nos últimos 3 meses com os dados necessários (Datasets: "Endereços", "Telefones" e "E-mails") para o respectivo sócio, na tabela "BUREAU" do banco "BBSFRAUDE". Se existir, o sistema não deve efetuar uma nova consulta na BigData, deve apenas recuperar a consulta existente na base "BBSFRAUDE". Se não existir, o sistema deve efetuar a consulta no bureau da BigDataCorp e deve gravar o resultado dessa consulta na tabela "BUREAU" e deve gravar o log na tabela "BUREAU_LOG" do banco BBSFRAUDE.                                                                                                                                                                                                                                                                                                                                                                                                                     |
#
#				Comportamento: 
#					- Se o e-mail da proposta for igual a um dos e-mails retornados pelo bureau, o sistema deve destacar ambos em verde e os demais e-mails retornados pelo bureau devem ser destacados em vermelho. 
#					- Caso o e-mail da proposta não seja igual a um dos e-mails retornados pelo bureau, o sistema deve destacar todos os e-mails em vermelho, tanto o da proposta quanto os retornados pelo bureau. 
#					- Se nenhum e-mail for retornado pelo bureau, o sistema não deve destacar os e-mails.
#
#			Pergunta 11: A consulta do bureau retornou os dados do cliente?
#
#				Campos: 
#					- Nome: O sistema deve recuperar o nome do sócio da consulta do bureau da BigData Corp.  
#					- Data de nascimento: O sistema deve recuperar a data de nascimento do sócio da consulta do bureau da BigData Corp.
#					- Mãe: O sisteam deve recuperar o nome da mãe do sócio da consulta do bureau da BigData Corp.
#					- Status do CPF: O sistema deve recuperar o status do CPF do sócio da consulta do bureau da BigData Corp.
#
#				Comportamento: 
#					N/A
#
#			Pergunta 12: Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização?
#
#				Campos: 
#					- O sistema deve apresentar o termo "Valide o documento na Receita Federal" na pergunta 12 como link.
#					- Ao clicar no link "Valide o documento na Receita Federal", o sistema deve abrir em uma nova guia o link: https://servicos.receita.fazenda.gov.br/Servicos/CPF/ConsultaSituacao/ConsultaPublica.asp
#
#				Comportamento: 
#					- Essa pergunta só será exibida se a pergunta 12 for respondida com a opção “Não”, caso a pergunta 12 seja respondida com uma opção diferente de “Não”, essa pergunta não deve ser apresentada.
#					- Quando a pergunta 12 não for respondida com a opção de resposta “Não”, o sistema deve responder automaticamente à pergunta 13 com a opção de resposta “N/A” e não deve apresentar a pergunta 13 na tela para o usuário responder.
#
#			Pergunta 13: Os dados abaixo conferem com os dados do documento (foto)?  
#			
#				Campos: 
#					- Nome: O sistema deve recuperar o nome do sócio da consulta do bureau da BigData Corp.  
#					- Data de nascimento: O sistema deve recuperar a data de nascimento do sócio da consulta do bureau da BigData Corp.
#					- Mãe: O sisteam deve recuperar o nome da mãe do sócio da consulta do bureau da BigData Corp.
#
#				Comportamento:
#					- Ao clicar uma vez sobre um dos campos listados acima, o sistema deve checar o campo como validado e deve destacá-lo em verde. Ao clicar duas vezes sobre o campo não destacado (cor preta) ou clicar uma vez quando ele já estiver destacado em verde, o sistema deve checar o campo como não validado e deve destacá-lo em vermelho.
#					- O sistema deve responder automaticamente essa pergunta com a opção de resposta “Sim” quando todos os campos forem validados, ou seja, quando todos os campos tiverem destacados em verde. 
#					- Se um dos campos estiver vazio, o sistema não deve permitir que o usuário o marque como validado ou não. E o sistema deve responder automaticamente essa pergunta com a opção de resposta “N/A” quando todos os campos estiverem vazios OU quando houver ao menos um campo vazio e nenhum dos campos estiverem destacados em vermelho, pois o sistema deve responder automaticamente essa pergunta com a opção de resposta “Não” quando pelo menos um campo não for validado, ou seja, quando pelo menos um dos campos tiver destacado em vermelho.
#
#			Pergunta 14: As perguntas do quiz foram exibidas?   
#			
#				Campos:
#					- Pergunta: O sistema deve apresentar as 5 perguntas respondidas pelo sócio durante o onboarding. Essas perguntas deverão ser recebidas do BPM, sendo perguntas aleatórias que o sócio responde durante o seu onboarding. As perguntas poderão vir nulas do BPM. Nesse caso, o sistema deve apresentar a tabela vazia. 
#					- Resposta do cliente: O sistema deve apresentar as respostas do sócio nas 5 perguntas respondidas durante o onboarding.
#						- Caso a resposta do sócio da proposta seja igual a resposta esperada da respectiva pergunta, o sistema deve apresentar a linha da tabela destacada na cor verde, ou seja, caso a “Resposta do cliente” = “Resposta esperada”. Caso a resposta do titular da proposta seja diferente da resposta esperada da respectiva pergunta, o sistema deve apresentar a linha da tabela destacada na cor vermelha, ou seja, caso a “Resposta do cliente” ≠ “Resposta esperada”.
#						- As perguntas poderão vir nulas do BPM. Nesse caso, o sistema deve apresentar a tabela vazia.
#						- Quando a resposta do cliente estiver vazia, ou seja, o cliente não respondeu à pergunta dentro do tempo estipulado no onboarding. O sistema deve apresentar a linha da tabela destacada na cor vermelha, pois não haverá a resposta do cliente, mas haverá a resposta esperada. Sendo assim, a “Resposta do cliente” ≠ “Resposta esperada”.
#					- Resposta esperada: O sistema deve apresentar as respostas esperadas pelo sistema para as 5 perguntas respondidas pelo titular durante o onboarding.
#						- Caso a resposta do sócio da proposta seja igual a resposta esperada da respectiva pergunta, o sistema deve apresentar a linha da tabela destacada na cor verde, ou seja, caso a “Resposta do cliente” = “Resposta esperada”. Caso a resposta do titular da proposta seja diferente da resposta esperada da respectiva pergunta, o sistema deve apresentar a linha da tabela destacada na cor vermelha, ou seja, caso a “Resposta do cliente” ≠ “Resposta esperada”.
#						- As perguntas poderão vir nulas do BPM. Nesse caso, o sistema deve apresentar a tabela vazia.
#
#				Comportamento:
#					N/A
#
#			Pergunta 15: O cliente acertou todas as perguntas do quiz? 
#
#				N/A
#
#			Pergunta 16: O cliente errou mais de uma pergunta do quiz?  
#
#				Campos: 
#					N/A
#
#				Comportamento: 
#					- Essa pergunta só será exibida se a pergunta 16 for respondida com as opções “Não” ou “N/A”, caso a pergunta 16 seja respondida com a opção “Sim”, essa pergunta não deve ser apresentada.
#					- Quando a pergunta 16 for respondida com a opção de resposta “Sim”, o sistema deve responder automaticamente à pergunta 17 com a opção de resposta “N/A” e não deve apresentar a pergunta 17 na tela para o usuário responder.
#					
#			Pergunta 17: De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?  
#			
#				N/A
#
#	BOTÕES "PRÓXIMO" E "ANTERIOR":
#		- O sistema deve apresentar o botão "Próximo" em todos os formulários, exceto o último.
#		- O sistema deve apresentar o botão "Anterior" em todos os formulários, exceto o primeiro que é o da empresa.
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################