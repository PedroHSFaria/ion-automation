﻿#language: pt-br

Funcionalidade: Onboarding PJ - Visualizar documentos de identificação dos sócios
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero visualizar os documentos de identificação dos sócios da empresa
	Para que possa analisá-los e responder os seus formulários

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	E clico em "Analisar" de uma proposta

@automatizar
Cenario: Apresentar foto do documento de identificação do único sócio da empresa
	Quando a empresa possui um único sócio
	E clico em "Foto"
	Então o sistema abre em uma nova guia a foto do documento de identificação mais recente do sócio da empresa contida no Ábaris

@automatizar
Cenario: Apresentar todas as fotos de identificação dos sócios da empresa
	Quando a empresa possui mais de um sócio 
	E clico em "Foto"
	Então o sistema abre em uma nova guia a foto do documento de identificação mais recente contida no Ábaris 
	Quando clico em "Próximo"
	Então o sistema apresenta a próxima foto do documento de identificação contida no Ábaris
	Quando clico em "Anterior"
	Então o sistema apresenta a foto do documento de identificação anterior contida no Ábaris
	

######################################## PROTÓTIPOS #########################################
#
#	Tela "Proposta" - Formulário da empresa: https://marvelapp.com/7e20jdb/screen/62378446
#	Tela "Proposta" - Formulário do sócio que preencheu a proposta: https://marvelapp.com/7e20jdb/screen/62378448
#	Tela "Proposta" - Formulário dos demais sócios da propsota: https://marvelapp.com/7e20jdb/screen/62378449
#	Visualizar todas as fotos contidas no Ábaris: https://marvelapp.com/7e20jdb/screen/62594097
#	Visualizar única foto contida no Ábaris: https://marvelapp.com/7e20jdb/screen/62594098
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################		
#
#	BOTÃO "FOTO":
#		Esse botão deve ficar fixo no topo da tela, de tal forma que ao rolar a tela para baixo ou para cima, o mesmo seja apresentado no topo da tela. 
#
################################# IDS PARA RECUPERAR OS DOCUMENTOS DO ÁBARIS ####################################									
#
#	Tipo de Documento: BS2 Empresas (ID = 52)
#	Índices: CNPJ (ID = 38) e Categoria Documento (ID = 103) 
#
#	O padrão utilizado para o preenchimento dos índices na inclusão dos documentos é a seguinte:
#
#		•	CNPJ
#			o	CNPJ da Empresa
#
#		•	Categoria Documento (Temos as 2 opções)
#			o	Contrato Social (Documento da Empresa)
#			o	Documento de Identificação (Documentos dos sócios)
#
#	No botão "Foto", além de recuperar os documentos dos sócios anexados na pasta "BS2 Empresas", o sistema deve recuperar os documentos (CNH e/ou RG) e as vídeos selfies e/ou selfies dos sócios da empresa nas pastas do Digital ("Digital- Documento" e "Digital - VideoSelfie"). 
#
######################################################################################