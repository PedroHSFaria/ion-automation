﻿#language: pt-br

Funcionalidade: Onboarding PJ - Concluir proposta através do botão "Natureza jurídica não permitida"
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero concluir uma proposta por natureza jurídica não permitida
	Para que a proposta seja encaminhada para análise dos times de prevenção à fraude e cadastro

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	E clico em "Analisar" de uma proposta

@automatizar
Cenario: Alterar a situação da proposta para "Concluída"
	Quando clico em "Natureza jurídica não permitida"
	Então o sistema apresenta a mensagem "Tem certeza que a natureza jurídica da empresa não é permitida? A proposta será enviada para análise. Não será mais possível responder as perguntas e esta ação não pode ser desfeita."
	Quando clico em "Concluir proposta"
	Então o sistema responde automaticamente todas as perguntas de todos os formulários
	E altera a situação da proposta para "Concluída"
	E apresenta a mensagem "A proposta <Seed da proposta> foi concluída com sucesso."
	E adiciona a "Data/Hora de conclusão" na proposta
	E me retorna para a tela de listagem das propostas
	E o sistema grava o log "Alteração de situação"
	E mantêm "Não" no campo "Fraude"
	E adiciona "Não" no campo "Natureza jurídica permitida"

@automatizar	
Cenario: Erro ao alterar a situação da proposta para "Concluída"
	Quando clico em "Natureza jurídica não permitida"
	Então o sistema apresenta a mensagem "Tem certeza que a natureza jurídica da empresa não é permitida? A proposta será enviada para análise. Não será mais possível responder as perguntas e esta ação não pode ser desfeita."
	Quando clico em "Concluir proposta"
	E ocorre algum erro 
	E o sistema não consegue alterar a situação da proposta para "Concluída"
	Então o sistema responde automaticamente todas as perguntas de todos os formulários
	E altera a situação da proposta para "Falha no envio"
	E grava as respostas das perguntas
	E me retorna para a tela de listagem das propostas
	E apresenta a mensagem "Desculpe, ocorreu um erro ao concluir a proposta <Seed da proposta>. Por favor, tente novamente e caso o erro persista entre em contato com o suporte técnico."
	E o sistema grava o log "Alteração de situação"
	E mantêm "Não" no campo "Fraude"
	E adiciona "Não" no campo "Natureza jurídica permitida"
	E o sistema tenta mais três vezes concluir a proposta, de forma automática
	E se em uma das três tentativas ele conseguir concluir a proposta
	Então altera a situação da proposta para "Concluída"
	E adiciona a "Data/Hora de conclusão" na proposta
	E as propostas com situção igual a "Falha no envio" ficam habilitas para que qualquer usuário possa tentar reenviá-la manualmente a qualquer momento

@automatizar	
Cenario: Cancelar conclusão da proposta por fraude - Botão "Cancelar"
	Quando clico em "Fraude"
	E clico em "Cancelar"
	Então o sistema cancela a conclusão da proposta por fraude
	E me mantêm na tela "Proposta"

@automatizar
Cenario: Cancelar conclusão da proposta - Botão "Fechar modal"
	Quando clico em "Fraude"
	E clico em "Fechar modal"
	Então o sistema cancela a conclusão da proposta por fraude
	E me mantêm na tela "Proposta"


######################################## PROTÓTIPOS #########################################
#
#	Modal "Concluir proposta com natureza não permitida": https://marvelapp.com/7e20jdb/screen/62378456
#	Mensagem proposta concluída com sucesso: https://marvelapp.com/7e20jdb/screen/62378452
#	Mensagem de erro ao concluir a proposta: https://marvelapp.com/7e20jdb/screen/62378453
#
################################# REGRAS DE NEGÓCIO ###################################	
#
#	| ID    | REGRA DE NEGÓCIO                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
#	| RN001 | Caso ocorra algum erro durante o envio da proposta para o BPM, o sistema deve alterar a situação da proposta para “Falha no envio”, deve redirecionar o usuário para a tela de listagem das propostas, tela “Onboarding” e deve apresentar a mensagem “Desculpe, ocorreu um erro ao concluir a proposta <Seed da proposta>. Por favor, tente novamente e caso o erro persista entre em contato com o suporte técnico.”. Sendo que, no caso de falha ao enviar a proposta para o BPM, o sistema deve tentar mais três vezes enviar a proposta, sendo de forma automática e invisível para o usuário. Sendo assim, se em uma das três tentativas a proposta for enviada com sucesso, ou seja, for concluída, o sistema deverá alterar a situação para “Concluída” e deverá gravar a data/hora que a mesma foi concluída no campo <Data/Hora de conclusão>. E as propostas com situação igual a “Falha no envio” ficarão habilitadas para que outro usuário possa tentar enviá-la. Para enviar uma proposta com situação igual a “Falha no envio”, o usuário terá que acionar o botão [Analisar], revisar as respostas e acionar o comando [Concluir]. Nesse caso, as perguntas já serão apresentadas respondidas com as respostas dadas anteriormente. |
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################		
#
#	BOTÃO "FRAUDE":
#		Esse botão deve ficar fixo em uma barra fixa na parte inferior tela, de tal forma que ao rolar a tela para baixo ou para cima, a barra será apresentada contendo esse botão.
#
#	RESPOSTAS AUTOMÁTICAS: 
#
#		Ao acionar o botão "Fraude", o sistema deve responder as perguntas dos formulários com as seguintes respostas:
#
#			FORMULÁRIO DA EMPRESA: 
#
#				| PERGUNTA                                                                                              | RESPOSTA AUTOMÁTICA |
#				| 1. A empresa está ativa no bureau ou na Receita Federal?                                              | N/A                 |
#				| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                 | N/A                 |
#				| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                 | N/A                 |
#				| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?       | N/A                 |
#				| 5. O domínio do e-mail pertence à empresa da proposta?                                                | N/A                 |
#				| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                   | N/A                 |
#				| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?         | N/A                 |
#				| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?         | N/A                 |
#				| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal? | N/A                 |
#
#			FORMULÁRIO DO SÓCIO QUE PREENCHEU A PROPOSTA:
#
#				| PERGUNTA                                                                                                           | RESPOSTA AUTOMÁTICA |
#				| 1. Os dados da proposta conferem com o documento?                                                                  | N/A                 |
#				| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           | N/A                 |
#				| 3. A foto do documento é válida?                                                                                   | N/A                 |
#				| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          | N/A                 |
#				| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        | N/A                 |
#				| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               | N/A                 |
#				| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  | N/A                 |
#				| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       | N/A                 |
#				| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 | N/A                 |
#				| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  | N/A                 |
#				| 11. A consulta do bureau retornou os dados do cliente?                                                             | N/A                 |
#				| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? | N/A                 |
#				| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     | N/A                 |
#				| 14. As perguntas do quiz foram exibidas?                                                                           | Não                 |
#				| 15. O cliente acertou todas as perguntas do quiz?                                                                  | N/A                 |
#				| 16. O cliente errou mais de uma pergunta do quiz?                                                                  | N/A                 |
#				| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   | Sim                 |
#
#			FORMULÁRIO DOS DEMAIS SÓCIOS DA PROPOSTA:
#
#				| PERGUNTA                                                                                                           | RESPOSTA AUTOMÁTICA |
#				| 1. Os dados da proposta conferem com o documento?                                                                  | N/A                 |
#				| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           | N/A                 |
#				| 3. A foto do documento é válida?                                                                                   | N/A                 |
#				| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          | N/A                 |
#				| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        | N/A                 |
#				| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               | N/A                 |
#				| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  | N/A                 |
#				| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       | N/A                 |
#				| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 | N/A                 |
#				| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  | N/A                 |
#				| 11. A consulta do bureau retornou os dados do cliente?                                                             | N/A                 |
#				| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? | N/A                 |
#				| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     | N/A                 |
#				| 14. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   | Sim                 |
#
#	FUNCIONALIDADE DO LOG "ONBOARDING PJ": 
#
#		EXEMPLO DO LOG DE "ALTERAÇÃO DE SITUAÇÃO": 
#
#			| NOME_DO_ATRIBUTO                                                                                                   | VALOR_ANTERIOR_A_TRANSACAO | VALOR_POSTERIOR_A_TRANSACAO |
#			| Produto                                                                                                            | Aplicativo                 | Aplicativo                  |
#			| Funcionalidade                                                                                                     | Onboarding PJ              | Onboarding PJ               |
#			| Transação                                                                                                          | Alteração de situação      | Alteração de situação       |
#			| Analista                                                                                                           | BSI90896 - José Silva      | BSI90896 - José Silva       |
#			| Data/Hora da transação                                                                                             | 30/09/2019 às 15:30:21     | 30/09/2019 às 15:30:21      |
#			| Seed                                                                                                               | 103986                     | 103986                      |
#			| Razão social                                                                                                       | Empresa A                  | Empresa A                   |
#			| CNPJ                                                                                                               | 09.163.687/0001-51         | 09.163.687/0001-51          |
#			| E-mail                                                                                                             | empresa@teste.com          | empresa@teste.com           |
#			| Data/Hora da abertura                                                                                              | 30/03/2018 às 14:49:23     | 30/03/2018 às 14:49:23      |
#			| Data/Hora início da análise                                                                                        | 30/03/2018 às 15:00:00     | 30/03/2018 às 15:00:00      |
#			| Data/Hora de conclusão                                                                                             |                            | 30/03/2018 às 16:00:00      |
#			| Situação                                                                                                           | Em análise                 | Concluída                   |
#			| FORMULÁRIO DA EMPRESA                                                                                              |                            |                             |
#			| 1. A empresa está ativa no bureau ou na Receita Federal?                                                           |                            | Sim                         |
#			| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                              |                            | Não                         |
#			| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                              |                            | N/A                         |
#			| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?                    |                            | Sim                         |
#			| 5. O domínio do e-mail pertence à empresa da proposta?                                                             |                            | Não                         |
#			| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                                |                            | N/A                         |
#			| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?                      |                            | Sim                         |
#			| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?                      |                            | Não                         |
#			| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal?              |                            | N/A                         |
#			| FORMULÁRIO DO SÓCIO QUE PREENCHEU A PROPOSTA                                                                       |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            | Sim                         |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            | Não                         |
#			| 3. A foto do documento é válida?                                                                                   |                            | N/A                         |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            | Sim                         |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            | Não                         |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            | N/A                         |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            | Sim                         |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            | Não                         |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            | Sim                         |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            | Não                         |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            | N/A                         |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            | Sim                         |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            | Não                         |
#			| 14. As perguntas do quiz foram exibidas?                                                                           |                            | Sim                         |
#			| 15. O cliente acertou todas as perguntas do quiz?                                                                  |                            | Não                         |
#			| 16. O cliente errou mais de uma pergunta do quiz?                                                                  |                            | N/A                         |
#			| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            | Sim                         |
#			| FORMULÁRIO DOS DEMAIS SÓCIOS DA PROPOSTA                                                                           |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            | Sim                         |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            | Não                         |
#			| 3. A foto do documento é válida?                                                                                   |                            | N/A                         |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            | Sim                         |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            | Não                         |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            | Sim                         |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            | Não                         |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            | Sim                         |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            | Não                         |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            | N/A                         |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            | Sim                         |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            | Não                         |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            | Sim                         |
#			| 14. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            | Não                         |
#			| Observação                                                                                                         |                            | Sem supeita de fruade.      |
#
#
#			| NOME_DO_ATRIBUTO                                                                                                   | VALOR_ANTERIOR_A_TRANSACAO | VALOR_POSTERIOR_A_TRANSACAO |
#			| Produto                                                                                                            | Aplicativo                 | Aplicativo                  |
#			| Funcionalidade                                                                                                     | Onboarding PJ              | Onboarding PJ               |
#			| Transação                                                                                                          | Alteração de situação      | Alteração de situação       |
#			| Analista                                                                                                           | BSI90896 - José Silva      | BSI90896 - José Silva       |
#			| Data/Hora da transação                                                                                             | 30/09/2019 às 15:30:21     | 30/09/2019 às 15:30:21      |
#			| Seed                                                                                                               | 103986                     | 103986                      |
#			| Razão social                                                                                                       | Empresa A                  | Empresa A                   |
#			| CNPJ                                                                                                               | 09.163.687/0001-51         | 09.163.687/0001-51          |
#			| E-mail                                                                                                             | empresa@teste.com          | empresa@teste.com           |
#			| Data/Hora da abertura                                                                                              | 30/03/2018 às 14:49:23     | 30/03/2018 às 14:49:23      |
#			| Data/Hora início da análise                                                                                        | 30/03/2018 às 15:00:00     | 30/03/2018 às 15:00:00      |
#			| Data/Hora de conclusão                                                                                             |                            |                             |
#			| Situação                                                                                                           | Em análise                 | Falha no envio              |
#			| FORMULÁRIO DA EMPRESA                                                                                              |                            |                             |
#			| 1. A empresa está ativa no bureau ou na Receita Federal?                                                           |                            | Sim                         |
#			| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                              |                            | Não                         |
#			| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                              |                            | N/A                         |
#			| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?                    |                            | Sim                         |
#			| 5. O domínio do e-mail pertence à empresa da proposta?                                                             |                            | Não                         |
#			| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                                |                            | N/A                         |
#			| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?                      |                            | Sim                         |
#			| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?                      |                            | Não                         |
#			| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal?              |                            | N/A                         |
#			| FORMULÁRIO DO SÓCIO QUE PREENCHEU A PROPOSTA                                                                       |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            | Sim                         |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            | Não                         |
#			| 3. A foto do documento é válida?                                                                                   |                            | N/A                         |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            | Sim                         |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            | Não                         |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            | N/A                         |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            | Sim                         |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            | Não                         |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            | Sim                         |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            | Não                         |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            | N/A                         |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            | Sim                         |
#			| 13. Os dados retornados da consulta do bureau conferem com os dados do documento?                                  |                            | Não                         |
#			| 14. As perguntas do quiz foram exibidas?                                                                           |                            | Sim                         |
#			| 15. O cliente acertou todas as perguntas do quiz?                                                                  |                            | Não                         |
#			| 16. O cliente errou mais de uma pergunta do quiz?                                                                  |                            | N/A                         |
#			| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            | Sim                         |
#			| FORMULÁRIO DOS DEMAIS SÓCIOS DA PROPOSTA                                                                           |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            | Sim                         |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            | Não                         |
#			| 3. A foto do documento é válida?                                                                                   |                            | N/A                         |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            | Sim                         |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            | Não                         |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            | Sim                         |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            | Não                         |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            | Sim                         |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            | Não                         |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            | N/A                         |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            | Sim                         |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            | Não                         |
#			| 13. Os dados retornados da consulta do bureau conferem com os dados do documento?                                  |                            | Sim                         |
#			| 14. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            | Não                         |
#			| Observação                                                                                                         |                            | Sem supeita de fruade.      |
#
######################################################################################