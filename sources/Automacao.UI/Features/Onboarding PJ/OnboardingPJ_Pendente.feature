﻿#language: pt-br

Funcionalidade: Onboarding PJ - Alterar a situação da proposta para "Pendente"
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero colocar uma proposta como pendente
	Para que possa analisar outras propostas enquanto as minhas dúvidas não são sanadas

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	E clico em "Analisar" de uma proposta

@automatizar
Cenario: Alterar a situação da proposta para "Pendente"
	Quando preencho ao menos um campo "Observação" de um dos formulários
	E clico em "Pendente"
	Então o sistema altera a situação da proposta para "Pendente"
	E me retorna para a tela de listagem das propostas
	E apresenta a mensagem "Situação alterada para: Pendente."
	E mantêm a "Data/Hora início da análise"
	E me mantem como "Responsável" por analisar a proposta
	E o sistema grava o log "Alteração de situação"

@automatizar
Cenario: Observação não preenchida
	Quando não preencho ao menos um campo "Observação" de um dos formulários
	E clico em "Pendente"
	Então o sistema apresenta a mensagem "O preenchimento do campo 'Observação' é obrigatório." no formulário da empresa
	E não prossegue com a alteração da situação da proposta para "Pendente"
	E me mantêm na tela "Proposta"

@automatizar	
Cenario: Erro ao alterar a situação da proposta para "Pendente"
	Quando ocorre algum erro 
	E o sistema não consegue alterar a situação da proposta para "Pendente"
	Então o sistema apresenta a mensagem "Desculpe, ocorreu um erro ao alterar a situação da proposta <Seed da proposta>. Por favor, tente novamente e caso o erro persista entre em contato com o suporte técnico."
	E me mantêm na tela "Proposta"


######################################## PROTÓTIPOS #########################################
#
#	Mensagem de situação alterada para "Pendente": https://marvelapp.com/7e20jdb/screen/62378455
#   Mensagem de erro ao alterar o status da proposta: https://marvelapp.com/7e20jdb/screen/62378445
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################		
#
#	MENSAGENS:
#
#		| MENSAGEM                                                                                                                                                                  | TEMPO DE EXIBIÇÃO |
#		| Situação alterada para: “Pendente”.                                                                                                                                       | 5 segundos        |
#		| Desculpe, ocorreu um erro ao alterar a situação da proposta <Seed da proposta>. Por favor, tente novamente e caso o erro persista entre em contato com o suporte técnico. | 5 segundos        |
#
#	BOTÃO "PENDENTE":
#		Esse botão deve ficar fixo em uma barra fixa na parte inferior tela, de tal forma que ao rolar a tela para baixo ou para cima, a barra será apresentada contendo esse botão.
#
#	FUNCIONALIDADE DO LOG "ONBOARDING PJ": 
#
#		EXEMPLO DO LOG DE "ALTERAÇÃO DE SITUAÇÃO": 
#
#			| NOME_DO_ATRIBUTO                                                                                                   | VALOR_ANTERIOR_A_TRANSACAO | VALOR_POSTERIOR_A_TRANSACAO                 |
#			| Produto                                                                                                            | Aplicativo                 | Aplicativo                                  |
#			| Funcionalidade                                                                                                     | Onboarding PJ              | Onboarding PJ                               |
#			| Transação                                                                                                          | Alteração de situação      | Alteração de situação                       |
#			| Analista                                                                                                           | BSI90896 - José Silva      | BSI90896 - José Silva                       |
#			| Data/Hora da transação                                                                                             | 30/09/2019 às 15:30:21     | 30/09/2019 às 15:30:21                      |
#			| Seed                                                                                                               | 103986                     | 103986                                      |
#			| Razão social                                                                                                       | Empresa A                  | Empresa A                                   |
#			| CNPJ                                                                                                               | 09.163.687/0001-51         | 09.163.687/0001-51                          |
#			| E-mail                                                                                                             | empresa@teste.com          | empresa@teste.com                           |
#			| Data/Hora da abertura                                                                                              | 30/03/2018 às 14:49:23     | 30/03/2018 às 14:49:23                      |
#			| Data/Hora início da análise                                                                                        | 30/03/2018 às 15:00:00     | 30/03/2018 às 15:00:00                      |
#			| Data/Hora de conclusão                                                                                             |                            |                                             |
#			| Situação                                                                                                           | Em análise                 | Pendente                                    |
#			| FORMULÁRIO DA EMPRESA                                                                                              |                            |                                             |
#			| 1. A empresa está ativa no bureau ou na Receita Federal?                                                           |                            | Não                                         |
#			| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                              |                            |                                             |
#			| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                              |                            |                                             |
#			| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?                    |                            |                                             |
#			| 5. O domínio do e-mail pertence à empresa da proposta?                                                             |                            | N/A                                         |
#			| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                                |                            |                                             |
#			| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?                      |                            |                                             |
#			| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?                      |                            |                                             |
#			| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal?              |                            |                                             |
#			| FORMULÁRIO DO SÓCIO QUE PREENCHEU A PROPOSTA                                                                       |                            |                                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            |                                             |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            | Não                                         |
#			| 3. A foto do documento é válida?                                                                                   |                            |                                             |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            |                                             |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            |                                             |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            |                                             |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            | Sim                                         |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            |                                             |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            |                                             |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            |                                             |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            |                                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            |                                             |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            |                                             |
#			| 14. As perguntas do quiz foram exibidas?                                                                           |                            | N/A                                         |
#			| 15. O cliente acertou todas as perguntas do quiz?                                                                  |                            |                                             |
#			| 16. O cliente errou mais de uma pergunta do quiz?                                                                  |                            |                                             |
#			| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            |                                             |
#			| FORMULÁRIO DOS DEMAIS SÓCIOS DA PROPOSTA                                                                           |                            |                                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            |                                             |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            |                                             |
#			| 3. A foto do documento é válida?                                                                                   |                            |                                             |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            |                                             |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            | Sim                                         |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            |                                             |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            |                                             |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            |                                             |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            |                                             |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            |                                             |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            |                                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            | Não                                         |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            |                                             |
#			| 14. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            | N/A                                         |
#			| Observação                                                                                                         |                            | Aguardando retorno sobre o contrato social. |
#
#	*Atualizar o "Relatório dos logs" para incluir os novos logs.
#
######################################################################################