﻿#language: pt-br

Funcionalidade: Onboarding PJ - Sair da proposta
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero sair de uma proposta
	Para que possa analisar outra proposta ou para que possa liberar a proposta para que outro usuário possa analisá-la

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	E clico em "Analisar" de uma proposta
	E clico em "Cancelar"
	Então o sistema apresenta a mensagem "Tem certeza de que deseja sair e não concluir essa proposta? As respostas serão perdidas e esta ação não pode ser desfeita."

@automatizar
Cenario: Sair da proposta
	Quando clico em "Sair da proposta"
	Então o sistema altera a situação da proposta para a situação anterior a de "Em análise"
	E altera a "Data/Hora início da análise" para a data/hora da análise anterior a minha
	E altera o "Responsável" para o da análise anterior
	E me retorna para a tela de listagem das propostas
	E o sistema grava o log "Alteração de situação"

@automatizar	
Cenario: Cancelar saída da proposta - Botão "Cancelar"
	Quando clico em "Cancelar"
	Então o sistema cancela a minha saída da proposta
	E me mantêm na tela "Proposta"

@automatizar
Cenario: Cancelar saída da proposta - Botão "Fechar modal"
	Quando clico em "Fechar modal"
	Então o sistema cancela a minha saída da proposta
	E me mantêm na tela "Proposta"

######################################## PROTÓTIPOS #########################################
#
#	Modal "Sair da proposta": https://marvelapp.com/7e20jdb/screen/62378450
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################		
#
#	BOTÃO "SAIR":
#		Esse botão deve ficar fixo em uma barra fixa na parte inferior tela, de tal forma que ao rolar a tela para baixo ou para cima, a barra será apresentada contendo esse botão.
#
#	FUNCIONALIDADE DO LOG "ONBOARDING PJ": 
#
#		EXEMPLO DO LOG DE "ALTERAÇÃO DE SITUAÇÃO": 
#
#			| NOME_DO_ATRIBUTO                                                                                                   | VALOR_ANTERIOR_A_TRANSACAO | VALOR_POSTERIOR_A_TRANSACAO |
#			| Produto                                                                                                            | Aplicativo                 | Aplicativo                  |
#			| Funcionalidade                                                                                                     | Onboarding PJ              | Onboarding PJ               |
#			| Transação                                                                                                          | Alteração de situação      | Alteração de situação       |
#			| Analista                                                                                                           | BSI90896 - José Silva      | BSI90896 - José Silva       |
#			| Data/Hora da transação                                                                                             | 30/09/2019 às 15:30:21     | 30/09/2019 às 15:30:21      |
#			| Seed                                                                                                               | 103986                     | 103986                      |
#			| Razão social                                                                                                       | Empresa A                  | Empresa A                   |
#			| CNPJ                                                                                                               | 09.163.687/0001-51         | 09.163.687/0001-51          |
#			| E-mail                                                                                                             | empresa@teste.com          | empresa@teste.com           |
#			| Data/Hora da abertura                                                                                              | 30/03/2018 às 14:49:23     | 30/03/2018 às 14:49:23      |
#			| Data/Hora início da análise                                                                                        | 30/03/2018 às 15:00:00     |                             |
#			| Data/Hora de conclusão                                                                                             |                            |                             |
#			| Situação                                                                                                           | Em análise                 | Aguardando análise          |
#			| FORMULÁRIO DA EMPRESA                                                                                              |                            |                             |
#			| 1. A empresa está ativa no bureau ou na Receita Federal?                                                           |                            |                             |
#			| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                              |                            |                             |
#			| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                              |                            |                             |
#			| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?                    |                            |                             |
#			| 5. O domínio do e-mail pertence à empresa da proposta?                                                             |                            |                             |
#			| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                                |                            |                             |
#			| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?                      |                            |                             |
#			| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?                      |                            |                             |
#			| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal?              |                            |                             |
#			| FORMULÁRIO DO SÓCIO QUE PREENCHEU A PROPOSTA                                                                       |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            |                             |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            |                             |
#			| 3. A foto do documento é válida?                                                                                   |                            |                             |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            |                             |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            |                             |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            |                             |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            |                             |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            |                             |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            |                             |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            |                             |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            |                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            |                             |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            |                             |
#			| 14. As perguntas do quiz foram exibidas?                                                                           |                            |                             |
#			| 15. O cliente acertou todas as perguntas do quiz?                                                                  |                            |                             |
#			| 16. O cliente errou mais de uma pergunta do quiz?                                                                  |                            |                             |
#			| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            |                             |
#			| FORMULÁRIO DOS DEMAIS SÓCIOS DA PROPOSTA                                                                           |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            |                             |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            |                             |
#			| 3. A foto do documento é válida?                                                                                   |                            |                             |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            |                             |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            |                             |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            |                             |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            |                             |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            |                             |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            |                             |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            |                             |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            |                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            |                             |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            |                             |
#			| 14. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            |                             |
#			| Observação                                                                                                         |                            |                             |
#
#	*Atualizar o "Relatório dos logs" para incluir os novos logs.
#
######################################################################################