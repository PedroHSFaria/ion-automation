﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Automacao.UI.Features.OnboardingPJ
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Onboarding PJ - Visualizar documentos de identificação dos sócios")]
    public partial class OnboardingPJ_VisualizarDocumentosDeIdentificacaoDosSociosFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "OnboardingPJ_Foto.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("pt-br"), "Onboarding PJ - Visualizar documentos de identificação dos sócios", "\tComo um usuário do Cactus com um dos seguintes perfis de acesso: \"Administrador\"" +
                    ", \"Coordenador BPO\", \"Analista\" ou \"BPO\"\r\n\tQuero visualizar os documentos de ide" +
                    "ntificação dos sócios da empresa\r\n\tPara que possa analisá-los e responder os seu" +
                    "s formulários", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 8
#line 9
 testRunner.Given("que estou logado no sistema", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 10
 testRunner.When("clico em \"Onboarding\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 11
 testRunner.And("clico em \"Pessoa jurídica\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 12
 testRunner.And("clico em \"Analisar\" de uma proposta", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Apresentar foto do documento de identificação do único sócio da empresa")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void ApresentarFotoDoDocumentoDeIdentificacaoDoUnicoSocioDaEmpresa()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Apresentar foto do documento de identificação do único sócio da empresa", null, new string[] {
                        "automatizar"});
#line 15
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 16
 testRunner.When("a empresa possui um único sócio", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 17
 testRunner.And("clico em \"Foto\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 18
 testRunner.Then("o sistema abre em uma nova guia a foto do documento de identificação mais recente" +
                    " do sócio da empresa contida no Ábaris", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Apresentar todas as fotos de identificação dos sócios da empresa")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void ApresentarTodasAsFotosDeIdentificacaoDosSociosDaEmpresa()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Apresentar todas as fotos de identificação dos sócios da empresa", null, new string[] {
                        "automatizar"});
#line 21
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 22
 testRunner.When("a empresa possui mais de um sócio", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 23
 testRunner.And("clico em \"Foto\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 24
 testRunner.Then("o sistema abre em uma nova guia a foto do documento de identificação mais recente" +
                    " contida no Ábaris", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 25
 testRunner.When("clico em \"Próximo\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 26
 testRunner.Then("o sistema apresenta a próxima foto do documento de identificação contida no Ábari" +
                    "s", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 27
 testRunner.When("clico em \"Anterior\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 28
 testRunner.Then("o sistema apresenta a foto do documento de identificação anterior contida no Ábar" +
                    "is", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
