﻿#language: pt-br

Funcionalidade: Onboarding PJ - Recalcular idade
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero recalcular as idades dos sócios da empresa
	Para que possa analisar se todos são maiores de idade

Contexto: 
	Dado que estou logado no sistema PJ
	Quando clico em Onboarding PJ
	E clico em Pessoa juridica
	E clico em Analisar de uma proposta pj
	E clico em Recalcular idade PJ

@automatizadoProblemaClickAnalisar
Cenario: Recalcular idade
	Quando informo uma data de nascimento PJ
	E clico em Recalcular idade no modal PJ
	Então o sistema apresenta a idade calculada PJ

@automatizadoProblemaClickAnalisar
Cenario: Campo obrigátório não preenchido
	Quando não informo uma data de nascimento
	E clico em Recalcular idade no modal PJ
	Então o sistema apresenta a mensagem "O preenchimento do campo ‘Data de nascimento’ é obrigatório." PJ

@automatizadoProblemaClickAnalisar	
Cenario: Data de nascimento inválida
	Quando informar uma data de nascimento inválida PJ
	E clico em Recalcular idade no modal PJ
	Então o sistema apresenta a mensagem "O valor informado no campo 'Data de nasciemnto' é inválido." PJ

@automatizadoProblemaClickAnalisar
Cenario: Data de nascimento maior que a data atual 
	Quando informar uma data de nascimento maior que a data atual PJ
	E clico em Recalcular idade no modal PJ
	Então o sistema apresenta a mensagem "A data informada no campo 'Data de nascimento' não pode ser maior que a data atual." PJ

######################################## PROTÓTIPOS #########################################
#
#	Tela "Proposta" - Formulário da empresa: https://marvelapp.com/7e20jdb/screen/62378446
#	Tela "Proposta" - Formulário do sócio que preencheu a proposta: https://marvelapp.com/7e20jdb/screen/62378448
#	Tela "Proposta" - Formulário dos demais sócios da propsota: https://marvelapp.com/7e20jdb/screen/62378449
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################		
#
#	BOTÃO "RECALCULAR IDADE":
#		Esse botão deve ficar fixo no topo da tela, de tal forma que ao rolar a tela para baixo ou para cima, o mesmo seja apresentado no topo da tela. 
#
######################################################################################