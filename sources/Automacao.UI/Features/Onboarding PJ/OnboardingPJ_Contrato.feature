﻿#language: pt-br

Funcionalidade: Onboarding PJ - Visualizar contrato social
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero visualizar o contrato social da empresa
	Para que possa analisá-lo e responder o formulário da empresa

Contexto: 
	Dado que estou logado no sistema
	Quando clico em "Onboarding"
	E clico em "Pessoa jurídica"
	E clico em "Analisar" de uma proposta

@automatizar
Cenario: Apresentar único contrato social da empresa
	Quando a empresa possui um único contrato social no Ábaris
	E clico em "Contrato"
	Então o sistema abre em uma nova guia o contrato mais recente da empresa 

@automatizar
Cenario: Apresentar todos os contratos sociais da empresa
	Quando a empresa possui mais de um contrato social no Ábaris
	E clico em "Contrato"
	Então o sistema abre em uma nova guia o contrato mais recente da empresa 
	Quando clico em "Próximo"
	Então o sistema apresenta o próximo contrato social da empresa contido no Ábaris
	Quando clico em "Anterior"
	Então o sistema apresenta o contrato social anterior da empresa contido no Ábaris
	

######################################## PROTÓTIPOS #########################################
#
#	Tela "Proposta" - Formulário da empresa: https://marvelapp.com/7e20jdb/screen/62378446
#	Tela "Proposta" - Formulário do sócio que preencheu a proposta: https://marvelapp.com/7e20jdb/screen/62378448
#	Tela "Proposta" - Formulário dos demais sócios da propsota: https://marvelapp.com/7e20jdb/screen/62378449
#	Visualizar todos os contratos contidos no Ábaris: https://marvelapp.com/7e20jdb/screen/62594097
#	Visualizar único contrato contido no Ábaris: https://marvelapp.com/7e20jdb/screen/62594098
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################		
#
#	BOTÃO "CONTRATO":
#		Esse botão deve ficar fixo no topo da tela, de tal forma que ao rolar a tela para baixo ou para cima, o mesmo seja apresentado no topo da tela. 
#
################################# IDS PARA RECUPERAR OS DOCUMENTOS DO ÁBARIS ####################################									
#
#	Tipo de Documento: BS2 Empresas (ID = 52)
#	Índices: CNPJ (ID = 38) e Categoria Documento (ID = 103) 
#
#	O padrão utilizado para o preenchimento dos índices na inclusão dos documentos é a seguinte:
#
#		•	CNPJ
#			o	CNPJ da Empresa
#
#		•	Categoria Documento (Temos as 2 opções)
#			o	Contrato Social (Documento da Empresa)
#			o	Documento de Identificação (Documentos dos sócios)
#
######################################################################################