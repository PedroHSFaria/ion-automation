﻿#language: pt-br

Funcionalidade: Selecionar mais opções nos campos de filtros do tipo "ComboBox"
	Como usuário do sistema
	Quero poder selecionar mais de uma opção nos campos de filtros do tipo "ComboBox"
	Para que possa efetuar filtros mais assertivos
	
Contexto: 
	Dado que acessei o sistema
	Quando acesso as seguintes telas
	| TELAS              |
	| Alertas de fraudes |
	| Contas bloqueadas  |
	| Lista positiva     |
	| Onboarding         |
	| Logs               |
	| Regras de alertas  |
	E clico no campo de filtro do tipo combobox
	Então o sistema apresenta as opções do combobox
	Quando seleciono mais de uma opção no combobox
	Então o sistema apresenta as opções selecionadas

@automatizado
Cenario: Filtrar utilizando mais de uma opção no campo de fitro do tipo "ComboBox" Alerta de fraudes
	Quando clico em "Filtrar"
	Então o sistema retorna os registros de acordo com os filtros aplicados

@automatizado
Cenario: Excluir opção selecionada no campo de filtro do tipo "ComboBox" Alerta de fraudes
	Quando clico em "Excluir" em uma das opções selecionadas do combobox
	Então o sistema exclui a opção da lista de opçõeos selecionadas do combobox

@automatizado
Cenario: Limpar filtros Alerta de fraudes
	Quando clico em Limpar alertas
	Então o sistema limpa os filtros aplicados
	E apresenta os filtros com seus valores iniciais

################################ OBSERVAÇÃO ######################################
#
#	Segue o exemplo de um componente que pode ser usado: https://www.npmjs.com/package/ng-multiselect-dropdown
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
#	Aumentar os comboboxes para permitir ler todo o texto, pois hoje o sistema está cortando os textos maiores.
#
#	CAMPOS DE FILTROS DO TIPO COMBOBOX DA TELA "ALERTAS DE FRAUDES":
#	
#			| FILTRO         | TIPO     | VALORES                                                                                                      | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL      |
#			| Produto        | Combobox | "Todos" e "Aplicativo"                                                                                       | Não         | Não          | N/A     | N/A         | Todos              |
#			| Alerta         | Combobox | "Todos" e lista dos nomes das regras                                                                         | Não         | Não          | N/A     | N/A         | Todos              |
#			| Gravidade      | Combobox | "Todas", "Alta", "Média", "Baixa" e "Info"                                                                   | Não         | Não          | N/A     | N/A         | Todas              |
#			| Situação       | Combobox | "Todas", "Não tratados", "Aguardando análise", "Em análise", "Pendente", "Tratados", "Não fraude" e "Fraude" | Não         | Não          | N/A     | N/A         | Aguardando análise |
#			| Tipo de pessoa | Combobox | "Todas", "Pessoa física" e "Pessoa jurídica"                                                                 | Não         | Não          | N/A     | N/A         | Todas              |
#	
#	CAMPOS DE FILTROS DO TIPO COMBOBOX DA TELA "CONTAS BLOQUEADAS":
#	
#			| FILTRO              | TIPO     | VALORES                      | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL |
#			| Bloqueio preventivo | Combobox | "Todos", "Ativo" e "Inativo" | Não         | Não          | N/A     | N/A         | Todos         |
#	
#	CAMPOS DE FILTROS DO TIPO COMBOBOX DA TELA "LISTA POSITIVA":
#	
#			| FILTRO                    | TIPO     | VALORES                                             | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL |
#			| Responsável pela inclusão | Combobox | "Todos" e lista dos usuários/nomes dos responsáveis | Não         | Não          | N/A     | N/A         | Todos         |
#	
#	CAMPOS DE FILTROS DO TIPO COMBOBOX DA TELA "ONBOARDING":
#	
#			| FILTRO   | TIPO     | VALORES                                                                                 | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL |
#			| Situação | Combobox | "Todas", "Aguardando análise", "Em análise", "Pendente", "Concluída" e "Falha no envio" | Não         | Não          | N/A     | N/A         | Todos         |
#			| Fraude   | Combobox | "Todas", "Sim" e "Não"                                                                  | Não         | Não          | N/A     | N/A         | Todas         |
#	
#	CAMPOS DE FILTROS DO TIPO COMBOBOX DA TELA "LOGS":
#	
#			| FILTRO         | TIPO     | VALORES                                          | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL |
#			| Produto        | Combobox | "Todos" e "Aplicativo"                           | Não         | Não          | N/A     | N/A         | Todos         |
#			| Funcionalidade | Combobox | "Todas" e lista das funcionalidades              | Não         | Não          | N/A     | N/A         | Todas         |
#			| Transação      | Combobox | "Todas" e lista das transações                   | Não         | Não          | N/A     | N/A         | Todas         |
#			| Analista       | Combobox | "Todos" e lista dos usuários/nomes dos analistas | Não         | Não          | N/A     | N/A         | Todos         |
#	
#	CAMPOS DE FILTROS DO TIPO COMBOBOX DA TELA "REGRAS DE ALERTAS":
#	
#			| FILTRO              | TIPO     | VALORES                      | OBRIGATORIO | DESABILITADO | MASCARA | PLACEHOLDER | VALOR_INICIAL |
#			| Bloqueio automático | Combobox | "Todos", "Ativo" e "Inativo" | Não         | Não          | N/A     | N/A         | Todos         |
#	
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################