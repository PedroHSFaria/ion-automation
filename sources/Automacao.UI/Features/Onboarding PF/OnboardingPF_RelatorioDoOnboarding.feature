﻿#language: pt-br

Funcionalidade: Onboarding PF - Relatório do onboarding
	Como um usuário do Cactus com um dos seguintes perfis de acesso: Administrador ou Coordenador BPO
	Quero poder extrair o relatório do onboarding
	Para visualizar as propostas de PF de acordo com os filtros aplicados

@automatizar
Esquema do Cenario: Exportar "Relatório do onboarding"
	Dado que estou logado com o <perfil de acesso>	
	E clico em "Onboarding"
	E existem registros retornados na lista de propostas
	Quando clico em "exportar para Excel"
	E clico em "Relatório do onboarding"
	Então o sistema efetua o download do "Relatório do onboarding" no formato ".xlsx", contendo as propostas de PF de acordo com os filtros aplicados 
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |