﻿#language: pt-br

Funcionalidade: Onboarding PF - Alterar a situação da proposta para "Pendente"
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero colocar uma proposta como pendente
	Para que possa analisar outras propostas enquanto as minhas dúvidas não são sanadas

Contexto: 
	Dado que estou logado no sistema
	E clico em "Onboarding"
	E clico em "Pessoa física"
	E clico em "Analisar" de uma proposta

@automatizar
Cenario: Alterar a situação da proposta para "Pendente"
	Quando preencho o campo "Observação"
	E clico em "Pendente"
	Então o sistema altera a situação da proposta para "Pendente"
	E me retorna para a tela de listagem das propostas
	E apresenta a mensagem "Situação alterada para: Pendente."
	E mantêm a "Data/Hora início da análise"
	E me mantem como "Responsável" por analisar a proposta
	E o sistema grava o log "Alteração de situação"

@automatizar
Cenario: Observação não preenchida
	Quando não preencho o campo "Observação"
	E clico em "Pendente"
	Então o sistema apresenta a mensagem "O preenchimento do campo 'Observação' é obrigatório."
	E não altera a situação da proposta para "Pendente"
	E me mantêm na tela "Proposta"

@automatizar	
Cenario: Erro ao alterar a situação da proposta para "Pendente"
	Quando ocorre algum erro 
	E o sistema não consegue alterar a situação da proposta para "Pendente"
	Então o sistema apresenta a mensagem "Desculpe, ocorreu um erro ao alterar a situação da proposta <Seed da proposta>. Por favor, tente novamente e caso o erro persista entre em contato com o suporte técnico."
	E me mantêm na tela "Proposta"