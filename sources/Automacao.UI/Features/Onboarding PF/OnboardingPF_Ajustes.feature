﻿#language: pt-br

Funcionalidade: Onboarding PF - Ajustes para inclusão do onboarding de PJ
	Como um usuário do Cactus com um dos seguintes perfis de acesso: Administrador, Coordenador BPO, Analista ou BPO
	Quero visualizar e responder as propostas do onboarding de PF do aplicativo do Bs2
	Para que possa consultar as propostas já respondidas e para que possa responder as propostas pendentes de análise

Contexto: 
	Dado que eu acesso a página de login

@automatizado
Esquema do Cenario: Visualizar propostas dos usuarios selecionados
	Dado que estou logado com o "<perfil de acesso>"
	Quando clico em Onboarding
	E clico em Pessoa física
	Então o sistema apresenta a tela "Onboarding PF" contendo as propostas do onboarding de PF
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |
	| Analista         |
	| BPO              |

@automatizar	
Esquema do Cenario: Apresentar única foto do documento do titular
	Dado que estou logado com o "<perfil de acesso>"
	Quando clico em "Onboarding"
	E clico em "Pessoa física"
	E clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	E o titular possui uma única foto de identificação no Ábaris
	E clico em "Foto"
	Então o sistema abre em uma nova guia a foto do documento de identificação mais recente do titular contida no Ábaris
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |
	| Analista         |
	| BPO              |

@automatizar
Esquema do Cenario: Apresentar todas as fotos de identificação do titular
	Dado que estou logado com o "<perfil de acesso>	"
	Quando clico em "Onboarding"
	E clico em "Pessoa física"
	E clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	E o titular possui mais de uma foto no Ábaris
	E clico em "Foto"
	Então o sistema abre em uma nova guia a foto de identificação mais recente do titular contida no Ábaris 
	Quando clico em "Próximo"
	Então o sistema apresenta a próxima foto do documento de identificação contida no Ábaris
	Quando clico em "Anterior"
	Então o sistema apresenta a foto do documento de identificação anterior contida no Ábaris
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |
	| Analista         |
	| BPO              |

@automatizar
Esquema do Cenario: Apresentar único vídeo ou selfie do titular
	Dado que estou logado com o <perfil de acesso>	
	Quando clico em "Onboarding"
	E clico em "Pessoa física"
	E clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	E o titular possui um único vídeo ou selfie no Ábaris
	E clico em "Vídeo"
	Então o sistema abre em uma nova guia o vídeo ou selfie mais recente do titular contido no Ábaris
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |
	| Analista         |
	| BPO              |

@automatizar
Esquema do Cenario: Apresentar todos os vídeos e/ou selfies do titular
	Dado que estou logado com o <perfil de acesso>	
	Quando clico em "Onboarding"
	E clico em "Pessoa física"
	E clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	E o titular possui mais de um vídeo e/ou selfie no Ábaris
	E clico em "Vídeo"
	Então o sistema abre em uma nova guia o vídeo ou selfie mais recente do titular contida no Ábaris 
	Quando clico em "Próximo"
	Então o sistema apresenta o próximo vídeo ou selfie contido no Ábaris 
	Quando clico em "Anterior"
	Então o sistema apresenta o vídeo ou selfie anterior contido no Ábaris
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |
	| Analista         |
	| BPO              |

################################ PROTÓTIPOS #####################################
#
#	Opção de menu "Onboarding": https://marvelapp.com/7e20jdb/screen/62378250
#	Tela "Onboarding PF": https://marvelapp.com/7e20jdb/screen/62378251
#	Tela "Proposta": https://marvelapp.com/7e20jdb/screen/62378252
#	Visualizar todas as fotos/vídeos contidos no Ábaris: https://marvelapp.com/7e20jdb/screen/62594097
#	Visualizar única foto/vídeo contida no Ábaris: https://marvelapp.com/7e20jdb/screen/62594098
#
################################## AJUSTES ######################################									
#
#	Ajustes:
#		- No menu "Onboarding" apresentar as opções de submenu "Pessoa física" e "Pessoa jurídica". 
#		- Alterar o nome da tela de "Onboarding" para "Onboarding PF".
#		- Alterar o breadcrumb de "Onboarding" para "Onboarding PF" nas telas "Onboarding PF" e "Proposta".
#		- Remover a coluna "Data/Hora de conclusão" do perfil de acesso "BPO".
#		- Alterar o nome da funcionalidade do log de "Onboarding" para "Onboarding PF".
#		- Ao clicar nos botões de foto ("Foto", "Foto frente" e "Foto verso"), o sistema deve recuperar e apresentar todas as fotos dos documentos do titular contidas no Ábaris.
#		- Ao clicar no botão "Vídeo", o sistema deve recuperar e apresentar todas os vídeos e selfies do titular contidos no Ábaris.
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################									
#			
#	COLUNAS DA TELA "ONBOARDING PJ" - PERFIL "BPO":
#		
#		| COLUNA                 | TIPO     | VALORES                                                                                  |
#		| Seed                   | Inteiro  | ID da solicitação                                                                        |
#		| Data/Hora de abertura  | DateTime | Data/Hora cuja proposta foi gerada                                                       |
#		| Responsável            | String   | Nome do responsável atribuído a proposta                                                 |
#		| Situação               | Combobox | "Aguardando análise", "Em análise", "Pendente", Concluída e "Falha no envio"             |
#		| Ações                  | N/A      | Botões "Analisar"e "Ver detalhes" e Ícones "Proposta sem contrato" e "Proposta sem foto" |
#
#################################################################################