﻿#language: pt-br

Funcionalidade: Onboarding PF - Listagem das propostas na visão do perfil "BPO"
	Como um usuário do Cactus com o perfil de acesso "BPO" 
	Quero visualizar as propostas do onboarding de PF
	Para que possa consultar as propostas já respondidas e as pendentes de análise

Contexto: 
	Dado que estou logado no sistema
	E clico em "Onboarding"
	E clico em "Pessoa física"
	E o sistema apresenta a tela "Onboarding PF" contendo as propostas do onboarding de PF

@automatizar
Cenario: Filtrar propostas do onboarding de PF
	Quando preencho os campos de filtros desejados
	| CAMPOS DE FILTRO            | VALOR           |
	| Seed                        | Valor inteiro   |
	| Situação                    | Situação válida |
	E clico em "Filtrar"
	Então o sistema recupera e apresenta as propostas de acordo com os filtros aplicados

@automatizar
Cenario: Limpar filtro
	Quando preencho os campos de filtros desejados
	| CAMPOS DE FILTRO            | VALOR           |
	| Seed                        | Valor inteiro   |
	| Situação                    | Situação válida |
	E clico em "Filtrar"
	E o sistema recupera e apresenta as propostas de acordo com os filtros aplicados
	E clico em "Limpar"
	Então o sistema limpa os filtros aplicados anteriormente
	E recupera e apresenta os registros sem nenhum filtro aplicado

@automatizar
Cenario: Nenhuma proposta de onboarding de PF foi efetuada
	Quando nenhuma proposta de onboarding de PJ foi efetuada
	Então o sistema apresenta a mensagem "Não existem propostas para serem listadas no momento."

@automatizar
Cenario: Nenhuma proposta de onboarding de PF foi encontrada
	Quando preencho os campos de filtros desejados
	| CAMPOS DE FILTRO            | VALOR           |
	| Seed                        | Valor inteiro   |
	| Situação                    | Situação válida |
	E clico em "Filtrar"
	E o sistema não encontra nenhuma proposta de acordo com os filtros aplicados
	Então apresenta a mensagem "Não existem propostas para serem listadas no momento."