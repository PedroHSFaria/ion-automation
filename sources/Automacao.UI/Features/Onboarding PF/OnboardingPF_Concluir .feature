﻿#language: pt-br

Funcionalidade: Onboarding PF - Concluir proposta através do botão "Concluir"
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero concluir a análise de uma proposta
	Para que a proposta siga o fluxo de aprovação/reprovação no BPM

Contexto: 
	Dado que estou logado no sistema
	E clico em Onboarding
	E clico em Pessoa física
	E clico em Analisar de uma proposta

@automatizar
Cenario: Alterar a situação da proposta para "Concluída"
	Quando respondo todas as perguntas obrigatórias
	E clico em Concluir
	E o sistema apresenta a mensagem Tem certeza de que deseja concluir essa proposta? Não será mais possível responder as perguntas e esta ação não pode ser desfeita.
	E clico em Concluir proposta
	Então o sistema altera a situação da proposta para "Concluída"
	E apresenta a mensagem "A proposta <Seed da proposta> foi concluída com sucesso."
#	E adiciona a "Data/Hora de conclusão" na proposta
	E me retorna para a tela de listagem das propostas
#	E o sistema grava o log "Alteração de situação"

@automatizar
Cenario: Erro ao alterar a situação da proposta para "Concluído"
	Quando respondo todas as perguntas obrigatórias
	E clico em Concluir
	E ocorre algum erro 
	Então o sistema altera a situação da proposta para "Falha no envio"
	E grava as respostas das perguntas
	E me retorna para a tela de listagem das propostas
	E apresenta a mensagem "Desculpe, ocorreu um erro ao concluir a proposta <Seed da proposta>. Por favor, tente novamente e caso o erro persista entre em contato com o suporte técnico."

@automatizar
Cenario: Perguntas obrigatórias não respondidas
	Quando clico em "Concluir" sem que tenha respondido todas as perguntas obrigatórias
	E o sistema apresenta a mensagem "Tem certeza de que deseja concluir essa proposta? Não será mais possível responder as perguntas e esta ação não pode ser desfeita."
	E clico em Concluir proposta
	Então o sistema apresenta a mensagem "É preciso responder essa pergunta para concluir a proposta." em todas as perguntas obrigatórias não respondidas
	E me redireciona para a primeira pergunta obrigatória não respondida

@automatizadoProntoPraSubir
Cenario: Cancelar conclusão da proposta - Botão "Cancelar"
	Quando respondo todas as perguntas obrigatórias
	E clico em Concluir
	E clico em Cancelar conclusao de proposta
	Então o sistema cancela a conclusão da proposta
	E me mantêm na tela Proposta

@automatizadoProntoPraSubir
Cenario: Cancelar conclusão da proposta - Botão "Fechar modal"
	Quando respondo todas as perguntas obrigatórias
	E clico em Concluir
	E clico em Fechar modal
	Então o sistema cancela a conclusão da proposta
	E me mantêm na tela "Proposta"