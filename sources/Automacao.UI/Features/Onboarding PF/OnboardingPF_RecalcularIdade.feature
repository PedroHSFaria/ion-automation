﻿#language: pt-br

Funcionalidade: Onboarding PF - Recalcular idade
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero recalcular a idade do cliente
	Para que possa analisar se o mesmo é maior de idade

Contexto: 
	Dado que estou logado no sistema pf
	E clico em "Onboarding" pf
	E clico em "Pessoa física" pessoa
	E clico em "Analisar" de uma proposta pf

@automatizado
Cenario: Recalcular idade
	Quando informo uma data de nascimento
	E clico em "Recalcular idade" nascimento
	Então o sistema apresenta a idade calculada 

@automatizado
Cenario: Campo obrigátório não preenchido
	Quando clico em "Recalcular idade" sem que tenha informado uma data de nascimento
	Então o sistema apresenta a mensagem "O preenchimento do campo ‘Data de nascimento’ é obrigatório." pf

@automatizado
	Cenario: Data de nascimento inválida
	Quando informar uma data de nascimento inválida
	E clico em "Recalcular idade" pessoa fisica
	Então o sistema apresenta a mensagem "O valor informado no campo 'Data de nasciemnto' é inválido." pessoa fisica

@automatizado
Cenario: Data de nascimento maior que a data atual
	Quando informar uma data de nascimento maior que a data atual
	E clico em "Recalcular idade" maior que a atual
	Então o sistema apresenta a mensagem "A data informada no campo 'Data de nascimento' não pode ser maior que a data atual." data maior que a atual