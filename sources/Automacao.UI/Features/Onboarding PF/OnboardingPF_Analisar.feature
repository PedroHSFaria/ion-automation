﻿#language: pt-br

Funcionalidade: Onboarding PF - Alterar a situação da proposta para "Em análise"
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero analisar uma proposta
	Para que possa responder os formulários

Contexto: 
	Dado que estou logado no sistema
	E clico em Onboarding
	E clico em Pessoa física
	E o sistema apresenta a tela Onboarding PF

@automatizar
Cenario: Alterar a situação da proposta para "Em análise"
	Quando clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	Então o sistema me redireciona para a tela Proposta
	E apresenta a mensagem Situação alterada para: “Em análise”. Você tem até às 23:59:59 de hoje para finalizar a análise dessa proposta.
#	E altera a situação da proposta para "Em análise"
#	E adiciona a Data/Hora início da análise na proposta
#	E me coloca como o "Responsável" por analisar essa proposta
#	E o sistema grava o log Alteração de situação

@automatizar
Cenario: Proposta sendo analisada por outro analista
	Quando clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	E outro usuário clicou em "Analisar" na mesma proposta um pouco antes de mim
	Então o sistema apresenta a mensagem Desculpe, não será possível analisar essa proposta, pois outro analista iniciou a análise.
	Quando clico em Ok
	Então o sistema me retorna para a tela de listagem das propostas

@automatizar
Cenario: Propostas não analisadas até 23:59:59 do dia
	Quando clico em "Analisar" de uma proposta com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	| Falha no envio     |
	E o sistema me redireciona para a tela "Proposta"
	E não altero o status da proposta para um dos status abaixo até às 23:59:59 do dia atual
	| STATUS             |
	| Concluída          |
	| Pendente           |
	| Falha no envio     |
	| Aguardando análise |
	Então o sistema deve alterar a situação da proposta para a situação anterior a de "Em análise" às 00:00:00 do dia seguinte
	E deve limpar a "Data/Hora início da análise"
	E deve me remover de "Responsável" por analisar a proposta
	E o sistema grava o log Alteração de situação