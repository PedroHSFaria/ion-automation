﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Automacao.UI.Features.OnboardingPF
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Onboarding PF - Alterar a situação da proposta para \"Pendente\"")]
    public partial class OnboardingPF_AlterarASituacaoDaPropostaParaPendenteFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "OnboardingPF_Pendente.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("pt-br"), "Onboarding PF - Alterar a situação da proposta para \"Pendente\"", "\tComo um usuário do Cactus com um dos seguintes perfis de acesso: \"Administrador\"" +
                    ", \"Coordenador BPO\", \"Analista\" ou \"BPO\"\r\n\tQuero colocar uma proposta como pende" +
                    "nte\r\n\tPara que possa analisar outras propostas enquanto as minhas dúvidas não sã" +
                    "o sanadas", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 8
#line 9
 testRunner.Given("que estou logado no sistema", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 10
 testRunner.And("clico em \"Onboarding\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 11
 testRunner.And("clico em \"Pessoa física\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 12
 testRunner.And("clico em \"Analisar\" de uma proposta", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Alterar a situação da proposta para \"Pendente\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void AlterarASituacaoDaPropostaParaPendente()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Alterar a situação da proposta para \"Pendente\"", null, new string[] {
                        "automatizar"});
#line 15
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 16
 testRunner.When("preencho o campo \"Observação\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 17
 testRunner.And("clico em \"Pendente\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 18
 testRunner.Then("o sistema altera a situação da proposta para \"Pendente\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 19
 testRunner.And("me retorna para a tela de listagem das propostas", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 20
 testRunner.And("apresenta a mensagem \"Situação alterada para: Pendente.\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 21
 testRunner.And("mantêm a \"Data/Hora início da análise\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 22
 testRunner.And("me mantem como \"Responsável\" por analisar a proposta", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 23
 testRunner.And("o sistema grava o log \"Alteração de situação\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Observação não preenchida")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void ObservacaoNaoPreenchida()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Observação não preenchida", null, new string[] {
                        "automatizar"});
#line 26
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 27
 testRunner.When("não preencho o campo \"Observação\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 28
 testRunner.And("clico em \"Pendente\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 29
 testRunner.Then("o sistema apresenta a mensagem \"O preenchimento do campo \'Observação\' é obrigatór" +
                    "io.\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 30
 testRunner.And("não altera a situação da proposta para \"Pendente\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 31
 testRunner.And("me mantêm na tela \"Proposta\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Erro ao alterar a situação da proposta para \"Pendente\"")]
        [NUnit.Framework.CategoryAttribute("automatizar")]
        public virtual void ErroAoAlterarASituacaoDaPropostaParaPendente()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Erro ao alterar a situação da proposta para \"Pendente\"", null, new string[] {
                        "automatizar"});
#line 34
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 35
 testRunner.When("ocorre algum erro", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 36
 testRunner.And("o sistema não consegue alterar a situação da proposta para \"Pendente\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 37
 testRunner.Then("o sistema apresenta a mensagem \"Desculpe, ocorreu um erro ao alterar a situação d" +
                    "a proposta <Seed da proposta>. Por favor, tente novamente e caso o erro persista" +
                    " entre em contato com o suporte técnico.\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line 38
 testRunner.And("me mantêm na tela \"Proposta\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
