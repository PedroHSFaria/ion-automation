﻿#language: pt-br

Funcionalidade: Onboarding PF - Sair da proposta
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Coordenador BPO", "Analista" ou "BPO"
	Quero sair de uma proposta
	Para que possa analisar outra proposta ou para que possa liberar a proposta para que outro usuário possa analisá-la

Contexto: 
	Dado que estou logado no sistema
	E clico em Onboarding
	E clico em Pessoa física
	E clico em Analisar de uma proposta
	E clico em Cancelar

@automatizado
Cenario: Sair da proposta
	Quando clico em Sair da proposta
	Então o sistema altera a situação da proposta para a situação anterior a de Em análise
	E altera a Data/Hora início da análise para a data/hora da análise anterior a minha
	E altera o "Responsável" para o da análise anterior
	E me retorna para a tela de listagem das propostas
	E o sistema grava o log "Alteração de situação"

@automatizado
Cenario: Cancelar saída da proposta - Botão "Cancelar"
	Quando clico em Cancelar no modal
	Então o sistema cancela a minha saída da proposta 
	E me mantêm na tela "Proposta"

@automatizado
Cenario: Cancelar saída da proposta - Botão "Fechar modal"
	Quando clico em Fechar modal
	Então o sistema cancela a minha saída da proposta
	E me mantêm na tela "Proposta"