﻿#language: pt-br

Funcionalidade: Onboarding PF - Relatório do percentual das respostas
	Como um usuário do Cactus com um dos seguintes perfis de acesso: Administrador ou Coordenador BPO
	Quero poder extrair o relatório do percentual das respostas
	Para validar a efetividade das perguntas do formulário do BPO de PF

@automatizar
Esquema do Cenario: Exportar "Relatório do percentual das respostas"
	Dado que estou logado com o <perfil de acesso>	
	E clico em "Onboarding"
	E existem registros retornados na lista de propostas de acordo com os filtros aplicados
	Quando clico em "exportar para Excel"
	E clico em "Relatório do percentual das respostas"
	Então o sistema efetua o download do "Relatório do percentual das respostas" no formato ".xlsx", contendo as perguntas do formulário e o percentual das respostas de acordo com os filtros aplicados
	Exemplos: 
	| perfil de acesso |
	| Administrador    |
	| Coordenador BPO  |