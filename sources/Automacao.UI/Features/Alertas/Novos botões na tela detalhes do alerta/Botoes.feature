﻿#language: pt-br

Funcionalidade: Botões da tela "Detalhes do alerta"
	Como um usuário do Cactus com um dos seguintes perfis de acesso: "Administrador", "Analista", "Consultor" ou "Especial 1"
	Quero que os botões "Fraude", "Suspeita de fraude", "Não fraude", "Pendente", "Manutenção cadastral", "Desinteresse comercial", "Bloquear conta", "Desbloquear conta" e "Sair" sejam apresentados
	Para que possa tratar todos os alertas de um respectivo cliente de uma forma mais eficaz
	
Contexto:	
	Dado que estou logado no sistema
	E clico no menu "Alertas"

@automatizar
Cenario: Analisar alerta pendente de análise de um agrupamento de CPF/CNPJ que possui um único alerta pendente de análise
	Quando clico em "Analisar" do único alerta pendente de análise do agrupamento, ou seja, com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	Então me redireciona para a tela de detalhes do alerta
	E altera a situação do alerta para "Em análise"
	E apresenta a mensagem "Situação alterada para: “Em análise”. Você tem até às 23:59:59 de hoje para finalizar a análise desse alerta."
	E adiciona a "Data/Hora início do tratamento" no alerta
	E me coloca como o "Analista" responsável por tratar esse alerta

@automatizar
Cenario: Classificar alerta como "Fraude"
	Quando clico em "Fraude"
	E selecino o "Tipo de fraude"
	   

#	BOTÕES:
#
#		| BOTÃO                  | APRESENTADO SEMPRE | COMPORTAMENTO                                                    |
#		| Fraude                 | Sim                | Ao acionado o sistema deve apresentar a modal de confirmação "". |
#		| Suspeita de fraude     | Sim                | Ao acionado o sistema deve apresentar a modal de confirmação "". |
#		| Não fraude             | Sim                | Ao acionado o sistema deve apresentar a modal de confirmação "". |
#		| Pendente               | Sim                | Ao acionado o sistema deve apresentar a modal de confirmação "". |
#		| Manutenção cadastral   | Sim                | Ao acionado o sistema deve apresenta a modal de confirmação "".  |
#		| Desinteresse comercial | Sim                | Ao acionado o sistema deve apresentar a modal de confirmação "". |
#		| Bloquear conta         | Não                | Esse botão só deve ser apresentado se                            |
#		| Desbloquear conta      | Não                |
#



######################################## PROTÓTIPOS #########################################
#
#	Tela "Detalhes do alerta": https://xd.adobe.com/view/8e4a692c-cc10-4463-4cd2-86c42b9173dc-f87b/screen/19f1775f-d11f-4356-be78-282596d48de4/Detalhe-alerta/
#
################################ ESPECIFICAÇÃO DE CAMPOS ####################################		

#	BOTÃO "SAIR":
#		Esse botão deve ficar fixo em uma barra fixa na parte inferior tela, de tal forma que ao rolar a tela para baixo ou para cima, a barra será apresentada contendo esse botão.
#
#	FUNCIONALIDADE DO LOG "ONBOARDING PJ": 
#
#		EXEMPLO DO LOG DE "ALTERAÇÃO DE SITUAÇÃO": 
#
#			| NOME_DO_ATRIBUTO                                                                                                   | VALOR_ANTERIOR_A_TRANSACAO | VALOR_POSTERIOR_A_TRANSACAO |
#			| Produto                                                                                                            | Aplicativo                 | Aplicativo                  |
#			| Funcionalidade                                                                                                     | Onboarding PJ              | Onboarding PJ               |
#			| Transação                                                                                                          | Alteração de situação      | Alteração de situação       |
#			| Analista                                                                                                           | BSI90896 - José Silva      | BSI90896 - José Silva       |
#			| Data/Hora da transação                                                                                             | 30/09/2019 às 15:30:21     | 30/09/2019 às 15:30:21      |
#			| Seed                                                                                                               | 103986                     | 103986                      |
#			| Razão social                                                                                                       | Empresa A                  | Empresa A                   |
#			| CNPJ                                                                                                               | 09.163.687/0001-51         | 09.163.687/0001-51          |
#			| E-mail                                                                                                             | empresa@teste.com          | empresa@teste.com           |
#			| Data/Hora da abertura                                                                                              | 30/03/2018 às 14:49:23     | 30/03/2018 às 14:49:23      |
#			| Data/Hora início da análise                                                                                        | 30/03/2018 às 15:00:00     |                             |
#			| Data/Hora de conclusão                                                                                             |                            |                             |
#			| Situação                                                                                                           | Em análise                 | Aguardando análise          |
#			| FORMULÁRIO DA EMPRESA                                                                                              |                            |                             |
#			| 1. A empresa está ativa no bureau ou na Receita Federal?                                                           |                            |                             |
#			| 2. A razão social da proposta confere com a retornada pelo bureau ou Receita Federal?                              |                            |                             |
#			| 3. O endereço da proposta confere com algum retornado pelo bureau ou Receita Federal?                              |                            |                             |
#			| 4. O número do telefone da proposta confere com algum retornado pelo bureau ou Receita Federal?                    |                            |                             |
#			| 5. O domínio do e-mail pertence à empresa da proposta?                                                             |                            |                             |
#			| 6. O e-mail da proposta confere com algum retornado pelo bureau ou Receita Federal?                                |                            |                             |
#			| 7. A quantidade de sócios da proposta confere com a retornada pelo bureau ou Receita Federal?                      |                            |                             |
#			| 8. Os nomes dos sócios da proposta conferem com os retornados pelo bureau ou Receita Federal?                      |                            |                             |
#			| 9. O percentual de participação de cada sócio confere com o retornado pelo bureau ou Receita Federal?              |                            |                             |
#			| FORMULÁRIO DO SÓCIO QUE PREENCHEU A PROPOSTA                                                                       |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            |                             |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            |                             |
#			| 3. A foto do documento é válida?                                                                                   |                            |                             |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            |                             |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            |                             |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            |                             |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            |                             |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            |                             |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            |                             |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            |                             |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            |                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            |                             |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            |                             |
#			| 14. As perguntas do quiz foram exibidas?                                                                           |                            |                             |
#			| 15. O cliente acertou todas as perguntas do quiz?                                                                  |                            |                             |
#			| 16. O cliente errou mais de uma pergunta do quiz?                                                                  |                            |                             |
#			| 17. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            |                             |
#			| FORMULÁRIO DOS DEMAIS SÓCIOS DA PROPOSTA                                                                           |                            |                             |
#			| 1. Os dados da proposta conferem com o documento?                                                                  |                            |                             |
#			| 2. O CPF  que consta na proposta é o mesmo do documento?                                                           |                            |                             |
#			| 3. A foto do documento é válida?                                                                                   |                            |                             |
#			| 4. A UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau?                          |                            |                             |
#			| 5. O DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau?                        |                            |                             |
#			| 6. A UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau?                               |                            |                             |
#			| 7. A cidade do endereço da proposta confere com a cidade de algum endereço retornado pelo bureau?                  |                            |                             |
#			| 8. O número do telefone da proposta confere com algum retornado pelo bureau?                                       |                            |                             |
#			| 9. O endereço da proposta confere com algum retornado pelo bureau?                                                 |                            |                             |
#			| 10. O e-mail da proposta confere com algum retornado pelo bureau?                                                  |                            |                             |
#			| 11. A consulta do bureau retornou os dados do cliente?                                                             |                            |                             |
#			| 12. Foi possível fazer a validação do documento no site externo e o CPF está regular ou pendente de regularização? |                            |                             |
#			| 13. Os dados abaixo conferem com os dados do documento (foto)?                                                     |                            |                             |
#			| 14. De acordo com todas as informações referentes a essa proposta, na sua opinião, existe algum risco de fraude?   |                            |                             |
#			| Observação                                                                                                         |                            |                             |
#
#	*Atualizar o "Relatório dos logs" para incluir os novos logs.
#
######################################################################################