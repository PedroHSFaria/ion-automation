﻿#language: pt-br

Funcionalidade: Analisar todos os alertas do CPF ou CNPJ
	Como responsável pelo monitoramento de fraude do BS2
	Quero poder atribuir todos os alertas pendentes de análise de um titular para mim
	Para que possa tratar todos os alertas de um respectivo cliente de uma forma mais eficaz
	
Contexto:	
	Dado que eu acesso a página de login
	E que estou logado como Analista
	E clico no menu Alertas

@automatizadoProntoPraSubir
Cenario: Analisar alerta pendente de análise de um agrupamento de CPF/CNPJ
	Quando clico em "Analisar" do único alerta pendente de análise do agrupamento, ou seja, com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	Então me redireciona para a tela de detalhes do alerta
	E altera a situação do alerta para "Em análise"
	E apresenta a mensagem Situação alterada para: “Em análise”. Você tem até às 23:59:59 de hoje para finalizar a análise desse alerta.
	E adiciona a "Data/Hora início do tratamento" no alerta
	E me coloca como o "Analista" responsável por tratar esse alerta

@automatizar
Cenario: Analisar alerta pendente de análise de um agrupamento de CPF/CNPJ que possui mais de um alerta pendente de análise
	Quando clico em "Analisar" de um alerta pendente de análise do agrupamento, ou seja, com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	Então o sistema cria uma fila de alertas pendentes de análise 
	E me coloca como o "Analista" responsável por tratar esse alerta
	E altera a situação do alerta para "Em análise"
	E o primeiro alerta da fila será o que acionei o "Analisar"
	E os demais alertas devem ser ordenados da seguinte maneira
	| ORDENACAO             |
	| Gravidade             |
	| Data/Hora de abertura |
	| Situação              |
	E o sistema me redireciona para a tela de detalhes do primeiro alerta da fila
	E apresenta a mensagem "As situações dos alertas pendentes de análise do titular <Nome do titular> foram alteradas para: Em análise. Você tem até às 23:59:59 de hoje para finalizar a análise desses alertas."
	E adiciona a "Data/Hora início do tratamento" apenas no alerta atual

@automatizar
Cenario: Apresentar próximo alerta da fila
	Quando tratar o alerta atual com um dos seguintes status
	| STATUS     |
	| Fraude     |
	| Não fraude |
	| Pendente   |
	E existem mais alertas na fila
	Então o sistema me redireciona para a tela de detalhes do próximo alerta da fila
	E adiciona a "Data/Hora início do tratamento" no alerta atual

@automatizar
	Cenario: Tratar último alerta da fila
	Quando tratar o alerta atual com um dos seguintes status
	| STATUS     |
	| Fraude     |
	| Não fraude |
	| Pendente   |
	E não existem mais alertas na fila
	Então o sistema me redireciona para a tela "Alertas de fraudes" mantendo o filtro aplicado anteriormente, quando houver

@automatizar
Cenario: Não alterar a situação dos alertas já tratados
	Quando clico em "Analisar" de um alerta pendente de análise, ou seja, com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	E o titular possui alertas já tratados, ou seja, com um dos seguintes status
	| STATUS     |
	| Fraude     |
	| Não fraude |
	Então o sistema não altera a situação dos alertas já tratados 

@automatizar
Cenario: Analisar alerta já tratado
	Quando clico em "Analisar" de um alerta já tratado, ou seja, com um dos seguintes status
	| STATUS     |
	| Fraude     |
	| Não fraude |
	Então o sistema me redireciona para a tela de detalhes do alerta
	E altera a situação do alerta para "Em análise"
	E apresenta a mensagem "Situação alterada para: “Em análise”. Você tem até às 23:59:59 de hoje para finalizar a análise desse alerta."
	E altera a "Data/Hora início do tratamento" no alerta
	E limpa a "Data/Hora término do tratamento" no alerta
	E me coloca como o "Analista" responsável por tratar esse alerta
	E adiciona o comentário automático "Analista <”Login - Nome do analista” que alterou a situação do alerta já tratado> alterou a situação desse alerta que já havia sido tratado anteriormente como “<Situação anterior do alerta>” para “<Nova situação do alerta>”."
	E o autor do comentário é "Comentário automático"

@automatizadoProntoPraSUbir
Cenario: Sair dos alertas
	Quando clico em "Analisar" do único alerta pendente de análise do agrupamento, ou seja, com um dos seguintes status
	| STATUS             |
	| Aguardando análise |
	| Pendente           |
	E clico em Sair 
	Então o sistema apresenta a mensagem Tem certeza que deseja sair desse alerta?
	Quando clico em Sair do alerta
	E me redireciona para a tela "Alertas de fraudes" mantendo o filtro aplicado anteriormente, quando houver
	
################################ PROTÓTIPO ######################################
#
#	Mensagem apresentada ao clicar no botão "Analisar" de um alerta pendente de análise de um agrupamento que possui mais de um alerta pendente de análise: https://marvelapp.com/7e20jdb/screen/62173985
#
################################ OBSERVAÇÕES ###################################									
#
#	Que ao acionar o comando [Analisar] de um alerta pendente de análise (alertas com situação igual a “Aguardando análise” e “Pendente”) de um dos agrupamentos de CPF/CNPJ, que o sistema altere a situação de todos os alertas pendentes de análise do respectivo titular (CPF/CNPJ), inclusive o que acionei o comando [Analisar], para a situação “Em análise” e deve me colocar como o analista responsável por tratar todos esses alertas (Ex.: Situação = “Em análise” e Analista = “BSI21210 - José Silva”). 
#	
#	A situação dos alertas já tratados, ou seja, com situação igual a “Fraude” e “Não fraude” não deverá ser alterada ao acionar o comando [Analisar] de um alerta pendente de análise de um dos agrupamentos de CPF/CNPJ.  
#	
#	E quando o analista acionar o comando [Analisar] de um alerta já tratado como “Fraude” ou “Não fraude”, o sistema deve manter o comportamento atual do sistema, deve alterar apenas a situação do respectivo alerta já tratado para “Em análise”. 
#
#	O tratamento de alertas em lote permanecerá da mesma forma que é hoje, não sofrerá alterações. 
#
#	Quando acionar o comando [Analisar], o sistema deve criar uma fila de alertas pendentes de análise, sendo que o primeiro será o que acionei o comando [Analisar] e os demais alertas pendentes de análise devem ser ordenados na fila da seguinte forma: 
#
#		•	Gravidade: “Alta”, “Média”, “Baixa” e “Informação”, ou seja, do alerta que possui maior gravidade para o que possui menor gravidade; 
#		•	Data/Hora: Ordem decrescente.
#		•	Situação: “Aguardando análise”, “Em análise”, “Pendente”, “Não fraude” e “Fraude”.
#
#	Sendo assim, quando alterar a situação do alerta que estou analisando no momento para “Fraude”, “Não fraude” ou “Pendente”, o sistema deve me redirecionar para a tela de detalhes do próximo alerta da fila. 

#	Quando não houver mais alertas na fila, ou seja, estou analisando o último alerta pendente de análise da fila, o sistema deve me redirecionar para a tela de listagem dos alertas, a tela “Alertas de fraude”.  
#
#	O sistema deve incluir a data/hora de início do tratamento apenas no alerta que acionei o comando [Analisar], nos demais alertas pendentes de análise atribuídos a mim e colocados como “Em análise”, o sistema não deve preencher o campo <Data/Hora início do tratamento> até que seja redirecionado para as suas respectivas telas de detalhes. Sendo assim, o sistema só deve incluir a data/hora de início do tratamento quando for redirecionado para a tela de detalhes do respectivo alerta, para analisá-lo. 
#
#	Quando acionar o comando [Sair] do alerta que estou analisando no momento, o sistema deve me remover de analista responsável por tratar todos os alertas pendentes de análise do respectivo CPF/CNPJ, deve remover a data/hora de início do tratamento do alerta que estava analisando e deve alterar a situação do alerta para a situação que ele estava antes de ser colocado “Em análise”. 
#
#	O sistema deverá alterar a situação para “Em análise” e deverá me colocar como o analista responsável por tratar todos os alertas gerados para o respectivo CPF/CNPJ após ter acionado o comando [Analisar] de um alerta pendente de análise do agrupamentos do respectivo CPF/CNPJ.
#
################################ FIM DAS OBSERVAÇÕES ################################