﻿#language: pt-br

Funcionalidade: Dashboard
	Como usuário do sistema Cactus
	Eu quero visualizar o Dashboard de acordo com o meu perfil de acesso
	Para que não haja acessos a páginas e menus não autorizados

Contexto: 
	Dado que eu acesso a página de login

@automatizado
Esquema do Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso
	Quando eu preencho os campos com credenciais de usuário com múltiplos perfis
	E eu clico em Entrar
	E eu seleciono o perfil <perfil>
	Então o Dashboard é exibido conforme com as permissões de <perfil>

	Exemplos: 
	| perfil               |
	| Administrador        |
	| Analista             |
	| BPO                  |
	| Consultor            |
	| Coordenador BPO      |
	| Especial 1           |
	| Terceiro             |
	| Compliance           |
	| Analista Funcionário |

@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - Administrador
	Quando eu preencho os campos com credenciais de usuário Administrador
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de Administrador
	
@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - Analista
	Quando eu preencho os campos com credenciais de usuário Analista
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de Analista

@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - BPO
	Quando eu preencho os campos com credenciais de usuário BPO
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de BPO

@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - Consultor
	Quando eu preencho os campos com credenciais de usuário Consultor
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de Consultor

@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - Coordenador BPO
	Quando eu preencho os campos com credenciais de usuário Coordenador BPO
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de Coordenador BPO

@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - Especial 1
	Quando eu preencho os campos com credenciais de usuário Especial 1
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de Especial 1

@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - Terceiro
	Quando eu preencho os campos com credenciais de usuário Terceiro
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de Terceiro

@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - Compliance
	Quando eu preencho os campos com credenciais de usuário Compliance
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de Compliance

@automatizado
Cenário: Visualizar o Dashboard de acordo com o meu perfil de acesso - Analista Funcionário
	Quando eu preencho os campos com credenciais de usuário Analista Funcionario
	E eu clico em Entrar
	Então o Dashboard é exibido conforme com as permissões de Analista Funcionario