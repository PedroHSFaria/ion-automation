﻿#language: pt-br

Funcionalidade: ES183. Painel do titular - Manter Observações
	Como responsável pelo monitoramento de fraude do BS2
	Quero visualizar, incluir, editar e excluir observações em um item do checklist 
	Para que possa visualizar os itens do respectivo titular que já foram validados, para que não precise validar um mesmo item novamente

@automatizar
Cenario: Visualizar observação
	Dado que estou no painel de um titular
	Quando clico em "ver observações" de um item do checklist
	Então o sistema deve apresentar a área "Observação" contendo o campo "Observação"
	E a listagem das observações já inseridas, ordenada de forma decrescente pela "Data/Hora da inclusão da observação"
	
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
# CAMPOS DA "ÁREA DE INCLUSÃO/EDIÇÃO DE OBSERVAÇÃO":
#
#	| CAMPO                                                                   | TIPO     | TAMANHO_MINIMO | TAMANHO_MAXIMO |
#	| Observação                                                              | TextArea | 15 caracteres  | 500 caracteres |
#	| <Qtde. de caracteres inseridos no campo "Observação"> de 500 caracteres | String   | N/A            | N/A            |
#
# CAMPOS DA "ÁREA DE LISTAGEM DAS OBSERVAÇÕES":
#
#	| CAMPO                                      | TIPO     | TAMANHO_MINIMO | TAMANHO_MAXIMO |
#	| Usuário                                    | String   | N/A            | N/A            |
#	| Data/Hora da inclusão/edição do comentário | DateTime | N/A            | N/A            |
#	| Observação                                 | TextArea | 15 caracteres  | 500 caracteres |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################

@automatizar
Cenario: Adicionar observação
	Dado que estou na área "Observação" de um item do checklist
	Quando preencho o campo "Observação" com no mínimo 15 caracteres
	E clico em "Salvar"
	Então o sistema deve gravar e exibir a nova observação na lista das observações já inseridas
	E deve gravar o log de "Inclusão de observação"

@automatizar
Cenario: Editar observação
	Dado que inseri uma observação anteriormente
	Quando clico em "Editar" de uma obsrevção feita por mim
	Então o sistema deve apresentar o campo "Observação" habilitado para edição
	Quando edito a observação da forma desejada
	E clico em "Salvar"
	Então  sistema deve atualizar e exibir a observação na lista das observações já inseridas
	E deve apresentar da "Data/Hora de edição da observação" no local da "Data/Hora de inclusão da observação"
	E deve apresentar o termo "(editado)" na frente da "Data/Hora de edição da obserrvação" para indicar a alteração
	E deve gravar o log de "Alteração de observação"

@automatizar
Cenario: Cancelar edição de observação
	Dado que estou editando uma observação
	Quando clico em "Cancelar"
	Então o sistema não deve efetuar a atualização da observação
	E deve permanecer na área "Observação" com o campo "Observação" desabilitado para edição

@automatizar
Cenario: Excluir observação
	Dado que estou na área "Observação" de um item do checklist
	Quando clico em "Excluir" de uma observação feita por mim
	Então o sistema apresenta a modal "Excluir observação" com a mensagem "Tem certeza que deseja excluir sua observação? Esta ação não pode ser desfeita."
	Quando clico em "Excluir observação"
	Então o sistema deve efetuar a exclusão da observação
	E deve permanecer na área "Observação"
	E deve gravar o log de "Exclusão de observação"

@automatizar
Cenario: Cancelar exclusão de observação
	Dado que estou na modal "Excluir observação"
	Quando clico em "Cancelar"
	Então o sistema não deve efetuar a exclusão da observação
	E deve permanecer na área "Observação"

@automatizar
Cenario: Fechar modal "Excluir observação"
	Dado que estou na modal "Excluir observação"
	Quando clico em "Fechar modal"
	Então o sistema não deve efetuar a exclusão da observação
	E deve permanecer na área "Observação"

@automatizar
Cenario: Quantidade de caracteres informados maior do que o permitido
	Dado que estou inserindo ou editando uma observação
	Quando tento inserir mais de 500 caracteres no campo "Observação"
	Então o sistema deve bloquear a inserção, não permitindo inserir mais de 500 caracteres

@automatizar
Cenario: Quantidade de caracteres informados menor do que o permitido
	Dado que estou inserindo ou editando uma observação
	Quando informo menos de 15 caracteres no campo "Observação"
	E clico em "Salvar"
	Então o sistema deve apresentar a mensagem "O campo ‘Observação’ deve conter no mínimo 15 caracteres." abaixo do campo <Observação>

@automatizar
Cenario: Fechar área "Observação"
	Dado que estou na área "Observação"
	Quando clico em "Fechar modal"
	Então o sistema deve fechar a área "Observação"
	E deve permanecer no painel do respectivo titular

#
################################ PROTÓTIPO ######################################
#
#	Links "ver observações" em cada item do checklist: https://marvelapp.com/7e20jdb/screen/61910652
#	Área "Observação": https://marvelapp.com/7e20jdb/screen/61917861
#	Editar observação: https://marvelapp.com/7e20jdb/screen/61917860
#	Observação editada: https://marvelapp.com/7e20jdb/screen/61917862
#	Modal de confirmação de exclusão de observação: https://marvelapp.com/7e20jdb/screen/61917863
#
################################ EXEMPLOS DOS LOGS ###################################	
#
# EXEMPLO DO LOG DE "INCLUSÃO DE OBSERVAÇÃO": 
#
#		| NOME_DO_ATRIBUTO                    | VALOR_POSTERIOR_A_TRANSACAO     |
#		| Produto                             | Aplicativo                      |
#		| Funcionalidade                      | Painel do titular               |
#		| Transação                           | Inclusão de observação          |
#		| Analista                            | BSI90896 - José Silva           |
#		| Data/Hora da transação              | 30/09/2019 às 15:30:21          |
#		| Titular                             | Maria Silva                     |
#		| ID do titular                       | Dec21231-9e8f-4f22-8a63         |
#		| CPF/CNPJ                            | 046.146.356-36                  |
#		| Redes sociais validadas             | facebook, instagram             |
#		| Observação                          | Rede social validada e está ok. |
#		| Data/Hora de inclusão da observação | 30/09/2019 às 15:30:21          |
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE OBSERVAÇÃO": 
#
#		| NOME_DO_ATRIBUTO                  | VALOR_ANTERIOR _A_TRANSACAO         | VALOR_POSTERIOR_A_TRANSACAO     |
#		| Produto                           | Aplicativo                          | Aplicativo                      |
#		| Funcionalidade                    | Painel do titular                   | Painel do titular               |
#		| Transação                         | Alteração de observação             | Alteração de observação         |
#		| Analista                          | BSI90896 - José Silva               | BSI90896 - José Silva           |
#		| Data/Hora da transação            | 30/09/2019 às 15:30:21              | 30/09/2019 às 15:30:21          |
#		| Titular                           | Maria Silva                         | Maria Silva                     |
#		| ID do titular                     | Dec21231-9e8f-4f22-8a63             | Dec21231-9e8f-4f22-8a63         |
#		| CPF/CNPJ                          | 046.146.356-36                      | 046.146.356-36                  |
#		| Redes sociais validadas           | facebook, instagram                 | facebook, instagram             |
#		| Observação                        | Rede social validada e NAÕ está ok. | Rede social validada e está ok. |
#		| Data/Hora de edição da observação |                                     | 30/09/2019 às 15:30:21          |
#
# EXEMPLO DO LOG DE "EXCLUSÃO DE OBSERVAÇÃO": 
#
#		| NOME_DO_ATRIBUTO                    | VALOR_ANTERIOR_A_TRANSACAO      |
#		| Produto                             | Aplicativo                      |
#		| Funcionalidade                      | Painel do titular               |
#		| Transação                           | Exclusão de observação          |
#		| Analista                            | BSI90896 - José Silva           |
#		| Data/Hora da transação              | 30/09/2019 às 15:30:21          |
#		| Titular                             | Maria Silva                     |
#		| ID do titular                       | Dec21231-9e8f-4f22-8a63         |
#		| CPF/CNPJ                            | 046.146.356-36                  |
#		| Redes sociais validadas             | facebook, instagram             |
#		| Observação                          | Rede social validada e está ok. |
#		| Data/Hora de exclusão da observação | 30/09/2019 às 15:30:21          |
#
################################ FIM DOS EXEMPLOS DOS LOGS ################################