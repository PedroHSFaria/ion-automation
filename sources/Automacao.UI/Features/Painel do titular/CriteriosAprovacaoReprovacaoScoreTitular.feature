﻿#language: pt-br

Funcionalidade: ES183. Painel do titular - Critérios de Aprovação/Reprovação e Score do Titular
	Como responsável pelo monitoramento de fraude do BS2
	Quero que os titulares do BS2 tenham um painel de informações com sugestões de aprovação ou reprovação de abertura de conta e o score do titular, de acordo com as opções marcadas nos itens do checklist
	Para que possa auxiliar os analistas nas tomadas de decisões das aberturas de contas que passam pela análise manual 

Contexto:	
	Dado que estou no painel de um titular 

@automatizar
Cenario: Não apresentar sugestão de aprovar/reprovar abertura da conta
	Dado que nenhuma das opções dos itens do checklist foram marcadas
	Quando acesso a seção "Checklist"
	Então o sistema deve apresentar o score "0"
	E não deve apresentar as mensagens "APROVAR ABERTURA DA CONTA" e "REPROVAR ABERTURA DA CONTA"

@automatizar
Cenario: Apresentar sugestão aprovar abertura da conta
	Dado que as opções marcadas nos itens do checklist calcula um score que se encaixa na faixa de "aprovar abertura da conta"
	Quando acesso a seção "Checklist"
	Então o sistema deve apresentar o score do titular calculado de acordo com os pesos das opções marcadas 
	E deve apresentar a mensagem "APROVAR ABERTURA DA CONTA"

@automatizar
Cenario: Apresentar sugestão reprovar abertura da conta
	Dado que as opções marcadas nos itens do checklist calcula um score que se encaixa na faixa de "reprovar abertura da conta"
	Quando acesso a seção "Checklist"
	Então o sistema deve apresentar o score do titular calculado de acordo com os pesos das opções marcadas 
	E deve apresentar a mensagem "REPROVAR ABERTURA DA CONTA"

#
################################ PROTÓTIPO ######################################
#
#	Sem sugestão aprovar/reprovar abertura da conta: https://marvelapp.com/7e20jdb/screen/61910652
#	Sugestão “Aprovar abertura da conta”: https://marvelapp.com/7e20jdb/screen/61910653
#	Sugestão “Reprovar abertura da conta”: https://marvelapp.com/7e20jdb/screen/61910654
#
################################ FAIXAS DO SCORE DO TITULAR ###################################	
#
#		| Faixa      | Sugestão                   |
#		| 701 à 1000 | APROVAR ABERTURA DA CONTA  |
#		| 1 à 700    | REPROVAR ABERTURA DA CONTA |
#		| 0          | Nenhuma sugestão           |
#
#	Ajustar as faixas de acordo com o que o Carlos enviar
#
################################ FAIXAS DO SCORE DO TITULAR ################################


################################ PESOS DAS OPÇÕES PARA CÁLCULO DO SCORE DO TITULAR #########################################	
#
#	Observações: 
#
#		Redes sociais validadas:	
#			- Quando houver pelo menos uma rede social estiver marcada, considerar "Sim" no cálculo do score.
#			- Quando não houver rede social marcada, considerar "Não" no cálculo do score.
#
#		Quiz:	
#			- Quando a opção "aprovado" tiver selecionada, considerar "Sim" no cálculo do score.
#			- Quando a opção "reprovado" tiver selecionada, considerar "Não" no cálculo do score.
#			- Quando nenhuma opção tiver selecionada, considerar "N/A" no cálculo do score.
#
#	Aguardando o Carlos me enviar os pesos
#
################################ FIM DOS PESOS DAS OPÇÕES PARA CÁLCULO DO SCORE DO TITULAR ################################
