﻿#language: pt-br

Funcionalidade: ES183. Painel do titular - Itens do checklist
	Como responsável pelo monitoramento de fraude do BS2
	Quero marcar e desmarcar as opções dos itens do checklist 
	Para que possa visualizar os itens do respectivo titular que já foram validados, para que não precise validar um mesmo item novamente

@automatizar
Cenario: Acessar o painel do titular através de um alerta de fraude
	Dado que acessei o sistema
	Quando clico na opção "Alertas"
	Então o sistema apresenta a tela "Alertas de fraudes"
	Quando clico em "Analisar" de um dos alertas listados
	Então o sistema deve apresentar a tela "Detalhes do alerta"
	Quando clico em "Painel do titular"
	Então o sistema deve apresentar o painel do respectivo titular do alerta

@automatizar
Cenario: Acessar o painel do titular através da tela Informações dos titulares 
	Dado que acessei o sistema
	Quando clico em "Listas"
	E clico em "Informações dos titulares"
	Então o sistema apresenta a tela "Informações dos tituales"
	Quando clico em "Filtrar" sem informar os campos de filtros
	Então o sistema deve listar todos os titulares que já possuem um painel
	Quando clico em "Ver detalhes" de um dos titulares
	Então o sistema deve apresentar o painel do respectivo titular

@automatizar
Cenario: Itens do checklist do painel do titular
	Dado que estou no painel de um titular
	Quando acesso a seção "Checklist"
	Então o sistema deve apresentar a lista de itens do checklist ordenada de forma alfabética 
	E deve apresentar as opções de cada item do checklist
	E o sistema não deve permitir marcar mais de uma opção no item do checklist, exceto no item "Redes sociais validadas"

#
################################ PROTÓTIPO ######################################
#
#	Sem sugestão aprovar/reprovar abertura da conta: https://marvelapp.com/7e20jdb/screen/61910652
#	Sugestão “Aprovar abertura da conta”: https://marvelapp.com/7e20jdb/screen/61910653
#	Sugestão “Reprovar abertura da conta”: https://marvelapp.com/7e20jdb/screen/61910654
#
################################ ITENS E OPÇÕES DO CHECKLIST ###################################	
#
#	| ITENS                                                              | OPCOES                                                     |
#	| Análise documentoscopia                                            | "sim" e "não"                                              |
#	| Cidade do endereço vinculada nos bureaux                           | "sim" e "não"                                              |
#	| Cliente validado em órgão de classe                                | "sim" e "não"                                              |
#	| DDD do telefone vinculado nos bureaux                              | "sim" e "não"                                              |
#	| Documento validado em site oficial                                 | "sim" e "não"                                              |
#	| E-mail vinculado nos bureaux                                       | "sim" e "não"                                              |
#	| Empresa ativa vinculada                                            | "sim" e "não"                                              |
#	| Endereço vinculado nos bureaux                                     | "sim" e "não"                                              |
#	| Face Match                                                         | "sim" e "não"                                              |
#	| IP                                                                 | "sim" e "não"                                              |
#	| N.º do RG validado nos bureaux                                     | "sim" e "não"                                              |
#	| Quiz                                                               | "aprovado" e "reprovado"                                   |
#	| Redes sociais validadas                                            | "facebook", "whatsapp", "linkedin", "instagram" e "outras" |
#	| Riskband Emailage                                                  | "1", "2", "3", "4", "5" e "6"                              |
#	| Score DataValid                                                    | "alto", "altíssimo", "baixo" e "baixíssimo"                |
#	| Telefone vinculado nos bureaux                                     | "sim" e "não"                                              |
#	| UF do endereço confere com a UF de algum DDD vinculado nos bureaux | "sim" e "não"                                              |
#	| UF do endereço vinculada nos bureaux                               | "sim" e "não"                                              |
#	| Vínculo empregatício validado nos bureaux                          | "sim" e "não"                                              |
#
################################ FIM DOS ITENS E OPÇÕES DO CHECKLIST ################################

@automatizar
Cenario: Marcar opção de um item do checklist
	Dado que uma opção de um item do checklist está desmarcada
	Quando marco essa opção
	Então o sistema deve apresentá-la destacada
	E o sistema deve gravar o log de "Alteração de item do checklist"

################################ CORES DAS OPÇÕES DOS ITENS DO CHECKLIST ###################################	
#
#	| OPCOES     | COR DA OPÇÃO DESMARCADA | COR DA OPÇÃO MARCADA |
#	| sim        | cinza                   | verde                |
#	| não        | cinza                   | rosa                 |
#	| aprovado   | cinza                   | verde                |
#	| reprovado  | cinza                   | rosa                 |
#	| facebook   | cinza                   | verde                |
#	| whatsapp   | cinza                   | verde                |
#	| linkedin   | cinza                   | verde                |
#	| instagram  | cinza                   | verde                |
#	| outras     | cinza                   | verde                |
#	| 1          | cinza                   | verde                |
#	| 2          | cinza                   | verde                |
#	| 3          | cinza                   | rosa                 |
#	| 4          | cinza                   | rosa                 |
#	| 5          | cinza                   | rosa                 |
#	| 6          | cinza                   | rosa                 |
#	| alto       | cinza                   | verde                |
#	| altíssimo  | cinza                   | verde                |
#	| baixo      | cinza                   | rosa                 |
#	| baixíssimo | cinza                   | rosa                 |
#
################################ FIM DAS OPÇÕES DOS ITENS DO CHECKLIST ################################

@automatizar
Cenario: Desmarcar opção de um item do checklist
	Dado que uma opção de um item do checklist está marcada, ou seja, está destacada na cor verde ou rosa
	Quando desmarco essa opção
	Então o sistema deve apresentá-la destacada em cinza 
	E o sistema deve gravar o log de "Alteração de item do checklist"

@automatizar
Cenario: Checar automaticamente opções dos itens do checklist
	Quando um titular possui proposta com status "Concluída" na tela "Onboarding"
	E atende as condições que permite marcar automaticamente algumas opções dos itens do checklist
	Então o sistema deve marcar as opções automaticamente
	
################################ REGRAS PARA CHECAR AS OPÇÕES AUTOMATICAMENTE #########################################	
#
#	| ITEM                                                               | OPCAO     | MARCAR_AUTOMATICAMENTE                                                                                                                 |
#	| Cidade do endereço vinculada nos bureaux                           | sim       | Quando a cidade do endereço da proposta confere com as cidades dos endereços retornados do bureau na seção "Cidades dos endereços"     |
#	| Cidade do endereço vinculada nos bureaux                           | não       | Quando a cidade do endereço da proposta NÃO confere com as cidades dos endereços retornados do bureau na seção "Cidades dos endereços" |
#	| DDD do telefone vinculado nos bureaux                              | sim       | Quando o DDD do telefone da proposta confere com o DDD de algum telefone retornado pelo bureau na seção "DDDs dos telefones"           |
#	| DDD do telefone vinculado nos bureaux                              | não       | Quando o DDD do telefone da proposta NÃO confere com o DDD de algum telefone retornado pelo bureau na seção "DDDs dos telefones"       |
#	| E-mail vinculado nos bureaux                                       | sim       | Quando o e-mail da proposta confere com algum retornado pelo bureau na seção "E-mails"                                                 |
#	| E-mail vinculado nos bureaux                                       | não       | Quando o e-mail da proposta NÃO confere com algum retornado pelo bureau na seção "E-mails"                                             |
#	| Endereço vinculado nos bureaux                                     | sim       | Quando a pergunta 10 foi respondida com a opção "Sim"                                                                                  |
#	| Endereço vinculado nos bureaux                                     | não       | Quando a pergunta 10 NÃO foi respondida com a opção "Sim"                                                                              |
#	| Quiz                                                               | aprovado  | Quando o titular tiver acertado no mínimo 4 perguntas do Quiz                                                                          |
#	| Quiz                                                               | reprovado | Quando o titular NÃO tiver acertado no mínimo 4 perguntas do Quiz, apenas quando o quiz tiver sido apresentado                         |
#	| Telefone vinculado nos bureaux                                     | sim       | Quando o telefone da proposta confere com algum retornado pelo bureau na seção "Telefones"                                             |
#	| Telefone vinculado nos bureaux                                     | não       | Quando o telefone da proposta NÃO confere com algum retornado pelo bureau na seção "Telefones"                                         |
#	| UF do endereço confere com a UF de algum DDD vinculado nos bureaux | sim       | Quando a UF do endereço da proposta confere com a UF de algum DDD retornado pelo bureau na seção "UF do endereço x UFs dos DDDs"       |
#	| UF do endereço confere com a UF de algum DDD vinculado nos bureaux | não       | Quando a UF do endereço da proposta NÃO confere com a UF de algum DDD retornado pelo bureau na seção "UF do endereço x UFs dos DDDs"   |
#	| UF do endereço vinculada nos bureaux                               | sim       | Quando a UF do endereço da proposta confere com a UF de algum endereço retornado pelo bureau na seção "UFs dos endereços"              |
#	| UF do endereço vinculada nos bureaux                               | não       | Quando a UF do endereço da proposta NÃO confere com a UF de algum endereço retornado pelo bureau na seção "UFs dos endereços"          |
#
################################ FIM DAS REGRAS PARA CHECAR AS OPÇÕES AUTOMATICAMENTE ################################

################################ EXEMPLOS DOS LOGS ###################################	
#
# EXEMPLO DO LOG DE "ALTERAÇÃO DE ITEM DO CHECKLIST": 
#
#		| NOME_DO_ATRIBUTO        | VALOR_ANTERIOR_A_TRANSACAO     | VALOR_POSTERIOR_A_TRANSACAO    |
#		| Produto                 | Aplicativo                     | Aplicativo                     |
#		| Funcionalidade          | Painel do titular              | Painel do titular              |
#		| Transação               | Alteração de item do checklist | Alteração de item do checklist |
#		| Analista                | BSI90896 - José Silva          | BSI90896 - José Silva          |
#		| Data/Hora da transação  | 30/09/2019 às 15:30:21         | 30/09/2019 às 15:30:21         |
#		| Titular                 | Maria Silva                    | Maria Silva                    |
#		| ID do titular           | Dec21231-9e8f-4f22-8a63        | Dec21231-9e8f-4f22-8a63        |
#		| CPF/CNPJ                | 046.146.356-36                 | 046.146.356-36                 |
#		| Redes sociais validadas | facebook                       | facebook, instagram            |
#
################################ FIM DOS EXEMPLOS DOS LOGS ################################