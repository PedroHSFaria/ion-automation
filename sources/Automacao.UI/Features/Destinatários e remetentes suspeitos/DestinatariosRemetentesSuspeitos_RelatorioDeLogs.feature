﻿#language: pt-br

Funcionalidade: Destinatários e remetentes suspeitos - Relatório de regra
	Como responsável pelas parametrizações dos alertas de fraudes do BS2
	Quero poder extrair o relatório de log
	Para saber quando um destinatário/remetente suspeito foi incluído, editado ou excluído
	
Contexto:	
	Dado que estou logado como "Administrador"
	E clico no menu Listas
	E clico no submenu Destinatários e remetentes suspeitos

@automatizadoProblemaTimeOut
Cenario: Relatório de log de inclusão de destinatário/remetente suspeito
	Quando clico em Adicionar
	E preencho os campos de adicao
	E clico em Salvar
	E clico no menu Logs
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em Filtrar logs
	E aciono o comando exportar para Excel
#	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs de inclusão de destinatários/remetentes suspeitos
	E me mantêm na tela Logs

@automatizadoProblemaTimeOut
Cenario: Relatório de log de alteração de destinatário/remetente suspeito
	Quando clico em Editar
	E edito os campos
	E clico em Salvar alteracao
	E clico no menu Logs
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em Filtrar logs
	E aciono o comando exportar para Excel
#	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs de alteração de destinatários/remetentes suspeitos
	E me mantêm na tela Logs

@automatizadoProblemaTimeOut
Cenario: Relatório de log de exclusão de destinatário/remetente suspeito
	Quando clico em Excluir
	E clico em Excluir destinatário/remetente
	E clico no menu Logs exclusao
	E seleciono "Data/Hora" menor ou igual a 31 dias
	E clico em Filtrar logs
	E aciono o comando exportar para Excel
#	Então o sistema efetua o download do relatório de logs no formato ".xls", contendo os logs de exclusão de destinatários/remetentes suspeitos
	E me mantêm na tela Logs
	
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
#	FUNCIONALIDADE DO LOG "DESTINATÁRIOS E REMETENTES SUSPEITOS": 
#
#		EXEMPLO DO LOG DE "INCLUSÃO DE DESTINATÁRIO/REMETENTE SUSPEITO": 
#
#			| NOME_DO_ATRIBUTO       | VALOR_POSTERIOR_A_TRANSACAO                 |
#			| Produto                | Aplicativo                                  |
#			| Funcionalidade         | Destinatários e remetentes suspeitos        |
#			| Transação              | Inclusão de destinatário/remetente suspeito |
#			| Analista               | BSI90896 - José Silva                       |
#			| Data/Hora da transação | 30/09/2019 às 15:30:21                      |
#			| Titular                | José da Silva                               |
#			| CPF/CNPJ               | 875.654.985-84                              |
#			| Agência                | 0001                                        |
#			| N.º da conta           | 254786-9                                    |
#
#		EXEMPLO DO LOG DE "ALTERAÇÃO DE DESTINATÁRIO/REMETENTE SUSPEITO": 
#
#			| NOME_DO_ATRIBUTO       | VALOR_ANTERIOR_A_TRANSACAO                   | VALOR_POSTERIOR_A_TRANSACAO                  |
#			| Produto                | Aplicativo                                   | Aplicativo                                   |
#			| Funcionalidade         | Destinatários e remetentes suspeitos         | Destinatários e remetentes suspeitos         |
#			| Transação              | Alteração de destinatário/remetente suspeito | Alteração de destinatário/remetente suspeito |
#			| Analista               | BSI90896 - José Silva                        | BSI90896 - José Silva                        |
#			| Data/Hora da transação | 30/09/2019 às 15:30:21                       | 30/09/2019 às 15:30:21                       |
#			| Titular                | José da Silva                                | José da Silva                                |
#			| CPF/CNPJ               | 875.654.985-84                               | 875.654.985-84                               |
#			| Agência                | 0001                                         | 0001                                         |
#			| N.º da conta           | 254786-9                                     | 254786-9                                     |
#
#		EXEMPLO DO LOG DE "EXCLUSÃO DE DESTINATÁRIO/REMETENTE SUSPEITO": 
#
#			| NOME_DO_ATRIBUTO       | VALOR_ANTERIOR_A_TRANSACAO                  | VALOR_POSTERIOR_A_TRANSACAO                 |
#			| Produto                | Aplicativo                                  | Aplicativo                                  |
#			| Funcionalidade         | Destinatários e remetentes suspeitos        | Destinatários e remetentes suspeitos        |
#			| Transação              | Exclusão de destinatário/remetente suspeito | Exclusão de destinatário/remetente suspeito |
#			| Analista               | BSI90896 - José Silva                       | BSI90896 - José Silva                       |
#			| Data/Hora da transação | 30/09/2019 às 15:30:21                      | 30/09/2019 às 15:30:21                      |
#			| Titular                | José da Silva                               | José da Silva                               |
#			| CPF/CNPJ               | 875.654.985-84                              | 875.654.985-84                              |
#			| Agência                | 0001                                        | 0001                                        |
#			| N.º da conta           | 254786-9                                    | 254786-9                                    |
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################