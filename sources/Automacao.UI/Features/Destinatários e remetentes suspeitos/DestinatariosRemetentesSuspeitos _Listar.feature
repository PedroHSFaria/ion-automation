﻿#language: pt-br

Funcionalidade: Destinatários e remetentes suspeitos - Listar
	Como administrador do sistema
	Quero visualizar os destinatários/remetentes suspeitos
	Para que possa ser alertado quando um cliente efetuar ou receber transações dos destinatários/remetentes suspeitos

Contexto: 
	Dado que estou logado como Administrador
	E clico em Listas
	E clico em Destinatários e remetentes suspeitos
	E o sistema apresenta a tela Destinatários e rementes suspeitos e lista os destinatários/remetentes já cadastrados

@ProntoProblemaBD
Cenario: Filtrar destinatários/remetentes suspeitos
	Quando preencho os campos de filtros com os valores desejados
	| CAMPOS DE FILTRO | VALOR                  |
	| Titular          | José da Silva          | 
	| CPFouCNPJ        | 57296099062			|
	| Agência          | 002		            |
	| Número da conta  | 2547861			    |
	E clico em Filtrar
    Então o sistema recupera e apresenta os destinatários/remetentes de acordo com os filtros aplicados

@automatizado
Cenario: Limpar filtro
	Dado preencho os campos de filtros com os valores desejados
	| CAMPOS DE FILTRO | VALOR                  |
	| Titular          | José da Silva          | 
	| CPFouCNPJ        | 572.960.990-62         |
	| Agência          | 002		            |
	| Número da conta  | 254786-1			    |
	E clico em Filtrar
	Quando clico em Limpar
#	Então o sistema limpa os filtros aplicados anteriormente

@automatizado
Cenario: Nenhum destinatário/remetente foi cadastrado
	Quando não há destinatário/remetente cadastrado no cactus
	Então o sistema apresenta a mensagem "Não existem destinatários/remetentes para serem listados no momento."

@ProntoProblemaBD
Cenario: Nenhum destinatário/remetente foi encontrado 
	Dado preencho os campos de filtros com os valores desejados.
	| CAMPOS DE FILTRO | VALOR                  |
	| Titular          | Teste                  |
	| CPFouCNPJ        | 99999999999            |
	| Agência          | 1                      |
	| Número da conta  | 11111				    |
	E clico em Filtrar
	Quando o sistema não encontra nenhum destinatário/remetente de acordo com os filtros aplicados
	Então o sistema apresenta a mensagem "Não existem destinatários/remetentes para serem listados no momento."

################################ PROTÓTIPO ######################################
#	https://marvelapp.com/7e20jdb/screen/62175366
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
#	CAMPOS DE FILTROS DA TELA "DESTINATÁRIOS E REMETENTES SUSPEITOS":
#
#		| CAMPO        | TIPO    | OBRIGATORIO | DESABILITADO | MASCARA                                          | PLACEHOLDER         | VALOR_INICIAL |
#		| Titular      | String  | Não         | Não          | N/A                                              | Nome do titular     | N/A           |
#		| CPF/CNPJ     | String  | Não         | Não          | 999.999.999-99 (CPF) / 99.999.999/9999-99 (CNPJ) | CPF/CNPJ do titular | N/A           |
#		| Agência      | Integer | Não         | Não          | 9999                                             | Agência da conta    | N/A           |
#		| N.º da conta | String  | Não         | Não          | 9999999-9                                        | Número da conta     | N/A           |
#
#	COLUNAS DA TELA "DESTINATÁRIOS E REMETENTES SUSPEITOS":
#		
#		| COLUNA       | TIPO    | MASCARA                                          |  
#		| Titular      | String  | N/A                                              |
#		| CPF/CNPJ     | String  | 999.999.999-99 (CPF) / 99.999.999/9999-99 (CNPJ) |
#		| Agência      | Integer | 9999                                             |
#		| N.º da conta | String  | 9999999-9                                        |
#	
#	ORDENAÇÃO DA LISTA DE DESTINATÁRIOS/REMETENTES SUSPEITOS:
#
#		| ORDENACAO                   |
#		| Titular: Ordem alfabética   |
#		| CPF/CNPJ: Ordem crescente   |
#
#	TOOLTIPS:
#
#		| BOTAO                    | TOOLTIP   |
#		| Adicionar (ícone "+")    | Adicionar |
#		| Editar (ícone "lápis"    | Editar    |
#		| Excluir (ícone "lixeira" | Excluir   |
#		| Fechar (ícone "x")       | Fechar    |
#
#	GRUPOS DE FUNCIONALIDADES NO AD:
#		
#		FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_VIEW_HML
#		FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_VIEW_PRD
#
#	Perfis de acessos atribuídos aos grupos de funcionalidades:
#
#		Grupo de funcionalidade "FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_VIEW_HML":
#			Perfis de acesso:
#				FRAUDPREV_ADMIN_HML
#				FRAUDPREV_CONSULTOR_HML
#
#		Grupo de funcionalidade "FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_VIEW_PRD":
#			Perfis de acesso:
#				FRAUDPREV_ADMIN_PRD
#				FRAUDPREV_CONSULTOR_PRD
#
################################ FIM ESPECIFICAÇÃO DE CAMPOS ################################