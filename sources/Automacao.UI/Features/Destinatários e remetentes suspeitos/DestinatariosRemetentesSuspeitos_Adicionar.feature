﻿#language: pt-br

Funcionalidade: Destinatários e remetentes suspeitos - Adicionar
	Como administrador do sistema
	Quero incluir destinatários/remetentes suspeitos
	Para que possa ser alertado quando um cliente efetuar ou receber transações dos destinatários/remetentes suspeitos

Contexto: 
	Dado que estou logado como "Administrador"
	E clico em opcao "Listas"
	E clico em opcao Destinatários e remetentes suspeitos
	E o sistema apresenta a tela Destinatários e rementes suspeitos 
#	E lista os destinatários/remetentes já cadastrados 
	Quando clico em Adicionar
	Então o sistema apresenta a área "Adicionar destinatário/remetente suspeito"

@automatizado
Cenario: Incluir destinatário/remetente suspeito
	Quando preencho os seguintes campos
	| CAMPOS       |
	| Titular      |
	| CPF/CNPJ     |
	| Agência      |
	| N.º da conta |
	| Comentario   |
	E clico em Salvar
	Entao apresenta a mensagem O destinatário/remetente foi adicionado com sucesso.
	E me redireciona para a tela "Destinatários e rementes suspeitos"
#	E o sistema adiciona o novo destinatário/remetente suspeito na lista
#	E grava o log de Inclusão de destinatário/remetente suspeito

@automatizado
Cenario: Cancelar inclusão de destinatário/remetente suspeito
	Quando clico em Cancelar.
	Então o sistema cancela a inclusão do novo destinatário/remetente suspeito na lista
	E me redireciona para a tela "Destinatários e rementes suspeitos"

@automatizado
Cenario: Fechar área "Adicionar destinatário/remetente suspeito"
	Quando clico em Fechar
	Então o sistema cancela a inclusão do novo destinatário/remetente suspeito na lista
	E me redireciona para a tela "Destinatários e rementes suspeitos"
 
################################ PROTÓTIPO ######################################
#
#	https://marvelapp.com/7e20jdb/screen/62175369
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
#	CAMPOS DA ÁREA "ADICIONAR DESTINATÁRIO/REMETENTE SUSPEITO"
#
#		| CAMPO        | TIPO    | OBRIGATORIO | DESABILITADO | MASCARA                                          | PLACEHOLDER         | VALOR_INICIAL |
#		| Titular      | String  | Sim         | Não          | N/A                                              | Nome do titular     | N/A           |
#		| CPF/CNPJ     | String  | Sim         | Não          | 999.999.999-99 (CPF) / 99.999.999/9999-99 (CNPJ) | CPF/CNPJ do titular | N/A           |
#		| Agência      | Integer | Não         | Não          | 9999                                             | Agência da conta    | N/A           |
#		| N.º da conta | String  | Não         | Não          | 9999999-9                                        | Número da conta     | N/A           |
#
#
#	FUNCIONALIDADE DO LOG "DESTINATÁRIOS E REMETENTES SUSPEITOS": 
#
#		EXEMPLO DO LOG DE "INCLUSÃO DE DESTINATÁRIO/REMETENTE SUSPEITO": 
#
#			| NOME_DO_ATRIBUTO       | VALOR_POSTERIOR_A_TRANSACAO                 |
#			| Produto                | Aplicativo                                  |
#			| Funcionalidade         | Destinatários e remetentes suspeitos        |
#			| Transação              | Inclusão de destinatário/remetente suspeito |
#			| Analista               | BSI90896 - José Silva                       |
#			| Data/Hora da transação | 30/09/2019 às 15:30:21                      |
#			| Titular                | José da Silva                               |
#			| CPF/CNPJ               | 875.654.985-84                              |
#			| Agência                | 0001                                        |
#			| N.º da conta           | 254786-9                                    |
#
#	GRUPOS DE FUNCIONALIDADES NO AD:
#		
#		FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_ADD_HML
#		FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_ADD_PRD
#
#	Perfis de acessos atribuídos aos grupos de funcionalidades:
#
#		Grupo de funcionalidade "FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_ADD_HML":
#			Perfis de acesso:
#				FRAUDPREV_ADMIN_HML
#		
#	Grupo de funcionalidade "FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_ADD_PRD":
#			Perfis de acesso:
#				FRAUDPREV_ADMIN_PRD
#				
################################# FIM ESPECIFICAÇÃO DE CAMPOS ################################