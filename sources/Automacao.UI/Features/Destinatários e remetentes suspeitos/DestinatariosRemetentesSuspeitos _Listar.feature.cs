﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace Automacao.UI.Features.DestinatariosERemetentesSuspeitos
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("Destinatários e remetentes suspeitos - Listar")]
    public partial class DestinatariosERemetentesSuspeitos_ListarFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "DestinatariosRemetentesSuspeitos _Listar.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("pt-br"), "Destinatários e remetentes suspeitos - Listar", "\tComo administrador do sistema\r\n\tQuero visualizar os destinatários/remetentes sus" +
                    "peitos\r\n\tPara que possa ser alertado quando um cliente efetuar ou receber transa" +
                    "ções dos destinatários/remetentes suspeitos", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 8
#line 9
 testRunner.Given("que estou logado como Administrador", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Dado ");
#line 10
 testRunner.And("clico em Listas", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 11
 testRunner.And("clico em Destinatários e remetentes suspeitos", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 12
 testRunner.And("o sistema apresenta a tela Destinatários e rementes suspeitos e lista os destinat" +
                    "ários/remetentes já cadastrados", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Filtrar destinatários/remetentes suspeitos")]
        [NUnit.Framework.CategoryAttribute("ProntoProblemaBD")]
        public virtual void FiltrarDestinatariosRemetentesSuspeitos()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Filtrar destinatários/remetentes suspeitos", null, new string[] {
                        "ProntoProblemaBD"});
#line 15
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line hidden
            TechTalk.SpecFlow.Table table13 = new TechTalk.SpecFlow.Table(new string[] {
                        "CAMPOS DE FILTRO",
                        "VALOR"});
            table13.AddRow(new string[] {
                        "Titular",
                        "José da Silva"});
            table13.AddRow(new string[] {
                        "CPFouCNPJ",
                        "57296099062"});
            table13.AddRow(new string[] {
                        "Agência",
                        "002"});
            table13.AddRow(new string[] {
                        "Número da conta",
                        "2547861"});
#line 16
 testRunner.When("preencho os campos de filtros com os valores desejados", ((string)(null)), table13, "Quando ");
#line 22
 testRunner.And("clico em Filtrar", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 23
    testRunner.Then("o sistema recupera e apresenta os destinatários/remetentes de acordo com os filtr" +
                    "os aplicados", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Limpar filtro")]
        [NUnit.Framework.CategoryAttribute("automatizado")]
        public virtual void LimparFiltro()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Limpar filtro", null, new string[] {
                        "automatizado"});
#line 26
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line hidden
            TechTalk.SpecFlow.Table table14 = new TechTalk.SpecFlow.Table(new string[] {
                        "CAMPOS DE FILTRO",
                        "VALOR"});
            table14.AddRow(new string[] {
                        "Titular",
                        "José da Silva"});
            table14.AddRow(new string[] {
                        "CPFouCNPJ",
                        "572.960.990-62"});
            table14.AddRow(new string[] {
                        "Agência",
                        "002"});
            table14.AddRow(new string[] {
                        "Número da conta",
                        "254786-1"});
#line 27
 testRunner.Given("preencho os campos de filtros com os valores desejados", ((string)(null)), table14, "Dado ");
#line 33
 testRunner.And("clico em Filtrar", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 34
 testRunner.When("clico em Limpar", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Nenhum destinatário/remetente foi cadastrado")]
        [NUnit.Framework.CategoryAttribute("automatizado")]
        public virtual void NenhumDestinatarioRemetenteFoiCadastrado()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Nenhum destinatário/remetente foi cadastrado", null, new string[] {
                        "automatizado"});
#line 38
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line 39
 testRunner.When("não há destinatário/remetente cadastrado no cactus", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 40
 testRunner.Then("o sistema apresenta a mensagem \"Não existem destinatários/remetentes para serem l" +
                    "istados no momento.\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Nenhum destinatário/remetente foi encontrado")]
        [NUnit.Framework.CategoryAttribute("ProntoProblemaBD")]
        public virtual void NenhumDestinatarioRemetenteFoiEncontrado()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Nenhum destinatário/remetente foi encontrado", null, new string[] {
                        "ProntoProblemaBD"});
#line 43
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 8
this.FeatureBackground();
#line hidden
            TechTalk.SpecFlow.Table table15 = new TechTalk.SpecFlow.Table(new string[] {
                        "CAMPOS DE FILTRO",
                        "VALOR"});
            table15.AddRow(new string[] {
                        "Titular",
                        "Teste"});
            table15.AddRow(new string[] {
                        "CPFouCNPJ",
                        "99999999999"});
            table15.AddRow(new string[] {
                        "Agência",
                        "1"});
            table15.AddRow(new string[] {
                        "Número da conta",
                        "11111"});
#line 44
 testRunner.Given("preencho os campos de filtros com os valores desejados.", ((string)(null)), table15, "Dado ");
#line 50
 testRunner.And("clico em Filtrar", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "E ");
#line 51
 testRunner.When("o sistema não encontra nenhum destinatário/remetente de acordo com os filtros apl" +
                    "icados", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Quando ");
#line 52
 testRunner.Then("o sistema apresenta a mensagem \"Não existem destinatários/remetentes para serem l" +
                    "istados no momento.\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Então ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
