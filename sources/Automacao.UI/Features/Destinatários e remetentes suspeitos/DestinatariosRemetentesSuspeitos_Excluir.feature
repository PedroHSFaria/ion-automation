﻿#language: pt-br

Funcionalidade: Destinatários e remetentes suspeitos - Excluir
	Como administrador do sistema
	Quero excluir destinatários/remetentes suspeitos
	Para que possa ser alertado quando um cliente efetuar ou receber transações dos destinatários/remetentes suspeitos

Contexto: 
	Dado que estou logado como "Administrador"
	E clico em Listas
	E clico em Destinatários e remetentes suspeitos
	E o sistema apresenta a tela Destinatários e rementes suspeitos
#	E lista os destinatários/remetentes já cadastrados 
	Quando clico em Excluir
	Então o sistema apresenta a área Excluir destinatário/remetente suspeito
	E apresenta a mensagem Tem certeza que excluir esse destinatário/remetente? Esta ação não pode ser desfeita.

@ProntoProblemaBD
Cenario: Excluir destinatário/remetente suspeito
	Quando clico em Excluir destinatário/remetente
#	Então o sistema exclui o destinatário/remetente suspeito da lista
	E me redireciona para a tela "Destinatários e rementes suspeitos"
#	E apresenta a mensagem O destinatário/remetente foi excluído com sucesso.
	E grava o log de Exclusão de destinatário/remetente suspeito 

@automatizado
Cenario: Cancelar exclusão de destinatário/remetente suspeito
	Quando clico em Cancelar rotina excluir
	Então o sistema cancela a exclusão do novo destinatário/remetente suspeito da lista
	E me redireciona para a tela "Destinatários e rementes suspeitos"

@automatizado
Cenario: Fechar área "Adicionar destinatário/remetente suspeito"
	Quando clico em Fechar rotina excluir
	Então o sistema cancela a exclusão do novo destinatário/remetente suspeito da lista
	E me redireciona para a tela "Destinatários e rementes suspeitos"

#
################################ PROTÓTIPO ######################################
#
#	https://marvelapp.com/7e20jdb/screen/62175381
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################									
#
#
#	FUNCIONALIDADE DO LOG "DESTINATÁRIOS E REMETENTES SUSPEITOS": 
#
#	EXEMPLO DO LOG DE "EXCLUSÃO DE DESTINATÁRIO/REMETENTE SUSPEITO": 
#
#		| NOME_DO_ATRIBUTO       | VALOR_ANTERIOR_A_TRANSACAO                  | VALOR_POSTERIOR_A_TRANSACAO                 |
#		| Produto                | Aplicativo                                  | Aplicativo                                  |
#		| Funcionalidade         | Destinatários e remetentes suspeitos        | Destinatários e remetentes suspeitos        |
#		| Transação              | Exclusão de destinatário/remetente suspeito | Exclusão de destinatário/remetente suspeito |
#		| Analista               | BSI90896 - José Silva                       | BSI90896 - José Silva                       |
#		| Data/Hora da transação | 30/09/2019 às 15:30:21                      | 30/09/2019 às 15:30:21                      |
#		| Titular                | José da Silva                               | José da Silva                               |
#		| CPF/CNPJ               | 875.654.985-84                              | 875.654.985-84                              |
#		| Agência                | 0001                                        | 0001                                        |
#		| N.º da conta           | 254786-9                                    | 254786-9                                    |
#
#	GRUPOS DE FUNCIONALIDADES NO AD:
#		
#		FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_DELETE_HML
#		FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_DELETE_PRD
#
#	Perfis de acessos atribuídos aos grupos de funcionalidades:
#
#		Grupo de funcionalidade "FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_DELETE_HML":
#			Perfis de acesso:
#				FRAUDPREV_ADMIN_HML
#		
#		Grupo de funcionalidade "FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_DELETE_PRD":
#			Perfis de acesso:
#				FRAUDPREV_ADMIN_PRD
#
################################# FIM ESPECIFICAÇÃO DE CAMPOS ################################