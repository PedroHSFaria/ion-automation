﻿#language: pt-br

Funcionalidade: Destinatários e remetentes suspeitos - Editar
	Como administrador do sistema
	Quero editar destinatários/remetentes suspeitos
	Para que possa ser alertado quando um cliente efetuar ou receber transações dos destinatários/remetentes suspeitos

Contexto: 
	Dado que estou logado como "Administrador"
	E clico em Listas
	E clico em Destinatários e remetentes suspeitos
	E o sistema apresenta a tela Destinatários e rementes suspeitos
#	E lista os destinatários/remetentes já cadastrados 
	Quando clico em Editar
	Então o sistema apresenta a área Editar destinatário/remetente suspeito

@automatizadoProntoPraSubir
Cenario: Editar destinatário/remetente suspeito
	Quando edito os seguintes campos
	| Campos de Filtro | Valor          |
	| Titular          | José da Silva  |
	| CPFouCNPJ        | 572.960.990-62 |
	| Agência          | 002            |
	| nConta           | 254786-1       |
	| Comentario       | Testes55       |
	E clico em Salvar em editar
	E apresenta a mensagem O destinatário/remetente foi editado com sucesso.
	E me redireciona para a tela "Destinatários e rementes suspeitos"
#	E grava o log de Alteração de destinatário/remetente suspeito 

@automatizadoProntoPraSubir
Cenario: Cancelar edição de destinatário/remetente suspeito
	Quando clico em Cancelar edicao
	Então o sistema cancela a edição do destinatário/remetente suspeito
	E me redireciona para a tela Destinatários e rementes suspeitos

@automatizado
Cenario: Fechar área "Adicionar destinatário/remetente suspeito"
	Quando clico em Fechar area remetente suspeito
	Então o sistema cancela a edição do destinatário/remetente suspeito 
#E me redireciona para a tela Destinatários e rementes suspeitos
#
################################ PROTÓTIPO ######################################
#
#	https://marvelapp.com/7e20jdb/screen/62175376
#
################################ ESPECIFICAÇÃO DE CAMPOS ###################################
#
#	CAMPOS DA ÁREA "ADICIONAR DESTINATÁRIO/REMETENTE SUSPEITO"
#
#		| CAMPO        | TIPO    | OBRIGATORIO | DESABILITADO | MASCARA                                          | PLACEHOLDER         | VALOR_INICIAL |
#		| Titular      | String  | Sim         | Não          | N/A                                              | Nome do titular     | N/A           |
#		| CPF/CNPJ     | String  | Sim         | Não          | 999.999.999-99 (CPF) / 99.999.999/9999-99 (CNPJ) | CPF/CNPJ do titular | N/A           |
#		| Agência      | Integer | Não         | Não          | 9999                                             | Agência da conta    | N/A           |
#		| N.º da conta | String  | Não         | Não          | 9999999-9                                        | Número da conta     | N/A           |
#
#
#	FUNCIONALIDADE DO LOG "DESTINATÁRIOS E REMETENTES SUSPEITOS": 
#
#		EXEMPLO DO LOG DE "ALTERAÇÃO DE DESTINATÁRIO/REMETENTE SUSPEITO": 
#
#			| NOME_DO_ATRIBUTO       | VALOR_ANTERIOR_A_TRANSACAO                   | VALOR_POSTERIOR_A_TRANSACAO                  |
#			| Produto                | Aplicativo                                   | Aplicativo                                   |
#			| Funcionalidade         | Destinatários e remetentes suspeitos         | Destinatários e remetentes suspeitos         |
#			| Transação              | Alteração de destinatário/remetente suspeito | Alteração de destinatário/remetente suspeito |
#			| Analista               | BSI90896 - José Silva                        | BSI90896 - José Silva                        |
#			| Data/Hora da transação | 30/09/2019 às 15:30:21                       | 30/09/2019 às 15:30:21                       |
#			| Titular                | José da Silva                                | José da Silva                                |
#			| CPF/CNPJ               | 875.654.985-84                               | 875.654.985-84                               |
#			| Agência                | 0001                                         | 0001                                         |
#			| N.º da conta           | 254786-9                                     | 254786-9                                     |
#
#	GRUPOS DE FUNCIONALIDADES NO AD:
#		
#		FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_EDIT_HML
#		FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_EDIT_PRD
#
#	Perfis de acessos atribuídos aos grupos de funcionalidades:
#
#		Grupo de funcionalidade "FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_EDIT_HML":
#			Perfis de acesso:
#				FRAUDPREV_ADMIN_HML
#		
#		Grupo de funcionalidade "FRAUDPREV_DESTINATARIOSREMETENTESSUSPEITOS_EDIT_PRD":
#			Perfis de acesso:
#				FRAUDPREV_ADMIN_PRD
#
################################# FIM ESPECIFICAÇÃO DE CAMPOS ################################