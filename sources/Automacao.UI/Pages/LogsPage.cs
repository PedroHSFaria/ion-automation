﻿using Automacao.UI.Bases;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automacao.UI.Pages
{
    public class LogsPage : PageBase
    {
        #region Mapeamentos Logss
        By ProdutoList = By.XPath("//span[@class='ng-arrow-wrapper'])[1]");
        By FuncionalidadeList = By.XPath("(//span[@class='ng-arrow-wrapper'])[2]");
        By TransacaoList = By.XPath("(//span[@class='ng-arrow-wrapper'])[3]");
        By AnalistaList = By.XPath("(//span[@class='ng-arrow-wrapper'])[4]");
        By Calendario = By.XPath("(//a[@class='input-group-addon glyphicon glyphicon-th'])");
        By AlertaField = By.Id("numero-alerta");
        By FitrarButton = By.XPath("//button[@btn='btnFilterSearch']");
        By LimparButton = By.Name("btnClearSearch");
        By ColunaLog = By.XPath("//td[text()= 'BSI98259 - Pedro Faria']");
        By AnalistaPedroList = By.XPath("//div[@class='ng-select-option-standart'][text()='BSI98259 - Pedro Faria']");
        By CalendarioDiaField = By.XPath("//span[@class='owl-dt-calendar-cell-content']");
        By MesAnoCalendariofield = By.XPath("(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[2]");
        By TituloLogsTitle = By.XPath("//h1[@class='margin-top-null']");
        By ExportarExcel = By.XPath("//button[@class='btn-excel']");
        By MensagemDestinatarioCadastrado = By.XPath("//div[@class='help-block margin-top']");
        By CancelarAdicaoButton = By.XPath("//button[@class='btn btn-default cancel']");
        By LogsMenu = By.Id("Logs-link");
        By PrimeiroRegistroDeLogs = By.XPath("//tr[@id='logList_0']//td[text()='Alteração de destinatário ou remetente suspeitos']");
        By colunaAnalista = By.XPath("//button[@class='btn-excel']");
        By BotaoCarregar = By.Id("loading-spinner-text");
        By PrimeiroRegistroAtivacaoRegra = By.XPath("(//tr//td[text()='Ativação de regra'])[1]");
        #endregion

        #region Mapeamento calendario
        By SelecionarCalendarioButton = By.XPath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][2]");
        By CancelarCalendarioButton = By.XPath("//button[@class='owl-dt-control owl-dt-control-button owl-dt-container-control-button'][1]");
        By SelecionarAnoCalendario = By.XPath("(//span[@class='owl-dt-control-content owl-dt-control-button-content'])[2]");
        By CalendarioArea = By.XPath("//div[@class='owl-dt-calendar-main']");
        By AreaMesCalendario = By.XPath("//div[@class='owl-dt-calendar-main']");
        By SetinhaAcimaMinuto = By.XPath("(//span[@class='owl-dt-control-button-content'])[1]");
        #endregion

        #region Actions
        public void  ClicarListaProdutos()
        {
            Click(ProdutoList);
        }
        public void ClicarListaFuncionalidade()
        {
            Click(FuncionalidadeList);
        }
        public void ClicarListaTransacao()
        {
            Click(TransacaoList);
        }
        public void ClicarListaAnalista()
        {
            Click(AnalistaList);
        }

        public void ClicarAlerta()
        {
            Click(AlertaField);
        }
        public void ClicarFiltrar()
        {
            Click(FitrarButton);
        }
        public void ClicarLimpar()
        {
            Click(LimparButton);
        }

        public void SelecionarExportarExcel()
        {
            ClickJavaScript(ExportarExcel);
        }

        public void SalvarArquivoExcel()
        {
            string localArquivo = @"\\10.218.19.204\Downloads\RelatorioLogsExcel.xls";

            File.Move(localArquivo, @"\\10.218.19.204\Downloads\RelatorioLogsExcel.xls");
        }

        #region Preenchendo Calendário
        public void ClicarCalendario()
        {
            WaitForElement(Calendario);
            Click(Calendario);
        }

        public By DiaCalendarioText(string dia)
        {
            return By.XPath("(//span[@class='owl-dt-calendar-cell-content'])[text()='" + dia + "']");
        }

        public By AnoCalendarioText(string ano)
        {
            return By.XPath("//span[@class='owl-dt-calendar-cell-content owl-dt-calendar-cell-today'][text()='"+ ano +"']");
        }

        public By MesCalendariotext(string mes)
        {
            return By.XPath("//span[@class='owl-dt-calendar-cell-content'][text()='" + mes + "']");
        }

        public void ClicarNoAnoSelecionado(string ano)
        {
            Click(AnoCalendarioText(ano));
        }

        public void ClicarNoMesSelecionado(string mes)
        {
            Click(MesCalendariotext(mes));
        }

        public void ClicarOpcaoAno()
        {
            Click(SelecionarAnoCalendario);
        }

        public void EsperarTituloPaginaLogs()
        {
            WaitForElement(TituloLogsTitle);
            Assert.IsTrue(driver.FindElement(TituloLogsTitle).Displayed);
        }

        public void ClicarDiaCalendario(string dia)
        {
            Click(DiaCalendarioText(dia));
        }

        public void ClicarSelecionarDataCalendario()
        {
            Click(SelecionarCalendarioButton);
        }
        public By SelecionarAnalista(string analista)
        {
            return By.XPath("//div[@class='ng-select-option-standart ng-star-inserted'][text()='" + analista +"']");
        }

        public void ClicarEmAnalistaSelecionado(string analista)
        {
            Click(SelecionarAnalista(analista));
        }

        public void VerificacaoSeExisteDestinatarioCadastrado()
        {
            if (driver.FindElement(MensagemDestinatarioCadastrado).Displayed)
            {
                Click(CancelarAdicaoButton);
            }
            else
            {
                Click(LogsMenu);
            }
        }
        public void VerificaSePrimeiroRegistroEAlteracao()
        {
            WaitForElement(TituloLogsTitle);
            WaitForElement(PrimeiroRegistroDeLogs);
            Assert.IsTrue(driver.FindElement(PrimeiroRegistroDeLogs).Displayed);
        }

        public void VerificaSeColunaAnalistaEstaSendoExibida()
        {
            WaitForElement(colunaAnalista);
        }

        public void VerificaSeBotaoCarregamentoFoiFechado()
        {
            if (driver.FindElement(BotaoCarregar).Displayed)
            {
                WaitForElement(ExportarExcel);
            }
            else
            {
            }
        }

        public void VerificaSeHaRegistroDeAtivacao()
        {
            ScrollToElementJavaScript(PrimeiroRegistroAtivacaoRegra);
            Assert.IsTrue(driver.FindElement(PrimeiroRegistroAtivacaoRegra).Displayed);
        }

      /*  public void VerificaSeCalendarioEstaAberto()
        {
            WaitForElement(CalendarioArea);
            Click(AreaMesCalendario);
        }  */
        #endregion
        #endregion
    }
}
