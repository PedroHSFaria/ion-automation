﻿using Automacao.UI.Bases;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Automacao.UI.Pages
{
    public class LoginPage : PageBase
    {
        #region Mapeamento

        #region Mapeamento - Tela de login
        By UserField = By.Id("usuario");
        By PasswordField = By.Id("senha");
        By EntrarButton = By.Id("btnLogin");
        public By MensagensDeErroMensagem(string mensagem)
        {
            return By.XPath("//div[text()[contains(.,'" + mensagem + "')]]");
        }
        #endregion Mapeamento - Tela de login

        #region Mapeamento - Modal de boas vindas
        By BoasVindasModal = By.Name("form-group-selection");
        By PerfilComboBox = By.XPath("//div[@class='ng-input']");
        By SairModalButton = By.XPath("//button[@class='btn btn-default']");
        By EntrarModalButton = By.XPath("//button[@class='btn btn-secondary']");
        public By PerfilSelecionadoCombobo(string perfil)
        {
            return By.XPath("//div[@class='ng-dropdown-panel-items scroll-host']//div[text()='" + perfil + "']");
        }
        #endregion Mapeamento - Modal de boas vindas

        #endregion

        #region Ações

        #region Ações - Tela de login
        public void PreencherUsuario(string usuario)
        {
            if (usuario.Equals("\"\""))
            {
                Clear(UserField);
            }
            else
            {
                SendKeys(UserField, usuario);
            }                 
        }

        public void PreencherSenha(string senha)
        {
            if (senha.Equals("\"\""))
            {
                Clear(PasswordField);
            }
            else
            {
                SendKeys(PasswordField, senha);
            }                    
        }

        public void ClicarEmEntrar()
        {
            Click(EntrarButton);
        }

        public void ExibeMensagensDeErro(string mensagem)
        {
            WaitForElement(MensagensDeErroMensagem(mensagem));
            Assert.IsTrue(driver.FindElement(MensagensDeErroMensagem(mensagem)).Displayed);
        }
        #endregion Ações - Tela de login

        #region Ações - Modal de boas vindas
        public void ExisteBoasVindasModal()
        {
            WaitForElement(BoasVindasModal);
            Assert.IsTrue(driver.FindElement(BoasVindasModal).Displayed);
        }        

        public void SelecionaPerfil(string perfil)
        {
            WaitForElement(BoasVindasModal);
            driver.FindElement(PerfilComboBox).Click();
            Click(PerfilSelecionadoCombobo(perfil));
            WaitForElement(EntrarModalButton);
            driver.FindElement(EntrarModalButton).Click();
        }
        #endregion Ações - Modal de boas vindas

        #endregion
    }
}
